import store from '../store/store';
import { Schedule, Notification } from '../types/types';
import { calcNextNotifTime } from './calcNextNotifTime';
import { scheduleLocalNotification } from './scheduleLocalNotification';
import { cancelAllLocalNotifications } from './cancelAllLocalNotifications';
import { getStoredQuoteNotification, storeQuoteNotification } from './quoteNotificationStorage';

type updateStoredNotifDateArgs = { quoteNotifSchedule?: Schedule; quotesDaily?: number };

export async function updateStoredNotifDate({
  quoteNotifSchedule = store.getState().settings.quoteNotifSchedule,
  quotesDaily = store.getState().settings.quotesDaily,
}: updateStoredNotifDateArgs) {
  cancelAllLocalNotifications();

  const storedNotif = await getStoredQuoteNotification();
  if (!storedNotif) return;

  const { quote } = storedNotif;
  const nextNotifTime = await calcNextNotifTime(quoteNotifSchedule, quotesDaily);
  const date = new Date(nextNotifTime);
  storeQuoteNotification(quote, date);

  const newNotif: Notification = { title: quote.source, message: quote.text, date: new Date(date) };
  scheduleLocalNotification(newNotif);
}
