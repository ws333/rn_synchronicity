import { ColorSchemeName, ScaledSize } from 'react-native';
import {
  Torientation,
  Quote,
  QuotesSources,
  Schedule,
  Screen,
  ScreenProps,
  Status,
  ThemeColors,
  ToastState,
} from './types';
import { AlertState } from './typesAlerts';

/**
 * Constants, interfaces and types for appReducer.ts and appActions.ts
 */
export const GOTO_PREVIOUS_SCREEN = 'GOTO_PREVIOUS_SCREEN';
export const SET_ACTIVE_SCREEN = 'SET_ACTIVE_SCREEN';
export const SET_ALERT = 'SET_ALERT';
export const SET_QUOTE = 'SET_QUOTE';
export const SET_QUOTE_IS_WAITING = 'SET_QUOTE_IS_WAITING';
export const SET_QUOTES = 'SET_QUOTES';
export const SET_ORIENTATION = 'SET_ORIENTATION';
export const SET_SELECTED_LIST_ITEM = 'SET_SELECTED_LIST_ITEM';
export const SET_TOAST = 'SET_TOAST';
export const SET_ADD_SOURCE_BUTTON_HEIGHT = 'SET_ADD_SOURCE_BUTTON_HEIGHT';

export type ActiveScreen = {
  screen: Screen;
  props?: ScreenProps;
};

interface GotoPreviousScreen {
  type: typeof GOTO_PREVIOUS_SCREEN;
}

interface SetActiveScreen {
  type: typeof SET_ACTIVE_SCREEN;
  value: ActiveScreen;
}

interface SetAlert {
  type: typeof SET_ALERT;
  value: AlertState;
}

interface SetQuote {
  type: typeof SET_QUOTE;
  value: Quote;
}

interface SetQuoteIsWaiting {
  type: typeof SET_QUOTE_IS_WAITING;
  value: boolean;
}

interface SetQuotes {
  type: typeof SET_QUOTES;
  value: Quote[];
}

interface SetOrientation {
  type: typeof SET_ORIENTATION;
  value: Torientation;
}

interface SetSelectedListItem {
  type: typeof SET_SELECTED_LIST_ITEM;
  value: string;
}

interface SetToast {
  type: typeof SET_TOAST;
  value: ToastState;
}

interface SetAddSourceButtonHeight {
  type: typeof SET_ADD_SOURCE_BUTTON_HEIGHT;
  value: number;
}

export type AppActionTypes =
  | GotoPreviousScreen
  | SetActiveScreen
  | SetAlert
  | SetQuote
  | SetQuoteIsWaiting
  | SetQuotes
  | SetOrientation
  | SetSelectedListItem
  | SetToast
  | SetAddSourceButtonHeight;

export type AppReducerState = {
  activeScreen: { screen: Screen; props?: ScreenProps };
  alert: AlertState;
  dimensions: ScaledSize;
  history: Screen[];
  orientation: Torientation;
  quote: Quote;
  quoteIsWaiting?: boolean;
  quotes: Quote[];
  selectedListItem: string;
  toast?: ToastState;
  addSourceButtonHeight?: number;
};

/**
 * Constants, interfaces and types for settingsReducer.ts and settingsActions.ts
 */
export const HYDRATE_SETTINGS_FROM_STORAGE = 'HYDRATE_SETTINGS_FROM_STORAGE';
export const SET_COLOR_SCHEME = 'SET_COLOR_SCHEME';
export const SET_FOLLOW_DEVICE_COLOR_SCHEME = 'SET_FOLLOW_DEVICE_COLOR_SCHEME';
export const SET_FOLLOW_DEVICE_TIME_FORMAT = 'SET_FOLLOW_DEVICE_TIME_FORMAT';
export const SET_IS24HOUR = 'SET_IS24HOUR';
export const SET_ISHYDRATED = 'SET_ISHYDRATED';
export const SET_QUOTE_NOTIF_START_TIME = 'SET_QUOTE_NOTIF_START_TIME';
export const SET_QUOTE_NOTIF_END_TIME = 'SET_QUOTE_NOTIF_END_TIME';
export const SET_QUOTE_NOTIF_STATUS = 'SET_QUOTE_NOTIF_STATUS';
export const SET_QUOTES_DAILY = 'SET_QUOTES_DAILY';
export const SET_QUOTES_SOURCES = 'SET_QUOTES_SOURCES';

interface HydrateSettingsFromStorage {
  type: typeof HYDRATE_SETTINGS_FROM_STORAGE;
  value: SettingsReducerState;
}

interface SetColorScheme {
  type: typeof SET_COLOR_SCHEME;
  value: ColorSchemeName;
  saveToStorage: boolean;
}

interface SetFollowDeviceColorScheme {
  type: typeof SET_FOLLOW_DEVICE_COLOR_SCHEME;
  value: boolean;
  saveToStorage: boolean;
}

interface SetFollowDeviceTimeFormat {
  type: typeof SET_FOLLOW_DEVICE_TIME_FORMAT;
  value: boolean;
  saveToStorage: boolean;
}

interface SetIs24Hour {
  type: typeof SET_IS24HOUR;
  value: boolean;
  saveToStorage: boolean;
}

interface SetIsHydrated {
  type: typeof SET_ISHYDRATED;
  value: boolean;
}

interface SetQuoteNotifStartTime {
  type: typeof SET_QUOTE_NOTIF_START_TIME;
  value: Date;
  saveToStorage: boolean;
}

interface SetQuoteNotifEndTime {
  type: typeof SET_QUOTE_NOTIF_END_TIME;
  value: Date;
  saveToStorage: boolean;
}

interface SetQuoteNotifStatus {
  type: typeof SET_QUOTE_NOTIF_STATUS;
  value: Status;
  saveToStorage: boolean;
}

interface SetQuotesDaily {
  type: typeof SET_QUOTES_DAILY;
  value: number;
  saveToStorage: boolean;
}

interface SetQuotesSources {
  type: typeof SET_QUOTES_SOURCES;
  value: QuotesSources;
  saveToStorage: boolean;
}

export type SettingsActionTypes =
  | HydrateSettingsFromStorage
  | SetColorScheme
  | SetFollowDeviceColorScheme
  | SetFollowDeviceTimeFormat
  | SetIs24Hour
  | SetIsHydrated
  | SetQuoteNotifStartTime
  | SetQuoteNotifEndTime
  | SetQuoteNotifStatus
  | SetQuotesDaily
  | SetQuotesSources;

export type SettingsReducerState = {
  colorScheme: ColorSchemeName;
  followDeviceColorScheme: boolean;
  followDeviceTimeFormat: boolean;
  is24Hour: boolean;
  isHydrated?: boolean;
  quoteNotifStartTime: Date;
  quoteNotifEndTime: Date;
  quoteNotifSchedule: Schedule;
  quoteNotifStatus: Status;
  quotesDaily: number;
  quotesSources: QuotesSources;
  themeColors: ThemeColors;
};
