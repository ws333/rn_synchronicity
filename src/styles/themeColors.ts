export const blue = '#007aba';
export const darkBlue = '#0d213f';
export const darkGreen = '#202C19';
export const paleLightGreen = '#0601';
const backgroundLight = '#fcfefc';
const borderColorLight = '#0002';
const borderColorDark = '#fff2';
const menuColorDark = blue;
const paperDark = '#000000dd';
const paperLight = backgroundLight + 'b0';
const textWhite = '#e5e5e7f0'; // Also used for spinner
const textBlack = darkGreen + 'c0'; // Also used for spinner

export const themeColors: ThemeColorObjects = {
  activeOpacity: {
    light: '0.7',
    dark: '0.8',
  },
  activeOpacityMenuGridItem: {
    light: '0.7',
    dark: '0.8',
  },
  alertTitle: {
    light: 'orange',
    dark: 'orange',
  },
  backdropOverlay: {
    light: '#0008',
    dark: darkBlue + '80',
  },
  backdropSpinner: {
    light: '#fff3',
    dark: '#0008',
  },
  background: {
    light: backgroundLight,
    dark: '#010101',
  },
  border: {
    light: borderColorLight,
    dark: borderColorDark,
  },
  brainwavePaper: {
    light: paperLight,
    dark: paperDark,
  },
  dateTimeItemBackground: {
    light: 'green',
    dark: 'darkgreen',
  },
  dateTimePickerText: {
    light: '#000',
    dark: '#fff',
  },
  dateTimeText: {
    // Does not support themes yet
    light: '#fff',
    dark: '#fff',
  },
  disabledText: {
    light: 'lightgrey',
    dark: '#333',
  },
  donatePaper: {
    light: paperLight,
    dark: paperDark,
  },
  FAQPaper: {
    light: paperLight,
    dark: paperDark,
  },
  headerPaper: {
    light: 'transparent',
    dark: 'transparent',
  },
  inputSpinner: {
    light: 'green',
    dark: 'darkgreen',
  },
  iconLink: {
    light: 'orange',
    dark: 'orange',
  },
  iconQuotesSourceCategory: {
    light: 'orange',
    dark: 'orange',
  },
  inputSpinnerMinMax: {
    light: 'darkred',
    dark: 'darkred',
  },
  logoPaper: {
    light: '#fff0',
    dark: '#0000',
  },
  manageQuotesSourcesPaper: {
    light: '#fff0',
    dark: '#0000',
  },
  menuGridBackground: {
    light: '#fff0',
    dark: '#0000',
  },
  menuGridItemBackground: {
    light: backgroundLight + '80',
    dark: '#000e',
  },
  menuGridItemBorder: {
    light: '#888a',
    dark: `${menuColorDark}55`,
  },
  menuGridItemText: {
    light: 'seagreen',
    dark: blue,
  },
  quoteButtonText: {
    light: 'darkseagreen',
    dark: blue,
  },
  quotePaper: {
    light: paperLight,
    dark: paperDark,
  },
  screenOverlay: {
    light: paleLightGreen,
    dark: darkBlue + '80',
  },
  settingsPaper: {
    light: paperLight,
    dark: paperDark,
  },
  settingsText: {
    light: 'dimgrey',
    dark: 'lightgrey',
  },
  shadowColor: {
    light: '#777b',
    dark: '#048b',
  },
  shadowColorMenuGrid: {
    light: '#000',
    dark: '#048b',
  },
  switchThumb: {
    light: 'green',
    dark: 'darkgreen',
  },
  switchTrack: {
    light: '#0002',
    dark: '#333333',
  },
  text: {
    // Also used for spinner
    light: textBlack,
    dark: textWhite,
  },
  textButtonBackground: {
    light: paleLightGreen,
    dark: '#000a',
  },
  textButtonBackgroundDark: {
    light: darkGreen + '80',
    dark: '#000e',
  },
  textButtonBorder: {
    light: '#00800099',
    dark: `${blue}99`,
  },
  textButtonDisabledText: {
    light: 'gold',
    dark: 'gold',
  },
  textButtonText: {
    light: '#040b',
    dark: blue,
  },
  textButtonTextDark: {
    light: '#fff',
    dark: blue,
  },
  toastBorderLeft: {
    light: 'darkseagreen',
    dark: blue,
  },
};

type ThemeColorObjects = { [k in ColorKeys]: { light: string; dark: string } };

export type ColorKeys =
  | 'activeOpacity'
  | 'activeOpacityMenuGridItem'
  | 'alertTitle'
  | 'backdropOverlay'
  | 'backdropSpinner'
  | 'background'
  | 'border'
  | 'brainwavePaper'
  | 'dateTimeItemBackground'
  | 'dateTimePickerText'
  | 'dateTimeText'
  | 'disabledText'
  | 'donatePaper'
  | 'FAQPaper'
  | 'headerPaper'
  | 'iconLink'
  | 'iconQuotesSourceCategory'
  | 'inputSpinner'
  | 'inputSpinnerMinMax'
  | 'logoPaper'
  | 'manageQuotesSourcesPaper'
  | 'menuGridBackground'
  | 'menuGridItemBackground'
  | 'menuGridItemBorder'
  | 'menuGridItemText'
  | 'quoteButtonText'
  | 'quotePaper'
  | 'screenOverlay'
  | 'settingsPaper'
  | 'settingsText'
  | 'shadowColor'
  | 'shadowColorMenuGrid'
  | 'switchThumb'
  | 'switchTrack'
  | 'text'
  | 'textButtonBackground'
  | 'textButtonBackgroundDark'
  | 'textButtonBorder'
  | 'textButtonDisabledText'
  | 'textButtonText'
  | 'textButtonTextDark'
  | 'toastBorderLeft';
