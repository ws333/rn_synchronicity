import { SettingsReducerState } from '../../types/typesRedux';
import { Schedule } from '../../types/types';
import { constants } from '../../constants/constants';

export function getNotifSchedule(state: Partial<SettingsReducerState>): Schedule {
  const quoteNotifStartTimeState = state.quoteNotifStartTime || constants.defaults.quoteNotifStartTime;
  const quoteNotifEndTimeState = state.quoteNotifEndTime || constants.defaults.quoteNotifEndTime;
  const startHours = quoteNotifStartTimeState.getHours();
  const startMins = quoteNotifStartTimeState.getMinutes();
  const endHours = quoteNotifEndTimeState.getHours();
  const endMins = quoteNotifEndTimeState.getMinutes();
  return { startHours, startMins, endHours, endMins };
}
