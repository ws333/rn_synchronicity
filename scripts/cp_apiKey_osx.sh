#!/bin/zsh
cp ~/Google\ Drive/Dev/Dev\ projects/RN/rn_synchronicity_misc/archive/types/YouTubeProps/main.d.ts ~/dev/vsc/RN/rn_synchronicity/node_modules/react-native-youtube/
if [ $? -eq 0 ]; then
  echo "YouTubeProps updated successfully"
else
  echo "Error updating YouTubeProps!"
fi