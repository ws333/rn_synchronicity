import store from '../store/store';
import { QuotesSources } from '../types/types';

export function getEnabledQuotesSources(sources?: QuotesSources) {
  const quotesSourcesState = sources ?? store.getState().settings.quotesSources;
  const enabledQuoteSources = Object.keys(quotesSourcesState).filter((key) => quotesSourcesState[key].enabled);
  return enabledQuoteSources;
}
