import { toastConstants } from '../components/ToastMessage';
import { styleConstants } from '../styles/globalStyles';
import { calcScreenSpacerTop } from './calcScreenSpacers';
import { getIsPortrait } from './screenDimensions';

export function getToastLayout(offset: number = styleConstants.screenSpacer * 2) {
  const screenSpacerTop = calcScreenSpacerTop();
  /**
   * Toast message (text2) should fit on one line in landscape mode
   */
  const toastHeight = getIsPortrait()
    ? toastConstants.height + toastConstants.text2FontSize * 1.2
    : toastConstants.height;
  const toastOffset = screenSpacerTop + offset - toastHeight - toastConstants.top;
  return {
    toastHeight,
    toastOffset,
  };
}
