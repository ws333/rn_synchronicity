import { ColorSchemeName } from 'react-native';
import { QuotesSources, Status } from '../../types/types';
import {
  HYDRATE_SETTINGS_FROM_STORAGE,
  SET_COLOR_SCHEME,
  SET_FOLLOW_DEVICE_COLOR_SCHEME,
  SET_FOLLOW_DEVICE_TIME_FORMAT,
  SET_IS24HOUR,
  SET_ISHYDRATED,
  SET_QUOTES_DAILY,
  SET_QUOTE_NOTIF_START_TIME,
  SET_QUOTE_NOTIF_END_TIME,
  SET_QUOTE_NOTIF_STATUS,
  SET_QUOTES_SOURCES,
  SettingsActionTypes,
  SettingsReducerState,
} from '../../types/typesRedux';

export const hydrateSettingsFromStorage = (value: SettingsReducerState): SettingsActionTypes => {
  return { type: HYDRATE_SETTINGS_FROM_STORAGE, value };
};
export const setColorScheme = (value: ColorSchemeName, saveToStorage = true): SettingsActionTypes => {
  return { type: SET_COLOR_SCHEME, value, saveToStorage };
};
export const setFollowDeviceColorScheme = (value: boolean, saveToStorage = true): SettingsActionTypes => {
  return { type: SET_FOLLOW_DEVICE_COLOR_SCHEME, value, saveToStorage };
};
export const setFollowDeviceTimeFormat = (value: boolean, saveToStorage = true): SettingsActionTypes => {
  return { type: SET_FOLLOW_DEVICE_TIME_FORMAT, value, saveToStorage };
};
export const setIs24Hour = (value: boolean, saveToStorage = true): SettingsActionTypes => {
  return { type: SET_IS24HOUR, value, saveToStorage };
};
export const setIsHydrated = (value: boolean): SettingsActionTypes => {
  return { type: SET_ISHYDRATED, value };
};
export const setQuotesDaily = (value: number, saveToStorage = true): SettingsActionTypes => {
  return { type: SET_QUOTES_DAILY, value, saveToStorage };
};
export const setQuoteNotifStartTime = (value: Date, saveToStorage = true): SettingsActionTypes => {
  return { type: SET_QUOTE_NOTIF_START_TIME, value, saveToStorage };
};
export const setQuoteNotifEndTime = (value: Date, saveToStorage = true): SettingsActionTypes => {
  return { type: SET_QUOTE_NOTIF_END_TIME, value, saveToStorage };
};
export const setQuoteNotifStatus = (value: Status, saveToStorage = true): SettingsActionTypes => {
  return { type: SET_QUOTE_NOTIF_STATUS, value, saveToStorage };
};
export const setQuotesSources = (value: QuotesSources, saveToStorage = true): SettingsActionTypes => {
  return { type: SET_QUOTES_SOURCES, value, saveToStorage };
};
