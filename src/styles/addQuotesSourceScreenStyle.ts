import { ViewStyle, TextStyle } from 'react-native';
import { getDimensionsState } from '../helpers/screenDimensions';
import { ThemeColors } from '../types/types';
import { styleConstants } from './globalStyles';

export interface LocalStyles {
  addUserQuotesSourceStyle: ViewStyle;
  helpText: TextStyle;
  input: {
    leftIcon: { type: string; name: string; color: string };
    style: TextStyle;
  };
  keyboardAvoidingView: ViewStyle;
  textButtonContainer: ViewStyle;
  textButtonDisabled: TextStyle;
}

module.exports = (colors: ThemeColors): LocalStyles => {
  const dimensionsState = getDimensionsState();
  return {
    addUserQuotesSourceStyle: {
      borderRadius: styleConstants.radius,
      backgroundColor: colors.background,
      borderColor: colors.border,
      borderWidth: styleConstants.borderWidth,
      alignItems: 'center',
      marginVertical: dimensionsState.height / 4,
    },
    helpText: {
      marginTop: -styleConstants.marginLRTB / 2,
      fontSize: styleConstants.fontSizeBase - 2,
      fontStyle: 'italic',
      textAlign: 'center',
      color: colors.text,
    },
    input: {
      leftIcon: { type: 'font-awesome', name: 'link', color: colors.iconLink },
      style: { color: colors.text },
    },
    keyboardAvoidingView: {
      width: styleConstants.quotesSourceWidth,
    },
    textButtonContainer: {
      flexDirection: 'row',
      marginTop: styleConstants.marginLRTB / 2,
      marginBottom: 4,
    },
    textButtonDisabled: {
      color: 'darkgrey',
      opacity: 0.5,
    },
  };
};
