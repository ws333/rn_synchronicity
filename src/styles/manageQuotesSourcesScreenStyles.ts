import { ViewStyle } from 'react-native';
import { getIsPortrait } from '../helpers/screenDimensions';
import { ThemeColors } from '../types/types';
import { styleConstants } from './globalStyles';

export interface LocalStyles {
  content: ViewStyle;
  paper: ViewStyle;
}

module.exports = (colors: ThemeColors): LocalStyles => {
  const isPortrait = getIsPortrait();

  return {
    content: {
      flex: 1,
      width: styleConstants.quotesSourceWidth,
    },
    paper: {
      flex: 1,
      width: '100%',
      height: '100%',
      alignItems: 'center',
      marginTop: isPortrait ? styleConstants.marginLRTB / 6 : styleConstants.marginLRTB / 8,
      paddingBottom: styleConstants.padding / 2,
      backgroundColor: colors.manageQuotesSourcesPaper,
      borderRadius: styleConstants.radius,
      borderColor: colors.border,
    },
  };
};
