/**
 * Hot reload requires a class based "root" component to work (march 2020)
 */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { RootState } from './store/store';
import { setToast } from './store/actions/appActions';
import Navigation from './Navigation';
import ToastMessage, { Toast, toastConstants } from './components/ToastMessage';
import { getToastLayout } from './helpers/getToastLayout';
import { ThemeColors, ToastState, Torientation } from './types/types';

interface Props {
  colors: ThemeColors;
  orientation: Torientation;
  addSourceButtonHeight: number | undefined;
  toast: ToastState;
  dispatch: any;
}

class NavigationWrapper extends PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  componentDidUpdate() {
    const { toast } = this.props;
    if (toast && !toast.isShowing && toast.type !== 'hidden') {
      Toast.show(toast);
      this.props.dispatch(setToast({ ...toast, isShowing: true }));
    } else if (toast && toast.isShowing && toast.type === 'hidden') {
      Toast.hide();
      this.props.dispatch(setToast({ ...toast, isShowing: false }));
    }
  }

  componentWillUnmount() {
    const { toast } = this.props;
    if (toast) {
      this.props.dispatch(setToast({ ...toast, type: 'hidden', isShowing: false }));
    }
  }

  render() {
    const { toastHeight, toastOffset } = getToastLayout(this.props.addSourceButtonHeight);
    const { top } = toastConstants;
    return (
      <>
        <Navigation />
        <ToastMessage style={{ top: top }} colors={this.props.colors} height={toastHeight} topOffset={toastOffset} />
      </>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  orientation: state.app.orientation,
  colors: state.settings.themeColors,
  addSourceButtonHeight: state.app.addSourceButtonHeight,
  toast: state.app.toast,
});

export default connect(mapStateToProps)(NavigationWrapper);
