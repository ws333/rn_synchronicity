interface Options extends RequestInit {
  timeout?: number;
}
const defaultOptions = { timeout: 4000 };

export async function fetchWithTimeout(resource: RequestInfo, options: Options = defaultOptions) {
  const { timeout } = options;

  const controller = new AbortController();
  const id = setTimeout(() => {
    controller.abort();
  }, timeout);

  const response = await fetch(resource, {
    ...options,
    signal: controller.signal,
  });
  clearTimeout(id);

  return response;
}
