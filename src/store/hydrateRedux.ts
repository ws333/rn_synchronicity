import store from './store';
import { getNotifSchedule } from './storehelpers/getNotifSchedule';
import { hydrateSettingsFromStorage, setIsHydrated } from './actions/settingsActions';
import { setActiveScreen, setQuote, setQuoteIsWaiting, setQuotes } from './actions/appActions';
import { SettingsReducerState } from '../types/typesRedux';
import { Quote, QuotesSources, Status, StoredNotification } from '../types/types';
import { displayStoredQuoteNotification } from '../helpers/displayStoredQuoteNotification';
import { getStoredQuoteNotification } from '../helpers/quoteNotificationStorage';
import { getStoredColorScheme } from '../helpers/getStoredColorScheme';
import { filterOutQuotes } from '../helpers/filterOutQuotes';
import { getQuotesInApp } from '../helpers/getQuotes';
import { getColorScheme } from '../helpers/getApperance';
import { getRandomQuote } from '../helpers/getRandomQuote';
import { getThemeColors } from '../helpers/getThemeColors';
import { getStoredData } from '../services/storage';
import { constants } from '../constants/constants';
import { getStoredIs24Hour } from '../helpers/getStoredIs24Hour';

export const hydrateRedux = async () => {
  try {
    global.log('[hydrateRedux] -> async hydrateRedux() -> hydrate settings and quotes').yellowOnGreen();

    store.dispatch(setIsHydrated(false));

    const storedFollowDeviceColorScheme: boolean | null = await getStoredData('FOLLOW_DEVICE_COLOR_SCHEME');
    const followDeviceColorScheme: boolean =
      storedFollowDeviceColorScheme ?? constants.defaults.followDeviceColorScheme;
    // prettier-ignore
    global.log('[hydrateRedux] -> settings.followDeviceColorScheme will be set to:', followDeviceColorScheme).blackOnOrange();

    const colorScheme = await getStoredColorScheme();
    global.log('[hydrateRedux] -> settings.colorScheme will be set to:', colorScheme).blackOnOrange();

    const storedFollowDeviceTimeFormat: boolean | null = await getStoredData('FOLLOW_DEVICE_TIME_FORMAT');
    const followDeviceTimeFormat: boolean = storedFollowDeviceTimeFormat ?? constants.defaults.followDeviceTimeFormat;
    // prettier-ignore
    global.log('[hydrateRedux] -> settings.followDeviceTimeFormat will be set to:', followDeviceTimeFormat).blackOnOrange();

    const is24Hour = await getStoredIs24Hour();
    global.log('[hydrateRedux] -> settings.is24Hour will be set to:', is24Hour).blackOnOrange();

    const storedQuotesDaily = await getStoredData('QUOTES_DAILY');
    const quotesDaily: number = storedQuotesDaily ?? constants.defaults.quotesDaily;
    global.log('[hydrateRedux] -> settings.quotesDaily will be set to:', quotesDaily).blackOnOrange();

    const storedStartTime = await getStoredData('QUOTE_NOTIF_START_TIME');
    const quoteNotifStartTime: Date = storedStartTime
      ? new Date(storedStartTime)
      : constants.defaults.quoteNotifStartTime; // Stored as string so need to convert back to Date object
    global.log('[hydrateRedux] -> settings.quoteNotifStartTime will be set to:', quoteNotifStartTime).blackOnOrange();

    const storedEndTime = await getStoredData('QUOTE_NOTIF_END_TIME');
    const quoteNotifEndTime: Date = storedEndTime ? new Date(storedEndTime) : constants.defaults.quoteNotifEndTime; // Stored as string so need to convert back to Date object
    global.log('[hydrateRedux] -> settings.quoteNotifEndTime will be set to:', quoteNotifEndTime).blackOnOrange();

    const quoteNotifSchedule = getNotifSchedule({ quoteNotifStartTime, quoteNotifEndTime });

    const storedQuoteNotifStatus = await getStoredData('QUOTE_NOTIF_STATUS');
    const quoteNotifStatus: Status = storedQuoteNotifStatus ?? constants.defaults.quoteNotifStatus;
    global.log('[hydrateRedux] -> settings.quoteNotifStatus will be set to:', quoteNotifStatus).blackOnOrange();

    const storedQuotesSources: QuotesSources = (await getStoredData('QUOTES_SOURCES')) ?? {};
    const inAppQuotesSources = filterOutQuotes(constants.defaults.quotesSources); // The quotes are handled by getQuotesInApp() below

    /**
     * Set default enabled in-app sources if non stored
     */
    if (Object.keys(storedQuotesSources).length === 0) {
      constants.defaults.enabledQuotesSources.forEach((key) => {
        storedQuotesSources[key] = { ...inAppQuotesSources[key], enabled: true };
      });
    }

    /**
     * Merge inAppQoutesSources into stored sources, but keep stored enabled values
     * This is to reflect changes in inApp sources during development and after app upgrades
     */
    Object.keys(inAppQuotesSources).forEach((key) => {
      inAppQuotesSources[key].enabled = storedQuotesSources[key]?.enabled;
      storedQuotesSources[key] = inAppQuotesSources[key];
    });

    const quotesSources: QuotesSources = storedQuotesSources;
    global.log('[hydrateRedux] -> settings.quotesSources will be set to:', quotesSources).blackOnOrange();

    const storedOnlineQuotes = (await getStoredData('ONLINE_QUOTES')) ?? [];
    const quotes: Quote[] = getQuotesInApp().concat(storedOnlineQuotes);
    store.dispatch(setQuotes(quotes));

    /**
     * Navigate to QuoteScreen and dispatch quote if a stored quote is due.
     * This is to avoid a HomeScreen to QuoteScreen flicker on app launch.
     */
    const notification: StoredNotification | null = await getStoredQuoteNotification();
    if (notification && 'quote' in notification && 'date' in notification) {
      const { quote, date } = notification;
      global.log('[hydrateRedux] -> stored notification -> quote, date = ', quote, date).blackOnOrange();
      if (date && new Date(date).getTime() < Date.now()) {
        global.log("[hydrateRedux] -> store.dispatch(setActiveScreen({ screen: 'QUOTES' }))").redOnYellow();
        store.dispatch(setQuote(quote));
        store.dispatch(setActiveScreen({ screen: 'QUOTES' }));
        notification.dispatched = true;
      }
    }

    const activeColorScheme = followDeviceColorScheme ? getColorScheme() : colorScheme;
    const themeColors = getThemeColors(activeColorScheme);

    const values: SettingsReducerState = {
      colorScheme,
      followDeviceColorScheme,
      followDeviceTimeFormat,
      is24Hour,
      quotesDaily,
      quoteNotifStartTime,
      quoteNotifEndTime,
      quoteNotifSchedule,
      quoteNotifStatus,
      quotesSources,
      themeColors,
    };

    store.dispatch(hydrateSettingsFromStorage(values));
    global.log('[hydrateRedux] -> hydrating redux done -> values =', values).redOnYellow();

    if (!notification?.dispatched) {
      const firstQuote = getRandomQuote({ currentQuote: constants.defaults.quote });
      global.log('[hydrateRedux] -> firstQuote =', firstQuote).redOnYellow();
      store.dispatch(setQuote(firstQuote));
    }

    const { quoteIsWaiting } = store.getState().app;
    if (quoteIsWaiting) {
      global.log('[hydrateRedux] -> app.quoteIsWaiting is true').yellowOnGreen();
      displayStoredQuoteNotification();
      store.dispatch(setQuoteIsWaiting(false));
    }
  } catch (e) {
    global.log('[hydrateRedux] -> hydrateRedux() exception:', e).yellowOnBlack();
  }
};
