export async function timeout(milliseconds: number = 1000) {
  await new Promise((resolve) => {
    setTimeout(() => resolve(1), milliseconds);
  });
}
