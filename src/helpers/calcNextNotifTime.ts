import { getStoredData, storeData } from '../services/storage';
import { millisToHours, hoursToMillis } from './time';
import { Schedule } from '../types/types';

/**
 * firstExecution = true when enabling notif Schedule from SettingsScreen, else false
 */
export async function calcNextNotifTime(schedule: Schedule, quotesDaily: number, firstExecution = false) {
  global.log('[calcNNT] -> start executing... -> firstExecution =', firstExecution).yellowOnGreen();

  // prettier-ignore
  if (!quotesDaily) {
    global.log('[calcNNT] -> calcNextNotifTime() -> quotesDaily is ', quotesDaily, ', NOT performing calcNextNotifTime').redOnBlack();
    return 0;
  }

  let prevCalcPeriod = await getStoredData('QUOTE_NOTIF_PREV_CALC_PERIOD');
  const startMins = schedule.startMins ? schedule.startMins / 60 : 0;
  const startTime = schedule.startHours + startMins;
  const endMins = schedule.endMins ? schedule.endMins / 60 : 0;
  const endTime = schedule.endHours + endMins;

  const scheduleDurationInHours = endTime < startTime ? 24 - startTime + endTime : endTime - startTime;
  const periodLenght = hoursToMillis(scheduleDurationInHours / quotesDaily);
  const randomNum = Math.random();
  const millisDeltaNextPeriodStartToNotif = Math.floor(periodLenght * randomNum);
  const currentTime = Date.now();
  // currentTime = adjustTime(currentTime, 0, 0); // Used for test/debugging

  const tmpDate = new Date(currentTime);
  const timeAtMidnight = tmpDate.setHours(0, 0, 0, 0);
  let millisDeltaMidnightToFirstPeriodStart = hoursToMillis(startTime);
  let timeFirstPeriodStart = timeAtMidnight + millisDeltaMidnightToFirstPeriodStart;
  let millisSinceMidnight = currentTime - timeAtMidnight;
  const tzMidnight = new Date(timeAtMidnight).getTimezoneOffset();
  const tzFirstPeriodStart = new Date(timeFirstPeriodStart).getTimezoneOffset();

  // prettier-ignore
  {
    global.log('[calcNNT] -> schedule =', schedule).black();
    global.log('[calcNNT] -> quotesDaily (number of time periods) =', quotesDaily).black();
    global.log('[calcNNT] -> stored QUOTE_NOTIF_PREV_CALC_PERIOD =', prevCalcPeriod).black();
    global.log('[calcNNT] -> startTime =', startTime).black();
    global.log('[calcNNT] -> endTime =', endTime).black();
    global.log('[calcNNT] -> scheduleDurationInHours =', scheduleDurationInHours).black();
    global.log('[calcNNT] -> periodLenght in secs =', periodLenght / 1000).black();
    global.log('[calcNNT] -> periodLenght in minutes =', millisToHours(periodLenght) * 60).black();
    global.log('[calcNNT] -> currentTime =', currentTime).black();
    global.log('[calcNNT] -> new Date(currentTime) = ', new Date(currentTime)).black();
    global.log('[calcNNT] -> new Date(timeAtMidnight) =', new Date(timeAtMidnight)).black();
    global.log('[calcNNT] -> millisDeltaMidnightFirstPeriodStart =', millisDeltaMidnightToFirstPeriodStart).black();
    global.log('[calcNNT] -> millisDeltaMidnightFirstPeriodStart in hours =', millisToHours(millisDeltaMidnightToFirstPeriodStart)).black();
    global.log('[calcNNT] -> new Date(timeFirstPeriodStart) =', new Date(timeFirstPeriodStart)).black();
    global.log('[calcNNT] -> millisSinceMidnight =', millisSinceMidnight).black();
    global.log('[calcNNT] -> millisSinceMidnight in hours =', millisToHours(millisSinceMidnight)).black();
    global.log('[calcNNT] -> tzMidnight =', tzMidnight).black();
    global.log('[calcNNT] -> tzFirstPeriodStart =', tzFirstPeriodStart).black();
  }

  /**
   * Adjust relevant variables when timezone change because of daylight saving time
   */
  // prettier-ignore
  if (tzMidnight !== tzFirstPeriodStart) {
    const diffInMillis = (tzFirstPeriodStart - tzMidnight) * 60 * 1000;
    timeFirstPeriodStart += diffInMillis;
    millisDeltaMidnightToFirstPeriodStart += diffInMillis;
    global.log('[calcNNT] -> adjusting for daylight saving time change since last period, diffInMillis =', diffInMillis).yellowOnRed();
    global.log('[calcNNT] -> new Date(timeFirstPeriodStart) =', new Date(timeFirstPeriodStart)).yellowOnRed();
    global.log('[calcNNT] -> millisDeltaFirstPeriodStart in hours =', millisToHours(millisDeltaMidnightToFirstPeriodStart)).yellowOnRed();
  }

  function setAndStorePrevCalcPeriod(value: number) {
    prevCalcPeriod = value;
    storeData('QUOTE_NOTIF_PREV_CALC_PERIOD', prevCalcPeriod);
  }

  const scheduleExceedMidnigthInHours = startTime + scheduleDurationInHours - 24;
  const isExceedMidnightTimeWindow = scheduleExceedMidnigthInHours > millisToHours(millisSinceMidnight);
  global.log('[calcNNT] -> isExceedMidnightTimeWindow =', isExceedMidnightTimeWindow).yellowOnRed();
  const isBeforeScheduleStart = millisSinceMidnight < millisDeltaMidnightToFirstPeriodStart;
  if (isBeforeScheduleStart && !isExceedMidnightTimeWindow) {
    const timeFirstNotif = timeFirstPeriodStart + millisDeltaNextPeriodStartToNotif;
    // prettier-ignore
    global.log('[calcNNT] -> isBeforeScheduleStart -> new Date(timeFirstNotif) =', new Date(timeFirstNotif)).yellowOnRed();
    setAndStorePrevCalcPeriod(0);
    return timeFirstNotif;
  }

  const scheduleEndIsSameDay = startTime < endTime;
  const isAfterScheduleEnd = millisSinceMidnight > hoursToMillis(endTime) && scheduleEndIsSameDay;
  if (isAfterScheduleEnd) {
    const timeNextDayNotif = hoursToMillis(24) + timeFirstPeriodStart + millisDeltaNextPeriodStartToNotif;
    // prettier-ignore
    global.log('[calcNNT] -> isAfterScheduleEnd -> new Date(timeNextDayNotif) =', new Date(timeNextDayNotif)).yellowOnRed();
    setAndStorePrevCalcPeriod(0);
    return timeNextDayNotif;
  }

  /**
   * Adjust millisSinceMidnight if scheduled range exceed midnight and now currently inside that time window
   */
  if (isExceedMidnightTimeWindow) {
    millisSinceMidnight += hoursToMillis(24);
    timeFirstPeriodStart -= hoursToMillis(24);
    global.log('[calcNNT] -> isExceedMidnightTimeWindow -> adjusting offsets').yellowOnRed();
  }

  const currentPeriod = Math.floor((millisSinceMidnight - millisDeltaMidnightToFirstPeriodStart) / periodLenght);
  const isNewPeriod = currentPeriod !== prevCalcPeriod;
  let calcPeriod = firstExecution || isNewPeriod ? currentPeriod : currentPeriod + 1;

  /**
   * If last calcPeriod is done, return time for first notif next day
   */
  if (calcPeriod >= quotesDaily) {
    const timeNextDayFirstPeriodStart = timeFirstPeriodStart + hoursToMillis(24);
    const timeFirstNextDayNotif = timeNextDayFirstPeriodStart + millisDeltaNextPeriodStartToNotif;
    setAndStorePrevCalcPeriod(0);
    // prettier-ignore
    global.log('[calcNNT] -> last calcPeriod done -> new Date(timeFirstNextDayNotif)', new Date(timeFirstNextDayNotif)).yellowOnRed();
    return timeFirstNextDayNotif;
  }

  const timeNextPeriodStart = (_calcPeriod: number) => {
    return timeFirstPeriodStart + periodLenght * _calcPeriod;
  };

  let timeNextNotif = timeNextPeriodStart(calcPeriod) + millisDeltaNextPeriodStartToNotif;
  if (isNewPeriod && timeNextNotif < currentTime) {
    calcPeriod++;
    timeNextNotif = timeNextPeriodStart(calcPeriod) + millisDeltaNextPeriodStartToNotif;
    global.log('[calcNNT] -> (!isNewPeriod && timeNextNotif < currentTime) -> calc with next period').yellowOnRed();
  }

  /**
   * Theese three variables are only used for debugging / logging
   */
  const secsDeltaCurrentTimeToNextNotif = (timeNextNotif - currentTime) / 1000;
  const secsToCalcPeriodStart = (timeNextPeriodStart(calcPeriod) - currentTime) / 1000;
  const checkMillisDeltaNextPeriodStartToNotif = timeNextNotif - (timeFirstPeriodStart + calcPeriod * periodLenght);

  // prettier-ignore
  {
    global.log('[calcNNT] -> randomNum =', randomNum).black();
    global.log('[calcNNT] -> prevCalcPeriod =', prevCalcPeriod).black();
    global.log('[calcNNT] -> currentPeriod =', currentPeriod).black();
    global.log('[calcNNT] -> calcPeriod =', calcPeriod).black();
    global.log('[calcNNT] -> millisDeltaNextPeriodStartToNotif =', millisDeltaNextPeriodStartToNotif).black();
    global.log('[calcNNT] -> millisDeltaNextPeriodStartToNotif in hours =', millisToHours(millisDeltaNextPeriodStartToNotif)).black();
    global.log('[calcNNT] -> checkMillisDeltaNextPeriodStartToNotif =', checkMillisDeltaNextPeriodStartToNotif).black();
    global.log('              (Should be equal to millisDeltaNextPeriodStartToNotif)').black();
    global.log('[calcNNT] -> new Date(timeNextPeriodStart) =', new Date(timeNextPeriodStart(calcPeriod))).black();
    global.log('[calcNNT] -> new Date(timeNextNotif) =', new Date(timeNextNotif)).yellowOnRed();
    global.log('[calcNNT] -> secsDeltaCurrentTimeToNextNotif =', secsDeltaCurrentTimeToNextNotif).black();
    global.log('[calcNNT] -> secsToCalcPeriodStart (negative is past) =', secsToCalcPeriodStart).black();
  }

  setAndStorePrevCalcPeriod(calcPeriod);
  global.log('[calcNNT] -> done executing!').whiteOnGreen();
  return timeNextNotif;
}
