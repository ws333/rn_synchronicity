import * as Sentry from '@sentry/react-native';

export function sentrySendMessage(message: string) {
  Sentry.captureMessage(message);
}
