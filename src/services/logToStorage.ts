import AsyncStorage from '@react-native-async-storage/async-storage';
import { getStoredData, removeStoredData, storeData } from './storage';
import { StorageKey } from '../types/typesStorage';
import { getTimeStamp } from '../helpers/time';

let logItems: string[] = [];
export const logKey: StorageKey = 'LOG';
export const logLevelKey: StorageKey = 'LOGLEVEL';
export const logStorageLength = 99;

function initialLogItem(cleared?: boolean) {
  const date = new Date(Date.now());
  const text = cleared ? 'Log cleared: ' : 'Log start: ';
  const jsonString = JSON.stringify(text + date);
  return jsonString;
}

async function fetchStoredLog() {
  const storedLog = await AsyncStorage.getItem(logKey);
  logItems = storedLog ? JSON.parse(storedLog) : [initialLogItem()];
}

/**
 * Not using get/setStoredData() from storage.ts since this might log values that are fetched/stored and this will create a cycle.
 */
export async function logToStorage(value: string) {
  try {
    if (!value) throw new Error('No value argument passed!');
    if (!logItems.length) await fetchStoredLog();

    if (logItems.length > logStorageLength) {
      logItems.shift();
    }

    logItems.unshift(`${getTimeStamp()} ${value}`);
    await AsyncStorage.setItem(logKey, JSON.stringify(logItems));
  } catch (e) {
    console.log('[logToStorage] -> logToStorage() -> exception: ', e);
  }
}

export async function clearStoredLog() {
  try {
    await removeStoredData(logKey);
    logItems = [initialLogItem(true)];
    await AsyncStorage.setItem(logKey, JSON.stringify(logItems));
    return logItems;
  } catch (e) {
    console.log('[logToStorage] -> clearStoredLog() -> exception: ', e);
    return [String(e)];
  }
}

export async function getStoredLogLevel() {
  try {
    const logLevel = await getStoredData(logLevelKey);
    return logLevel;
  } catch (e) {
    console.log('[logToStorage] -> getStoredLogLevel() -> exception: ', e);
    return null;
  }
}

export async function setStoredLogLevel(value: number) {
  try {
    await storeData(logLevelKey, value);
  } catch (e) {
    console.log('[logToStorage] -> setStoredLogLevel() -> exception: ', e);
  }
}
