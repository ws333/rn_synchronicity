import { is24HourFormat } from 'react-native-device-time-format';

export function getDeviceTimeFormat() {
  return is24HourFormat();
}
