import React, { FC, ReactElement } from 'react';
import { ScrollView, View, Text, ImageBackground, Image, Platform } from 'react-native';
import { useDispatch } from 'react-redux';
import { EnumPurchaseType, ScreenProps } from '../types/types';
import GoBackButton from '../components/GoBackButton';
import TextButton from '../components/TextButton';
import useTheme from '../hooks/useTheme';
import { isIOS } from '../helpers/platform';
import { executeIfOnline } from '../helpers/networkHelpers';
import { LocalStyles } from '../styles/donateScreenStyles';
import { monthlyDonations, onetimeDonations } from '../constants/products';
import { backgrounds, headers } from '../constants/images';
import { setActiveScreen } from '../store/actions/appActions';

const DonateScreen: FC<ScreenProps> = (props): ReactElement => {
  global.log('[DonateScreen] -> Component rendering...').green();

  const { styles, colors } = useTheme();
  const localStyles: LocalStyles = require('../styles/donateScreenStyles')(colors);

  const dispatch = useDispatch();

  const header = (
    <View style={{ ...styles.header }}>
      <Image source={headers.donationsHeaderImg()} />
    </View>
  );

  function handlePressOnetime() {
    const cb = () => {
      dispatch(
        setActiveScreen({
          screen: 'PURCHASE_DONATION',
          props: { productIDs: onetimeDonations, purchaseType: EnumPurchaseType.INAPP },
        }),
      );
    };
    executeIfOnline(cb);
  }

  function handlePressMonthly() {
    const cb = () => {
      dispatch(
        setActiveScreen({
          screen: 'PURCHASE_DONATION',
          props: { productIDs: monthlyDonations, purchaseType: EnumPurchaseType.SUBS },
        }),
      );
    };
    executeIfOnline(cb);
  }

  const DonateText = (
    <View style={localStyles.donateTextContainer}>
      {header}
      <Text style={{ ...localStyles.donateText, color: colors.text }}>
        If you feel inspired to support this app and would like to flow energy into the project this is highly
        appreciated. Countless nights have been spent designing, coding and configuring to make this happen, from
        inspirational highs to hair pulling lows. It felt like a calling, a deep urge to share something with the world.
        {'\n\n'}With love....
      </Text>
      <Text />
    </View>
  );

  const PaymentText = (
    <Text style={{ ...localStyles.paymentText, color: colors.text }}>
      Payments are handled securely by
      {Platform.select({ ios: '\nApple App store', android: '\nGoogle Play store' })}
    </Text>
  );

  const DonateButtons = (
    <View style={localStyles.donateButtons}>
      <TextButton containerStyle={styles.textButtonContainer} style={styles.textButton} onPress={handlePressOnetime}>
        Onetime donation
      </TextButton>
      <TextButton containerStyle={styles.textButtonContainer} style={styles.textButton} onPress={handlePressMonthly}>
        Monthly donation
      </TextButton>
    </View>
  );

  return (
    <ScrollView contentContainerStyle={styles.scrollView} alwaysBounceVertical={false}>
      <View style={styles.screen}>
        <ImageBackground source={backgrounds.donateScreenImg()} style={styles.imageBackground}>
          <View style={styles.screenSpacerTop} />
          <View style={styles.screenSpacerFlexGrow} />

          <View style={localStyles.donatePaper}>
            {DonateText}
            {DonateButtons}
            {PaymentText}
          </View>

          <View style={styles.screenSpacerFlexGrow} />
          {isIOS() && (
            <View style={styles.navButtonsContainer}>
              <GoBackButton onPress={props.handleBackPress!} />
            </View>
          )}
          <View style={styles.screenSpacerBottom} />
        </ImageBackground>
      </View>
    </ScrollView>
  );
};

export default DonateScreen;
