import store from '../store/store';
import { setQuoteNotifStatus } from '../store/actions/settingsActions';
import { setQuote, setActiveScreen } from '../store/actions/appActions';
import { pushNextQuoteNotification } from './pushNextQuoteNotification';
import { Quote } from '../types/types';

export const dispatchNavigateThenPushNextNotif = (quote: Quote) => {
  const quotesDailyState = store.getState().settings.quotesDaily;
  store.dispatch(setQuote(quote));
  store.dispatch(setActiveScreen({ screen: 'QUOTES' }));
  if (quotesDailyState === 0) {
    // prettier-ignore
    global.log("[dispatchNavigateThenPushNextNotif] -> setQuoteNotifStatus('Disabled')", quotesDailyState).redOnBlack();
    store.dispatch(setQuoteNotifStatus('Disabled'));
    return;
  }
  pushNextQuoteNotification({ quotesDaily: quotesDailyState, quote });
};
