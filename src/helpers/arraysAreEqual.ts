export function arraysAreEqual<T>(arr1: Array<T>, arr2: Array<T>) {
  if (!Array.isArray(arr1) || !Array.isArray(arr2) || arr1.length !== arr2.length) return false;

  const array1 = arr1.sort();
  const array2 = arr2.sort();

  for (let i = 0; i < array1.length; i++) {
    if (array1[i] !== array2[i]) return false;
  }

  return true;
}
