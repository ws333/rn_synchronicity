import React, { FC, ReactElement } from 'react';
import useTheme from '../hooks/useTheme';
import TextButtonAppTheme from './TextButtonAppTheme';

type Props = {
  onPress: () => void;
};

const GoBackButton: FC<Props> = (props): ReactElement => {
  const { styles, colors, activeOpacity } = useTheme();

  return (
    <TextButtonAppTheme dark colors={colors} activeOpacity={activeOpacity} onPress={props.onPress} styles={styles}>
      Go back
    </TextButtonAppTheme>
  );
};

export default GoBackButton;
