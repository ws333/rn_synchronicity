const httpError = 'HTTP error ';

export default {
  fetchTimeout: 'AbortError: Aborted',
  httpError: httpError,
  httpError404: `Error: ${httpError}404`,
  networkRequestFailed: 'TypeError: Network request failed',
  protocolNotSupported: 'Protocol not supported',
  quotesLengthZero: 'await response.json() -> quotesSource.quotes length is 0',
  syntaxErrorJsonParse: 'SyntaxError: JSON.parse',
};
