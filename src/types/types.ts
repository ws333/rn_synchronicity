import { ModalBaseProps } from 'react-native';
import { PURCHASE_TYPE } from 'react-native-purchases';
import { ColorKeys } from '../styles/themeColors';

export const EnumPurchaseType = PURCHASE_TYPE;

export type AnimationType = ModalBaseProps['animationType'];
export type Notification = { title: string; message: string; date: Date };
export type PurchaseType = PURCHASE_TYPE;
export type QuoteNotification = { quote: Quote; date: Date };
export type ThemeColors = { [k in ColorKeys]: string };
export type TimePickerType = 'start' | 'end' | null;
export type Torientation = 'PORTRAIT' | 'LANDSCAPE';

export class MenuItemBase {
  constructor(public key: string, public title: string, public description: string, public image?: any) {}
}

export class MenuItemHomeScreen extends MenuItemBase {
  constructor(
    public key: string,
    public title: string,
    public description: string,
    public route: Screen,
    public image?: any,
  ) {
    super(key, title, description, image);
  }
}

export class MenuItemBrainWaveScreen extends MenuItemBase {
  constructor(
    public key: string,
    public title: string,
    public description: string,
    public videoId: string,
    public image?: any,
  ) {
    super(key, title, description, image);
  }
}

export type MenuItem = MenuItemHomeScreen | MenuItemBrainWaveScreen;

export interface OnlineQuotesObject {
  name: string;
  subtitle?: string;
  url: string;
  imageUrl?: string;
  quotes: string[];
}

export type Quote = { source: string; text: string };
export type QuotesList = { [k in QuotesListKeys | string]: string[] };
export type QuotesListKeys =
  | 'Abraham-Hicks'
  | 'Albert Einstein'
  | 'Arjun of the Yahyel'
  | 'Nassim Haramein'
  | 'Nikola Tesla'
  // | 'MegaQuote' // Used for testing
  | 'Mooji'
  | 'Rumi';
export type QuotesSourceCategory = 'In-app' | 'Online';
export type QuotesSources = { [k in QuotesListKeys | string]: QuotesSource };
export type QuotesSourcesInApp = { [k in QuotesListKeys]: QuotesSourceInApp };
export type Status = 'Enabled' | 'Disabled';
export type StoredNotification = { quote: Quote; date: string; dispatched?: boolean };
export type UpdateStartEndTime = { newStartTime?: Date; newEndTime?: Date };

interface QuotesSourceBase {
  name: string;
  subtitle: string;
  category: QuotesSourceCategory;
}

export interface QuotesSource extends QuotesSourceBase {
  url?: string;
  imageUrl?: string;
  updatedDate?: string;
  enabled?: boolean;
}

export interface QuotesSourceInApp extends QuotesSourceBase {
  quotes: string[];
}

export interface Schedule {
  startHours: number;
  startMins: number;
  endHours: number;
  endMins: number;
}

export type ScreenProps = {
  handleBackPress?: () => void;
  openModal?: boolean;
  productIDs?: string[];
  purchaseType?: PurchaseType;
};

export type Screen =
  | 'ADD_QUOTES_SOURCE'
  | 'HOME'
  | 'BRAINWAVE'
  | 'DONATE'
  | 'FAQ'
  | 'LOG'
  | 'MANAGE_QUOTES_SOURCES'
  | 'PURCHASE_DONATION'
  | 'QUOTES'
  | 'SETTINGS'
  | 'TEST'
  | 'EXIT';

export interface ToastOptions {
  type: 'success' | 'error' | 'info' | 'hidden';
  position?: 'top' | 'bottom';
  text1: string;
  text2: string;
  visibilityTime?: number;
  autoHide?: boolean;
  topOffset?: number;
  bottomOffset?: number;
  props?: { [key: string]: any };
  onShow?: () => void;
  onHide?: () => void;
  onPress?: () => void;
  isShowing?: boolean;
}

export interface ToastParams extends Omit<ToastOptions, 'type' | 'text1' | 'text2'> {
  type?: ToastOptions['type'];
  title: string;
  message: string;
}

export type ToastState = ToastOptions | undefined;
