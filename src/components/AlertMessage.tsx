import React, { FC, ReactElement } from 'react';
import { View } from 'react-native';
import { Overlay, Text } from 'react-native-elements';
import AlertButtons from './AlertButtons';
import { Props } from '../types/typesAlerts';
import { handleBackPress } from '../helpers/handleBackPress';
import { LocalStyles } from '../styles/alertStyles';
import { constants } from '../constants/constants';

const Alert: FC<Props> = (props): ReactElement => {
  const { alertState, animationType, styles, colors } = props;

  const showConfirm = alertState?.showConfirm ?? true;
  const confirmText = alertState?.confirmText ?? 'Close';
  const showCancel = alertState?.showCancel ?? false;
  const cancelText = alertState?.cancelText ?? 'Cancel';

  const localStyles: LocalStyles = require('../styles/alertStyles')(colors);

  return (
    <Overlay
      animationType={animationType ?? 'none'}
      isVisible={!!alertState}
      onRequestClose={() => {
        handleBackPress();
      }}
      overlayStyle={localStyles.overlay}
      backdropStyle={localStyles.backdrop}
      onBackdropPress={alertState?.handleOnPressBackdrop}
      statusBarTranslucent={true}
      supportedOrientations={constants.supportedOrientations}>
      <View style={localStyles.alertContainer}>
        {!!alertState?.title && <Text style={localStyles.title}>{alertState?.title}</Text>}
        {!!alertState?.message && <Text style={localStyles.messsage}>{alertState?.message}</Text>}
        <AlertButtons
          alertState={alertState}
          styles={styles}
          localStyles={localStyles}
          showConfirm={showConfirm}
          confirmText={confirmText}
          showCancel={showCancel}
          cancelText={cancelText}
        />
      </View>
    </Overlay>
  );
};

export default Alert;
