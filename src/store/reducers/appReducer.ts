/* eslint-disable no-case-declarations */
import { Dimensions } from 'react-native';
import Orientation from 'react-native-orientation-locker';
import {
  SET_ACTIVE_SCREEN,
  SET_ALERT,
  GOTO_PREVIOUS_SCREEN,
  SET_ORIENTATION,
  SET_QUOTE,
  SET_QUOTES,
  SET_QUOTE_IS_WAITING,
  SET_SELECTED_LIST_ITEM,
  SET_TOAST,
  SET_ADD_SOURCE_BUTTON_HEIGHT,
  AppActionTypes,
  AppReducerState,
} from '../../types/typesRedux';
import { Screen } from '../../types/types';
import { lastInArray } from '../../helpers/lastInArray';
import { constants } from '../../constants/constants';

const initialState: AppReducerState = {
  activeScreen: { screen: 'HOME' },
  alert: null,
  quote: constants.defaults.quote,
  quotes: [],
  history: ['HOME'],
  orientation: Orientation.getInitialOrientation() === 'PORTRAIT' ? 'PORTRAIT' : 'LANDSCAPE',
  dimensions: Dimensions.get('screen'),
  selectedListItem: '',
};

// eslint-disable-next-line default-param-last
const appReducer = (state = initialState, action: AppActionTypes): AppReducerState => {
  global.log('[appReducer] -> action =', action).redOnYellow();
  let newHistory = [];
  switch (action.type) {
    case GOTO_PREVIOUS_SCREEN:
      if (state.history.length === 1) {
        return { ...state, activeScreen: { screen: 'EXIT' } };
      }
      newHistory = [...state.history];
      newHistory.pop();
      const previousScreen: Screen = lastInArray(newHistory);
      return { ...state, activeScreen: { screen: previousScreen }, history: newHistory };

    case SET_ACTIVE_SCREEN:
      const currentScreen = lastInArray(state.history);
      newHistory = action.value.screen === currentScreen ? state.history : [...state.history, action.value.screen];
      return { ...state, activeScreen: action.value, history: newHistory };

    case SET_ALERT:
      return { ...state, alert: action.value };

    case SET_ORIENTATION:
      const newState = {
        ...state,
        orientation: action.value,
        dimensions: Dimensions.get('screen'),
      };
      const { toast } = state;
      if (toast && toast.type !== 'hidden') {
        return {
          ...newState,
          toast: { ...toast, type: 'hidden' },
        };
      }
      return newState;

    case SET_QUOTE:
      return { ...state, quote: action.value };

    case SET_QUOTE_IS_WAITING:
      return { ...state, quoteIsWaiting: action.value };

    case SET_QUOTES:
      return { ...state, quotes: action.value };

    case SET_SELECTED_LIST_ITEM:
      return { ...state, selectedListItem: action.value };

    case SET_TOAST:
      return { ...state, toast: action.value };

    case SET_ADD_SOURCE_BUTTON_HEIGHT:
      return { ...state, addSourceButtonHeight: action.value };

    default:
      return state;
  }
};

export default appReducer;
