import Purchases from 'react-native-purchases';
import { constants } from '../constants/constants';

export default class IAP {
  static init: () => void;
}

IAP.init = () => {
  if (__DEV__) Purchases.setDebugLogsEnabled(true);
  Purchases.setup(constants.keys.revenueCatApiKey);
};
