import { logToStorage } from './logToStorage';
import { getTimeStamp } from '../helpers/time';

type Tcolor = 'white' | 'grey' | 'black' | 'red' | 'orange' | 'yellow' | 'green' | 'blue';
const defaultBackgroundColor: Tcolor = 'white';

export function logToConsoleWithColors(...args: any[]) {
  if (!('logLevel' in global)) {
    global.logLevel = 5;
  }

  function log(color: Tcolor, backgroundColor: Tcolor = defaultBackgroundColor) {
    if (global.logLevel) {
      console.log(
        `${getTimeStamp()} %c${args[0]}`,
        `color:${color};background-color:${backgroundColor}`,
        ...args.slice(1),
      );
    }

    /**
     *  Log to storage for debugging of release builds
     */
    if (global.loggingToStorage) {
      args.forEach((v, i) => {
        if (typeof v === 'object') {
          args[i] = JSON.stringify(v);
        }
      });
      const logItem = [...args].toString();
      logToStorage(logItem);
    }
  }

  function logLevel1(color: Tcolor, backgroundColor: Tcolor = defaultBackgroundColor) {
    if (global.logLevel >= 1) log(color, backgroundColor);
  }
  function logLevel2(color: Tcolor, backgroundColor: Tcolor = defaultBackgroundColor) {
    if (global.logLevel >= 2) log(color, backgroundColor);
  }
  function logLevel3(color: Tcolor, backgroundColor: Tcolor = defaultBackgroundColor) {
    if (global.logLevel >= 3) log(color, backgroundColor);
  }
  function logLevel4(color: Tcolor, backgroundColor: Tcolor = defaultBackgroundColor) {
    if (global.logLevel >= 4) log(color, backgroundColor);
  }
  function logLevel5(color: Tcolor, backgroundColor: Tcolor = defaultBackgroundColor) {
    if (global.logLevel >= 5) log(color, backgroundColor);
  }

  return {
    black: () => logLevel5('black'),
    green: () => logLevel4('green'),
    blue: () => logLevel4('blue'),
    red: () => logLevel4('red'),
    blackOnOrange: () => logLevel4('black', 'orange'),
    whiteOnGreen: () => logLevel3('white', 'green'),
    blackOnGreen: () => logLevel3('black', 'green'),
    yellowOnGreen: () => logLevel3('yellow', 'green'),
    whiteOnBlue: () => logLevel3('white', 'blue'),
    blackOnBlue: () => logLevel3('black', 'blue'),
    yellowOnBlue: () => logLevel3('yellow', 'blue'),
    whiteOnGrey: () => logLevel3('white', 'grey'),
    yellowOnGrey: () => logLevel3('yellow', 'grey'),
    greenOnYellow: () => logLevel3('green', 'yellow'),
    blueOnYellow: () => logLevel3('blue', 'yellow'),
    blackOnYellow: () => logLevel3('black', 'yellow'),
    redOnYellow: () => logLevel3('red', 'yellow'),
    whiteOnRed: () => logLevel2('white', 'red'),
    blackOnRed: () => logLevel2('black', 'red'),
    yellowOnRed: () => logLevel2('yellow', 'red'),
    whiteOnBlack: () => logLevel1('white', 'black'),
    greenOnBlack: () => logLevel1('green', 'black'),
    blueOnBlack: () => logLevel1('blue', 'black'),
    redOnBlack: () => logLevel1('red', 'black'),
    yellowOnBlack: () => logLevel1('yellow', 'black'),
  };
}
