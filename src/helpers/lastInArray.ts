export function lastInArray(arr: Array<any>) {
  return arr[arr.length - 1];
}
