import { QuotesSources } from '../types/types';

export function filterOutQuotes(qs: QuotesSources) {
  const qslKeys = Object.keys(qs);
  const quotesSources: QuotesSources = {};

  qslKeys.forEach((key) => {
    quotesSources[key] = {
      name: qs[key].name,
      subtitle: qs[key].subtitle,
      category: qs[key].category,
      imageUrl: qs[key].imageUrl,
    };
  });

  return quotesSources;
}
