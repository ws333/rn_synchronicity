import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import { isIOS } from './platform';

export const cancelAllLocalNotifications = () => {
  PushNotification.cancelAllLocalNotifications();
  if (isIOS()) {
    PushNotificationIOS.setApplicationIconBadgeNumber(0);
    PushNotificationIOS.removeAllDeliveredNotifications();
  }
};
