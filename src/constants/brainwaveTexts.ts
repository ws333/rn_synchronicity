export const chooseSessionText =
  "Choose a brainwave session below or tap 'Info' to get more information about the sessions. " +
  'Heaphones are only required for binaural versions, but can give a deeper experience with the other sessions as well. ' +
  '\n\nThe stream versions include the sound of a small stream and birdsong.';

export const brainwaveInfoText =
  'The brainwave sessions are based on the idea of the different ranges of frequencies that are prominent ' +
  'in the brain in states like deep sleep, meditation, creativity, mental activity and so on.' +
  '\n\nThe 15 minutes long sessions starts at epsilon 0.5Hz, gradually rise up to high lambda at 200Hz ' +
  'and then fall back down to epsilon again towards the end. ' +
  'This creates a circle through all brainwave ranges, and can help to harmonize, synchronize and amplify ' +
  'the vibrational frequencies of the neurological net in the brain for the different brainwave ranges. ' +
  'I.e. the epsilon, delta, theta, alpha, beta, gamma and lambda range which all are associated with different ' +
  'states like deep meditation, deep sleep, high creativity, activity, high focus, high awareness, high flow states. ' +
  'There is a bell sound that let you know when the next range of brainwaves begins, ' +
  'though it is quite faint in the versions with ambient soundscapes.' +
  '\n\nThe binaural versions require headphones, but the monaural versions can be used on any sound system. ' +
  'If you feel the volume of the brainwave sound is to low in the versions with ambient sounds, headphones can help get the right balance.' +
  '\n\nThe effect can feel like a rebalancing of all the layers of brainwave states that oscillate in the brain in different amounts. ' +
  '\n\nIt is suggested to do this only once a day, as it can be to activating to do more. ' +
  '\n\nSit in a comfortable position or lay down, take three deep breaths and enjoy a refreshing inner experience...';
