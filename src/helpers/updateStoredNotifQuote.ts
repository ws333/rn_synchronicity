import store from '../store/store';
import { Quote, Notification } from '../types/types';
import { calcNextNotifTime } from './calcNextNotifTime';
import { scheduleLocalNotification } from './scheduleLocalNotification';
import { cancelAllLocalNotifications } from './cancelAllLocalNotifications';
import { getStoredQuoteNotification, storeQuoteNotification } from './quoteNotificationStorage';

export async function updateStoredNotifQuote(newQuote: Quote) {
  cancelAllLocalNotifications();

  const storedNotif = await getStoredQuoteNotification();
  if (!storedNotif) return;
  const dateString = storedNotif.date;
  let date = new Date(dateString);

  /**
   * Calc new date if in the past to avoid new notifications being pushed instantly when user change enabled quotes sources
   */
  if (date.getTime() < Date.now()) {
    const { quoteNotifSchedule } = store.getState().settings;
    const quotesDailyState = store.getState().settings.quotesDaily;
    const nextNotifTime = await calcNextNotifTime(quoteNotifSchedule, quotesDailyState);
    date = new Date(nextNotifTime);
  }

  storeQuoteNotification(newQuote, date);
  const newNotif: Notification = { title: newQuote.source, message: newQuote.text, date: new Date(date) };
  scheduleLocalNotification(newNotif);
}
