import { gotoPreviousScreen, setAlert } from '../store/actions/appActions';
import store from '../store/store';

export const handleBackPress = () => {
  store.dispatch(gotoPreviousScreen());
  store.dispatch(setAlert(null));
  return true;
};
