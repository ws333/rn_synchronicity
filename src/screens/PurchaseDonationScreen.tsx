import React, { FC, ReactElement, useEffect } from 'react';
import { ImageBackground, View } from 'react-native';
import { ScreenProps } from '../types/types';
import ProductsList from '../components/ProductsList';
import GoBackButton from '../components/GoBackButton';
import useTheme from '../hooks/useTheme';
import useScreenDimensions from '../hooks/useScreenDimensions';
import { isAndroid, isIOS } from '../helpers/platform';
import { LocalStyles } from '../styles/purchaseDonationScreenStyles';
import { backgrounds } from '../constants/images';
import { alertAppException } from '../helpers/alertsExceptions';
import store from '../store/store';
import { setActiveScreen } from '../store/actions/appActions';

const PurchaseDonationScreen: FC<ScreenProps> = (props): ReactElement => {
  global.log('[PurchaseDonationScreen] -> rendering... -> props', { ...props }).green();

  useEffect(() => {
    if (!props.purchaseType) {
      const error = 'PurchaseDonationScreen: props.purchaseType is required, but not specified.';
      global.log(error).yellowOnBlack();
      store.dispatch(setActiveScreen({ screen: 'HOME' }));
      const cb = () => {};
      alertAppException(cb, error);
    }
  });

  const { activeOpacity, colors, styles } = useTheme();
  const { isSmallScreenHeight } = useScreenDimensions();

  const localStyles: LocalStyles = require('../styles/purchaseDonationScreenStyles')(colors);
  if (isAndroid() && isSmallScreenHeight) {
    styles.screenSpacerBottom = { height: Number(styles.screenSpacerBottom?.height) * 2 };
  }

  function handleOnPressGoBackButton() {
    if (props.handleBackPress) {
      props.handleBackPress();
    }
  }

  return (
    <View style={styles.screen}>
      <ImageBackground source={backgrounds.settingsScreenImg()} style={styles.imageBackground}>
        <View style={styles.screenOverlay}>
          <View style={styles.screenSpacerTop} />
          <View style={localStyles.paper}>
            <View style={localStyles.content}>
              <ProductsList
                activeOpacity={activeOpacity}
                colors={colors}
                productIDs={props.productIDs ?? []}
                purchaseType={props.purchaseType!}
                styles={styles}
              />
            </View>
          </View>
          {isIOS() && (
            <View style={styles.navButtonsContainer}>
              <GoBackButton onPress={handleOnPressGoBackButton} />
            </View>
          )}
          <View style={styles.screenSpacerBottom} />
        </View>
      </ImageBackground>
    </View>
  );
};

export default PurchaseDonationScreen;
