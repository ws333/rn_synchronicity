import { TextStyle, ViewStyle } from 'react-native';
import store from '../store/store';
import { ThemeColors } from '../types/types';
import { styleConstants } from './globalStyles';

export interface LocalStyles {
  alertContainer: ViewStyle;
  backdrop: ViewStyle;
  buttonContainer: ViewStyle;
  overlay: ViewStyle;
  title: TextStyle;
  subTitle?: TextStyle;
  messsage: TextStyle;
}

module.exports = (colors: ThemeColors): LocalStyles => {
  const dimensionState = store.getState().app.dimensions;
  return {
    alertContainer: {
      width: styleConstants.paperWidth,
      alignItems: 'center',
      marginBottom: dimensionState.height / 8,
      padding: styleConstants.padding,
      paddingHorizontal: styleConstants.padding * 1.5,
      paddingBottom: styleConstants.padding / 2,
      borderRadius: styleConstants.radius,
      backgroundColor: colors.background,
      shadowColor: colors.background,
      elevation: 3,
      ...styleConstants.shadow17,
    },
    backdrop: {
      backgroundColor: colors.backdropOverlay,
    },
    buttonContainer: {
      flexDirection: 'row',
      marginTop: styleConstants.marginLRTB,
    },
    overlay: {
      justifyContent: 'center',
      alignItems: 'center',
      elevation: 0,
      backgroundColor: 'transparent',
    },
    title: {
      fontSize: styleConstants.fontSizeXLarge,
      color: colors.alertTitle,
    },
    subTitle: {},
    messsage: {
      marginTop: -styleConstants.marginLRTB,
      textAlign: 'center',
      fontSize: styleConstants.fontSizeBase,
      color: colors.text,
    },
  };
};
