import React, { FC, ReactElement } from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import Button from 'react-native-button';
import { useDispatch } from 'react-redux';
import { setActiveScreen, setQuote } from '../store/actions/appActions';
import { Quote as TQuote, ThemeColors } from '../types/types';
import { getRandomQuote } from '../helpers/getRandomQuote';
import { styleConstants } from '../styles/globalStyles';
import { LocalStyles } from '../styles/quoteStyles';
import { constants } from '../constants/constants';

type Props = {
  quote: TQuote;
  activeOpacity: number;
  colors: ThemeColors;
};

const Quote: FC<Props> = (props): ReactElement => {
  global.log('[Quote] -> Component rendering... props.quote =', props.quote).green();

  const dispatch = useDispatch();

  const { activeOpacity, colors } = props;
  let { quote } = props;

  if (!quote) {
    quote = constants.quotes.noQuotesExistsForEnabledSources;
  }

  const localStyles: LocalStyles = require('../styles/quoteStyles')(colors);

  const handleOnPressNewQuote = () => {
    const _quote = getRandomQuote({ currentQuote: quote });
    dispatch(setQuote(_quote));
  };

  const handleOnPressGotoManage = () => {
    dispatch(setActiveScreen({ screen: 'MANAGE_QUOTES_SOURCES' }));
  };

  const genericButton = (buttonText: string, onPress: () => void) => (
    <View style={localStyles.quoteButtonContainer}>
      <Button style={localStyles.quoteButton} onPress={onPress} activeOpacity={activeOpacity}>
        {buttonText}
      </Button>
    </View>
  );

  const ScreenSpacer = <View style={{ height: styleConstants.screenSpacer }} />;

  const SourceAIButton =
    quote?.text === constants.quotes.noEnabledQoutesSources.text
      ? genericButton('Go to Manage quote sources', handleOnPressGotoManage)
      : ScreenSpacer;

  const QuoteButton = (
    <View>
      {quote?.source === constants.quotes.sourceAI
        ? SourceAIButton
        : genericButton('Tap to get new quote', handleOnPressNewQuote)}
    </View>
  );

  return (
    <TouchableHighlight
      underlayColor={localStyles.quotePaper.backgroundColor}
      style={localStyles.quotePaper}
      onPress={handleOnPressNewQuote}>
      <View>
        <Text style={localStyles.quoteText}>{quote?.text}</Text>
        <Text style={localStyles.quoteSource}>- {quote?.source} -</Text>

        {QuoteButton}
      </View>
    </TouchableHighlight>
  );
};

export default Quote;
