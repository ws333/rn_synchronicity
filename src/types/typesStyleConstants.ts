import { ViewStyle } from 'react-native';
import { AvatarProps } from 'react-native-elements';

export interface StyleConstants {
  avatarSize: AvatarProps['size'];
  borderWidth: number;
  fontSizeSmall: number;
  fontSizeBase: number;
  fontSizeMedium: number;
  fontSizeLarge: number;
  fontSizeXLarge: number;
  headerPaperWidth: string;
  infoSectionWidth: string;
  infoSectionWidthSmallScreen: string;
  listWidth: string;
  logTextFontSize: number;
  logPaperWidth: string;
  marginLRTB: number;
  marginVH: number;
  menuGridItemsOffsetTopLeft: number;
  menuGridItemsOffsetBottomRight: number;
  padding: number;
  paperWidth: string;
  quotesSourceWidth: string;
  radius: number;
  screenSpacer: number;
  smallScreenSpacerBottomFactor: number;
  textOpacity: number;
  shadow07: ViewStyle;
  shadow11: ViewStyle;
  shadow17: ViewStyle;
  shadow22: ViewStyle;
  shadow07NoOffset: ViewStyle;
  shadow11NoOffset: ViewStyle;
  shadow17NoOffset: ViewStyle;
  shadow22NoOffset: ViewStyle;
}
