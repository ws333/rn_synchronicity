import {
  SET_COLOR_SCHEME,
  SET_IS24HOUR,
  SET_ISHYDRATED,
  SET_FOLLOW_DEVICE_COLOR_SCHEME,
  SET_FOLLOW_DEVICE_TIME_FORMAT,
  SET_QUOTES_DAILY,
  SET_QUOTE_NOTIF_START_TIME,
  SET_QUOTE_NOTIF_END_TIME,
  SET_QUOTE_NOTIF_STATUS,
  SET_QUOTES_SOURCES,
  HYDRATE_SETTINGS_FROM_STORAGE,
  SettingsActionTypes,
  SettingsReducerState,
} from '../../types/typesRedux';
import { getNotifSchedule } from '../storehelpers/getNotifSchedule';
import { filterOutQuotes } from '../../helpers/filterOutQuotes';
import { getThemeColors } from '../../helpers/getThemeColors';
import { storeData } from '../../services/storage';
import { constants } from '../../constants/constants';

const { quoteNotifStartTime } = constants.defaults;
const { quoteNotifEndTime } = constants.defaults;

const initialState: SettingsReducerState = {
  colorScheme: constants.defaults.colorScheme,
  followDeviceColorScheme: constants.defaults.followDeviceColorScheme,
  followDeviceTimeFormat: constants.defaults.followDeviceTimeFormat,
  is24Hour: constants.defaults.is24Hour,
  quotesDaily: constants.defaults.quotesDaily,
  quoteNotifStartTime: quoteNotifStartTime,
  quoteNotifEndTime: quoteNotifEndTime,
  quoteNotifSchedule: getNotifSchedule({ quoteNotifStartTime, quoteNotifEndTime }),
  quoteNotifStatus: constants.defaults.quoteNotifStatus,
  quotesSources: filterOutQuotes(constants.defaults.quotesSources),
  themeColors: getThemeColors(constants.defaults.colorScheme),
};
global.log('[settingsReducer] -> initialState =', initialState).redOnYellow();

// eslint-disable-next-line default-param-last
const settingsReducer = (state = initialState, action: SettingsActionTypes): SettingsReducerState => {
  global.log('[settingsReducer] -> action =', action).redOnYellow();

  switch (action.type) {
    case SET_COLOR_SCHEME:
      if (action.saveToStorage) storeData('COLOR_SCHEME', action.value);
      return { ...state, colorScheme: action.value, themeColors: getThemeColors(action.value) };

    case SET_FOLLOW_DEVICE_COLOR_SCHEME:
      if (action.saveToStorage) storeData('FOLLOW_DEVICE_COLOR_SCHEME', action.value);
      return { ...state, followDeviceColorScheme: action.value };

    case SET_FOLLOW_DEVICE_TIME_FORMAT:
      if (action.saveToStorage) storeData('FOLLOW_DEVICE_TIME_FORMAT', action.value);
      return { ...state, followDeviceTimeFormat: action.value };

    case SET_IS24HOUR:
      if (action.saveToStorage) storeData('IS_24_HOUR', action.value);
      return { ...state, is24Hour: action.value };

    case SET_ISHYDRATED:
      return { ...state, isHydrated: action.value };

    case SET_QUOTES_DAILY:
      if (action.saveToStorage) {
        storeData('QUOTE_NOTIF_PREV_CALC_PERIOD', -1); // Resetting prevCalcPeriod since number of periods will change
        storeData('QUOTES_DAILY', action.value);
      }
      return { ...state, quotesDaily: action.value };

    case SET_QUOTE_NOTIF_START_TIME: {
      if (action.saveToStorage) storeData('QUOTE_NOTIF_START_TIME', action.value);
      const newState: SettingsReducerState = { ...state, quoteNotifStartTime: action.value };
      return { ...newState, quoteNotifSchedule: getNotifSchedule(newState) };
    }

    case SET_QUOTE_NOTIF_END_TIME: {
      if (action.saveToStorage) storeData('QUOTE_NOTIF_END_TIME', action.value);
      const newState: SettingsReducerState = { ...state, quoteNotifEndTime: action.value };
      return { ...newState, quoteNotifSchedule: getNotifSchedule(newState) };
    }

    case SET_QUOTE_NOTIF_STATUS:
      if (action.saveToStorage) storeData('QUOTE_NOTIF_STATUS', action.value);
      return { ...state, quoteNotifStatus: action.value };

    case SET_QUOTES_SOURCES:
      if (action.saveToStorage) storeData('QUOTES_SOURCES', action.value);
      return { ...state, quotesSources: action.value };

    case HYDRATE_SETTINGS_FROM_STORAGE: // Happens only on app launch so put last
      global.log('[settingsReducer] -> HYDRATE_SETTINGS_FROM_STORAGE -> action.value =', action.value).redOnYellow();
      return {
        ...state,
        colorScheme: action.value.colorScheme,
        followDeviceColorScheme: action.value.followDeviceColorScheme,
        followDeviceTimeFormat: action.value.followDeviceTimeFormat,
        is24Hour: action.value.is24Hour,
        quotesDaily: action.value.quotesDaily,
        quoteNotifStartTime: action.value.quoteNotifStartTime,
        quoteNotifEndTime: action.value.quoteNotifEndTime,
        quoteNotifSchedule: action.value.quoteNotifSchedule,
        quoteNotifStatus: action.value.quoteNotifStatus,
        quotesSources: action.value.quotesSources,
        themeColors: action.value.themeColors,
        isHydrated: true,
      };

    default:
      return state;
  }
};

export default settingsReducer;
