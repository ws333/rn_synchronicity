import React, { Dispatch, FC, ReactElement, SetStateAction, useState } from 'react';
import { LayoutChangeEvent, View, ViewStyle } from 'react-native';
import YouTube, { YouTubeProps } from 'react-native-youtube';
import store from '../store/store';
import { setAlert } from '../store/actions/appActions';
import { AlertState } from '../types/typesAlerts';
import { ThemeColors } from '../types/types';
import SpinnerThreeBounce from './SpinnerThreeBounce';
import { constants } from '../constants/constants';

/**
 * Note! The YouTubeProps interface in node_modules\react-native-youtube\main.d.ts has been modified to avoid typescript errors for missing apiKey.
 * For now a script (cp_apiKey.sh) is run on postinstall (see package.json) that overwrites node_modules/react-native-youtube/main.d.ts with a modified version.
 * Remember to disable this when a fix is released, or update main.d.ts if not fixed in next version.
 * See https://github.com/davidohayon669/react-native-youtube/issues/452 & https://github.com/davidohayon669/react-native-youtube/issues/471
 */
interface Props extends YouTubeProps {
  colors: ThemeColors;
  containerStyle: ViewStyle;
  setVideoState: Dispatch<SetStateAction<{ videoId: string }>>;
  spinnerSize: number;
  style: ViewStyle;
  videoState: {
    videoId: string;
  };
}

interface Error {
  error: string;
}

const errorText = {
  alertTitle: 'Not able to play the video:',
  messageYoutubePlayerViewTooSmall:
    'The screen height is to small.\n\nIf viewing in landscape mode, try rotating the device to portrait mode and restart the video.',
};

const YouTubeVideo: FC<Props> = (props): ReactElement => {
  const { colors, containerStyle, setVideoState, spinnerSize, style, videoState } = props;

  const [showYouTube, setShowYouTube] = useState(false);
  const [youTubeContainerHeight, setYouTubeContainerHeight] = useState(0);

  const handleOnError = (e: Error) => {
    setVideoState({ videoId: '' });

    const message = e.error === 'PLAYER_VIEW_TOO_SMALL' ? errorText.messageYoutubePlayerViewTooSmall : e.error;
    const newAlert: AlertState = {
      type: 'Exception',
      title: errorText.alertTitle,
      message: message,
      showCancel: false,
      confirmText: 'Close',
      handleOnPressConfirm: () => store.dispatch(setAlert(null)),
    };
    store.dispatch(setAlert(newAlert));
  };

  function handleOnLayoutYouTubeContainer(event: LayoutChangeEvent) {
    const { height } = event.nativeEvent.layout;
    setYouTubeContainerHeight(height);
  }

  const youTubeHeight = youTubeContainerHeight - Number(containerStyle.borderWidth) * 2 + 1;

  return (
    <>
      {!showYouTube && <SpinnerThreeBounce isVisible={true} spinnerSize={spinnerSize} colors={colors} noModal={true} />}
      <View
        style={{
          ...containerStyle,
          borderWidth: showYouTube ? containerStyle.borderWidth : 0,
        }}
        onLayout={handleOnLayoutYouTubeContainer}>
        <YouTube
          apiKey={constants.keys.googleApiKey}
          videoId={videoState.videoId}
          play
          rel={false}
          onError={handleOnError}
          onReady={(_e) => setShowYouTube(true)}
          style={{
            ...style,
            height: showYouTube ? youTubeHeight : 0,
          }}
        />
      </View>
    </>
  );
};

export default YouTubeVideo;
