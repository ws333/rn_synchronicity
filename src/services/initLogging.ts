/**
 * global.log (logToConsoleWithColors) is used to log to terminal with colors
 * logToConsoleWithColors.ts use global.logLevel to filter what to log
 * set desired logLevel for different colors in logToConsoleWithColors.ts
 *
 * Usage example:
 * global.log('text to log'[, variable(s) to output]).greenOnYellow();
 *
 * global.logLevel: 0 = off, 1 = minimum, 5 = all
 * See logToConsoleWithColors.ts for which colors that are supported, and which logLevel they belong to.
 */
import { logToConsoleWithColors } from './logToConsoleWithColors';
import { getStoredLogLevel } from './logToStorage';

if (__DEV__) {
  global.loggingToStorage = true;
}
global.logLevel = 2;
global.log = logToConsoleWithColors;

console.log('[initLogging] -> default global.logLevel (before fetching stored value async) =', global.logLevel);

/**
 * Until top level await is supported for React Native (or another technique is found) this will do...
 * I.e. some logging during app start will not be with stored logLevel, but with default logLevel.
 * Modify constants.defaults.LOG_LEVEL to get desired logLevel on startup.
 */
(async () => {
  if (global.loggingToStorage) {
    global.logLevel = (await getStoredLogLevel()) ?? global.logLevel;
    console.log('[initLogging] -> global.logLevel set to stored value:', global.logLevel);
  }
})();
