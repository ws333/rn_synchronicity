import { AddUpdateSourceCallback, Alert, AlertState } from '../types/typesAlerts';

type Callback = AddUpdateSourceCallback;

const signature = '\n\n - App AI -';

export function AlertDefault(callback: any, errorText: string) {
  const title = 'An unspecified error has occured...';
  const message = `\n${errorText}` + signature;
  setupAlert({ callback, title, message, confirmText: 'Close', showCancel: false });
}

export function AlertNetworkRequestFailed(callback: Callback) {
  const title = "I'm not able to download the specified online quote source file";
  const message = '\nPlease check if your device is online and that the specified url is valid' + signature;
  setupAlert({ callback, title, message });
}

export function AlertProtocolNotSupported(callback: Callback, protocol: string) {
  console.log('file: alerts.ts -> AlertProtocolNotSupported -> protocol', protocol);
  const title = `Protocol "${protocol}" is not supported`;
  const message = '\nPlease check if the url is correct' + signature;
  setupAlert({ callback, title, message });
}

export function AlertHttpError404(callback: Callback) {
  const title = 'The file was not found';
  const message = '\nPlease check if the url is correct' + signature;
  setupAlert({ callback, title, message });
}

export function AlertSyntaxErrorJsonParse(callback: Callback, error: string) {
  const title = 'The downloaded json file contains a syntax error';
  const message =
    `\nIf possible please check the format of the file specified\n\nThe syntax error is:\n${error}\n\n${quotesSourceJsonFormat}` +
    signature;
  setupAlert({ callback, title, message });
}

export function AlertQuotesLengthZero(callback: Callback) {
  const title = 'The online qoutes file is not valid';
  const message = `\nThe quotes array is missing\n\n${quotesSourceJsonFormat}` + signature;
  setupAlert({ callback, title, message });
}

interface SetupAlertArgs extends Alert {
  callback: Callback;
}

/**
 * Set up AlertState object which is then passed into Alert component by setAlertState in callback function.
 */
function setupAlert({ callback, title, message }: SetupAlertArgs) {
  const alert: AlertState = { type: 'Exception', title, message };
  callback({ alert });
}

const quotesSourceJsonFormat =
  'The specification for the json file is:\n' +
  '{\n' +
  '"name": "Mastery",\n' +
  '"subtitle": "Know thyself",\n' +
  '"imageUrl": "https://webaddress/some.jpg",\n' +
  '"quotes": ["All are One", "Just Be", "etc"]\n' +
  '}\n\n' +
  'Note that all fields are case sensitive and that the quotes field is required.\n';
