import { useEffect } from 'react';
import { Dimensions } from 'react-native';
import Orientation from 'react-native-orientation-locker';
import store from '../store/store';
import { setOrientation } from '../store/actions/appActions';
import { getIsPortrait } from '../helpers/screenDimensions';
import { Torientation } from '../types/types';

const useOrientation = () => {
  global.log('[useOrientation] -> start executing...').green();

  /**
   * Unlock orientation
   * Needed after sending app to background when watching YouTube video in fullscreen
   */
  Orientation.unlockAllOrientations();

  useEffect(() => {
    global.log('[useOrientation] -> useEffect executing...').blue();

    /**
     * Handler is called twice when rotating iOS device
     * Only dispatching changed value to prevent unnecessary rerenders
     */
    const handleChange = () => {
      const { orientation } = store.getState().app;
      const newOrientation: Torientation = getIsPortrait() ? 'PORTRAIT' : 'LANDSCAPE';
      if (newOrientation !== orientation) {
        store.dispatch(setOrientation(newOrientation));
      }
    };

    Dimensions.addEventListener('change', handleChange);
    return () => {
      Dimensions.removeEventListener('change', handleChange);
    };
  }, []);
};

export default useOrientation;
