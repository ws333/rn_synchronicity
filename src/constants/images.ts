import store from '../store/store';

import imageManEarth from '../assets/images/backgrounds/manEarth.jpg';
import imageGirlWhite from '../assets/images/backgrounds/suhail-ra-kQ73K7pJKrA-unsplash.jpg';

import headerBrainwavesWhite from '../assets/headers/brainwaves_white.png';
import headerBrainwavesBlack from '../assets/headers/brainwaves_darkgreen.png';
import headerDonationsWhite from '../assets/headers/donations_white.png';
import headerDonationsBlack from '../assets/headers/donations_darkgreen.png';
import headerFAQWhite from '../assets/headers/FAQ_white.png';
import headerFAQBlack from '../assets/headers/FAQ_darkgreen.png';
import logoHomeScreenWhite from '../assets/logos/logo_alignment_white.png';
import logoHomeScreenDarkGreen from '../assets/logos/logo_alignment_darkGreen.png';

import menuItemBinauralDarkgreen from '../assets/images/menuitems/menuitem_binaural_darkgreen.png';
import menuItemBinauralWhite from '../assets/images/menuitems/menuitem_binaural_white.png';
import menuItemBinauralStreamDarkgreen from '../assets/images/menuitems/menuitem_binaural_stream_darkgreen.png';
import menuItemBinauralStreamWhite from '../assets/images/menuitems/menuitem_binaural_stream_white.png';
import menuItemBrainwavesDarkgreen from '../assets/images/menuitems/menuitem_brainwaves_darkgreen.png';
import menuItemBrainwavesWhite from '../assets/images/menuitems/menuitem_brainwaves_white.png';
import menuItemDonateDarkgreen from '../assets/images/menuitems/menuitem_donate_darkgreen.png';
import menuItemDonateWhite from '../assets/images/menuitems/menuitem_donate_white.png';
import menuItemFAQDarkgreen from '../assets/images/menuitems/menuitem_faq_darkgreen.png';
import menuItemFAQWhite from '../assets/images/menuitems/menuitem_faq_white.png';
import menuItemInfoDarkgreen from '../assets/images/menuitems/menuitem_info_darkgreen.png';
import menuItemInfoWhite from '../assets/images/menuitems/menuitem_info_white.png';
import menuItemManageQuotesDarkgreen from '../assets/images/menuitems/manage-quotes_darkgreen.png';
import menuItemManageQuotesWhite from '../assets/images/menuitems/manage-quotes_white.png';
import menuItemMonauralDarkgreen from '../assets/images/menuitems/menuitem_monaural_darkgreen.png';
import menuItemMonauralWhite from '../assets/images/menuitems/menuitem_monaural_white.png';
import menuItemMonauralStreamDarkgreen from '../assets/images/menuitems/menuitem_monaural_stream_darkgreen.png';
import menuItemMonauralStreamWhite from '../assets/images/menuitems/menuitem_monaural_stream_white.png';
import menuItemQuotesDarkgreen from '../assets/images/menuitems/menuitem_quotes_darkgreen.png';
import menuItemQuotesWhite from '../assets/images/menuitems/menuitem_quotes_white.png';
import menuItemSettingsDarkgreen from '../assets/images/menuitems/menuitem_settings_darkgreen.png';
import menuItemSettingsWhite from '../assets/images/menuitems/menuitem_settings_white.png';

import { Avatars } from '../types/typesConstants';
import defaultImg from '../assets/avatars/avatar-yin-yang.png';
import abeHicksImg from '../assets/avatars/avatar-abraham-hicks.png';
import albertEinsteinImg from '../assets/avatars/avatar-albert-einstein.png';
import moojiImg from '../assets/avatars/avatar-mooji.png';
import nassimHarmeinImg from '../assets/avatars/avatar-nassim-haramein.png';
import rumiImg from '../assets/avatars/avatar-rumi.png';
import nikolaTeslaImg from '../assets/avatars/avatar-nikola-tesla.png';
import yahyelImg from '../assets/avatars/avatar-yahyel.png';
import heartGreenImg from '../assets/avatars/avatar-heart-green.png';

// prettier-ignore
export const avatars: Avatars = {
  'default': defaultImg,
  'Abraham-Hicks': abeHicksImg,
  'Albert Einstein': albertEinsteinImg,
  'Arjun of the Yahyel': yahyelImg,
  // 'MegaQuote': defaultImg, // Used for testing
  'heartGreen': heartGreenImg,
  'Mooji': moojiImg,
  'Nassim Haramein': nassimHarmeinImg,
  'Rumi': rumiImg,
  'Nikola Tesla': nikolaTeslaImg,
};

const isDarkMode = () => {
  return store.getState().settings.colorScheme === 'dark';
};

export const backgrounds = {
  brainwaveScreenImg: () => (isDarkMode() ? imageManEarth : imageGirlWhite),
  donateScreenImg: () => (isDarkMode() ? imageManEarth : imageGirlWhite),
  FAQScreenImg: () => (isDarkMode() ? imageManEarth : imageGirlWhite),
  homeScreenImg: () => (isDarkMode() ? imageManEarth : imageGirlWhite),
  quoteScreenImg: () => (isDarkMode() ? imageManEarth : imageGirlWhite),
  settingsScreenImg: () => (isDarkMode() ? imageManEarth : imageGirlWhite),
};

export const headers = {
  brainwaveHeaderImg: () => (isDarkMode() ? headerBrainwavesWhite : headerBrainwavesBlack),
  donationsHeaderImg: () => (isDarkMode() ? headerDonationsWhite : headerDonationsBlack),
  FAQHeaderImg: () => (isDarkMode() ? headerFAQWhite : headerFAQBlack),
};

export const menuItemImgs = {
  menuItemBinaural: () => (isDarkMode() ? menuItemBinauralWhite : menuItemBinauralDarkgreen),
  menuItemBinauralStream: () => (isDarkMode() ? menuItemBinauralStreamWhite : menuItemBinauralStreamDarkgreen),
  menuItemBrainwaves: () => (isDarkMode() ? menuItemBrainwavesWhite : menuItemBrainwavesDarkgreen),
  menuItemDonate: () => (isDarkMode() ? menuItemDonateWhite : menuItemDonateDarkgreen),
  menuItemFAQ: () => (isDarkMode() ? menuItemFAQWhite : menuItemFAQDarkgreen),
  menuItemInfo: () => (isDarkMode() ? menuItemInfoWhite : menuItemInfoDarkgreen),
  menuItemManageQuotes: () => (isDarkMode() ? menuItemManageQuotesWhite : menuItemManageQuotesDarkgreen),
  menuItemMonaural: () => (isDarkMode() ? menuItemMonauralWhite : menuItemMonauralDarkgreen),
  menuItemMonauralStream: () => (isDarkMode() ? menuItemMonauralStreamWhite : menuItemMonauralStreamDarkgreen),
  menuItemQuotes: () => (isDarkMode() ? menuItemQuotesWhite : menuItemQuotesDarkgreen),
  menuItemSettings: () => (isDarkMode() ? menuItemSettingsWhite : menuItemSettingsDarkgreen),
};

export const logo = {
  homeLogo: () => (isDarkMode() ? logoHomeScreenWhite : logoHomeScreenDarkGreen),
};
