import store from '../store/store';
import { setQuoteNotifStatus } from '../store/actions/settingsActions';
import { StoredNotification } from '../types/types';
import { cancelAllLocalNotifications } from './cancelAllLocalNotifications';
import { dispatchNavigateThenPushNextNotif } from './dispatchNavigateThenPushNextNotif';
import { getStoredQuoteNotification, removeStoredQuoteNotification } from './quoteNotificationStorage';

export const displayStoredQuoteNotification = async () => {
  try {
    const notification: StoredNotification | null = await getStoredQuoteNotification();
    if (!notification) {
      global.log("[displayStoredQuoteNotification] -> no stored notif -> setQuoteNotifStatus('Disabled')").redOnBlack();
      store.dispatch(setQuoteNotifStatus('Disabled'));
      return;
    }

    const { quote, date } = notification;
    if (date && new Date(date).getTime() < Date.now()) {
      await removeStoredQuoteNotification();
      cancelAllLocalNotifications();
      dispatchNavigateThenPushNextNotif(quote);
    } else {
      global.log('[displayStoredQuoteNotification] -> NOT TIME YET -> quote, date =', quote, date).yellowOnRed();
    }
  } catch (e) {
    global.log('[displayStoredQuoteNotification] -> exception:', e).yellowOnBlack();
  }
};
