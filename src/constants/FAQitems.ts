export type FAQItem = {
  Q: string;
  A: string;
};

export const FAQItems: FAQItem[] = [
  {
    Q: 'What is the idea behind this app?',
    A:
      'I found myself often forgetting the deep and pure wisdom found in the words of spiritual teachers I love to be inspired by. ' +
      'So one day I got the inspiration to create an app that generate reminders with quotes at random times during the day, ' +
      "allowing for synchronicity to do it's wonderful magic through a device we usually carry wherever we go. " +
      'There is a handfull of quote sources you can enable that we find inspiring, and this will expand further. ' +
      '\n\nThe app also supports user generated online quotes stored as json files, see http://livinglight.one for how to set it up.' +
      '\n\nTo manage quote sources go to Settings/Manage quote sources.',
  },
  {
    Q: 'How is the time for the quote notifications calculated ?',
    A:
      'The duration for one day is divivded in equal parts by the number of daily quotes specified. ' +
      "Then a random time is picked for the first of those time windows when tapping 'Enable quote notifications' in the Settings screen. " +
      'The next quote and time is then generated when tapping the notification or entering the app after the first notification is shown. ' +
      "So if you don't act on the notification you will postpone the generation of the next notification until you do act on it. " +
      'This means that even if three quotes daily are specified, only one will be generated if no action is taken on the first one. ' +
      'If you swipe a notification to the right this will pause the process, but it will automatically resume when you launch the app again.' +
      'If already inside the next time window at the time of acting on a notification, and the random time generated is in the past, ' +
      'the next time window will be chosen to avoid an immediate notification. Immediate random quotes are available on the Quotes screen at any time. ' +
      '\n\nWhen modifiying notififcation settings (number of daily quotes or start-/end times) the time for the next notification is automatically recalculated. ' +
      '\n\nWhen the selection of active quote lists are changed, any scheduled quote will be updated randomly from the new selection. ' +
      '\n\nThe suggestion is to keep the amount of notifications at a rate that feels inspiring and refreshing, that way it will have the deepest effect.',
  },
  {
    Q: 'Enjoy your unique journey in the expanding human experience!',
    A: '- The Living Light dev team -',
  },
];
