import { createStore, combineReducers } from 'redux';
import appReducer from './reducers/appReducer';
import settingsReducer from './reducers/settingsReducer';
import Reactotron from '../../ReactotronConfig';

const rootReducer = combineReducers({
  settings: settingsReducer,
  app: appReducer,
});
export type RootState = ReturnType<typeof rootReducer>;

const store = createStore(rootReducer, Reactotron.createEnhancer!());
export default store;
