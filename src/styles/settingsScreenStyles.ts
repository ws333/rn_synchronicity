import { ViewStyle, TextStyle } from 'react-native';
import { ThemeColors } from '../types/types';
import { styleConstants, SwitchStyles } from './globalStyles';

export interface LocalStyles {
  dailyQuotesContainer: ViewStyle;
  dailyQuotesText: TextStyle;
  dateTimePickerIOSModal: ViewStyle;
  inputSpinnerContainer: ViewStyle;
  inputSpinner: TextStyle;
  inputSpinnerColors: {
    color: string;
    textColor: string;
    colorMax: string;
    colorMin: string;
  };
  quoteNotifText: TextStyle;
  quoteNotifTextContainer: ViewStyle;
  settingsContent: ViewStyle;
  settingsPaper: ViewStyle;
  startEndTimeContainer: ViewStyle;
  startEndTimeItems: ViewStyle;
  startEndTimeHeader: TextStyle;
  startEndTimeTouchable: ViewStyle;
  startEndTimeBtnText: TextStyle;
  switchStyles: SwitchStyles;
}

module.exports = (colors: ThemeColors): LocalStyles => {
  return {
    dailyQuotesContainer: {
      flex: 1,
      alignItems: 'center',
    },
    dailyQuotesText: {
      marginTop: styleConstants.marginLRTB / 2,
      marginBottom: styleConstants.marginLRTB / 4,
      fontSize: styleConstants.fontSizeMedium,
      fontWeight: '500',
      color: colors.settingsText,
      shadowColor: colors.shadowColor,
      ...styleConstants.shadow22NoOffset,
    },
    dateTimePickerIOSModal: {
      alignItems: 'center',
      backgroundColor: colors.background,
    },
    inputSpinnerContainer: {
      marginTop: styleConstants.marginLRTB / 2,
      marginBottom: 3,
      shadowColor: colors.shadowColor,
      ...styleConstants.shadow11NoOffset,
    },
    inputSpinner: {
      width: 133,
      height: 42,
      fontSize: styleConstants.fontSizeMedium,
    },
    inputSpinnerColors: {
      color: colors.inputSpinner,
      textColor: colors.text,
      colorMax: colors.inputSpinnerMinMax,
      colorMin: colors.inputSpinnerMinMax,
    },
    quoteNotifText: {
      fontSize: styleConstants.fontSizeBase,
      fontWeight: '500',
      opacity: styleConstants.textOpacity,
      color: colors.settingsText,
    },
    quoteNotifTextContainer: {
      marginVertical: 3,
      alignItems: 'center',
    },
    settingsPaper: {
      width: '100%',
      alignItems: 'center',
      marginVertical: styleConstants.marginLRTB / 4,
      marginHorizontal: styleConstants.marginVH,
      paddingVertical: styleConstants.padding / 4,
      borderRadius: styleConstants.radius,
      borderWidth: styleConstants.borderWidth,
      ...styleConstants.shadow07NoOffset,
      backgroundColor: colors.settingsPaper,
      shadowColor: colors.shadowColor,
      borderColor: colors.border,
    },
    settingsContent: {
      flex: 1,
      width: 333,
      alignItems: 'center',
    },
    startEndTimeContainer: {
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'center',
      marginVertical: styleConstants.marginVH / 4,
      shadowColor: colors.shadowColor,
      ...styleConstants.shadow11NoOffset,
    },
    startEndTimeItems: {
      flexDirection: 'column',
      alignItems: 'center',
      marginHorizontal: styleConstants.marginVH / 2,
    },
    startEndTimeHeader: {
      fontWeight: '500',
      opacity: styleConstants.textOpacity,
      color: colors.settingsText,
    },
    startEndTimeTouchable: {
      borderRadius: styleConstants.radius / 2,
      paddingHorizontal: styleConstants.padding / 2,
      paddingVertical: styleConstants.padding / 5,
    },
    startEndTimeBtnText: {
      fontSize: styleConstants.fontSizeLarge,
    },
    switchStyles: {
      switchContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginVertical: styleConstants.marginVH / 4,
        shadowColor: colors.shadowColor,
        ...styleConstants.shadow11NoOffset,
      },
      switchColors: {
        switchTrackColor: {
          true: colors.switchTrack,
          false: colors.switchTrack,
        },
        switchThumbColor: colors.switchThumb,
      },
      switchText: {
        marginStart: styleConstants.marginVH / 2,
        fontSize: styleConstants.fontSizeMedium,
        fontWeight: '500',
        opacity: styleConstants.textOpacity,
        color: colors.settingsText,
      },
    },
  };
};
