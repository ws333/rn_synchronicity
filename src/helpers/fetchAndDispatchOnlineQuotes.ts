import store from '../store/store';
import { setQuotes } from '../store/actions/appActions';
import { storeData } from '../services/storage';
import { fetchWithTimeout } from './fetchWithTimeout';
import { getQuotesInApp } from './getQuotes';
import { Quote, QuotesSources, OnlineQuotesObject } from '../types/types';

export async function fetchAndDispatchOnlineQuotes(sources: QuotesSources) {
  try {
    const onlineQuotes: Quote[] = [];
    for (const key of Object.keys(sources)) {
      if (sources[key].enabled && sources[key].url) {
        console.log('Fetching quotes for ', sources[key].name);
        /**
         * If performance issues, this can be done in parallell, but then return the quotes array from each promise and concat.
         * https://eslint.org/docs/rules/no-await-in-loop
         */
        // eslint-disable-next-line no-await-in-loop
        const response = await fetchWithTimeout(sources[key].url!);
        // eslint-disable-next-line no-await-in-loop
        const sourceObject: OnlineQuotesObject = await response.json();

        sourceObject.quotes.forEach((quoteString) => {
          const quote: Quote = { source: key, text: quoteString };
          onlineQuotes.push(quote);
        });

        console.log('onlineQuotes =', onlineQuotes);
      }
    }
    await storeData('ONLINE_QUOTES', onlineQuotes);
    const quotes = onlineQuotes.concat(getQuotesInApp());
    store.dispatch(setQuotes(quotes));
  } catch (e) {
    if (e.toString().includes('Network request failed')) {
      global.log('[SettingsScreen] -> handleFetchQuotesBtnPress() -> not connected!').yellowOnBlack();
    }
    global.log('[SettingsScreen] -> handleFetchQuotesBtnPress() -> exception:', e).yellowOnBlack();
  }
}
