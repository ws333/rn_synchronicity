import React, { FC, ReactElement } from 'react';
import { ViewStyle } from 'react-native';
import DateTimePicker, { AndroidEvent } from '@react-native-community/datetimepicker';
import DatePickerIOS from 'react-native-date-picker'; // Has the iOS look.
import Modal from 'react-native-modal';
import store from '../store/store';
import { TimePickerType, ThemeColors } from '../types/types';
import { updateStoredNotifStartEndTime } from '../helpers/updateStoredNotifStartEndTime';
import { isAndroid } from '../helpers/platform';

interface Props {
  colors: ThemeColors;
  is24Hour: boolean;
  startTime: Date;
  endTime: Date;
  setStartTimeState: Function;
  setEndTimeState: Function;
  timePickerType: TimePickerType;
  setTimePickerType: Function;
}

const StartEndTimePicker: FC<Props> = (props): ReactElement => {
  const {
    colors,
    is24Hour,
    startTime,
    endTime,
    setStartTimeState,
    setEndTimeState,
    timePickerType: showDateTimePicker,
    setTimePickerType,
  } = props;

  function onChangeDTPStartAndroid(_event: AndroidEvent, selectedTime?: Date) {
    const newStartTime = selectedTime ?? startTime;
    setTimePickerType(null);
    updateStoredNotifStartEndTime({ newStartTime });
    store.dispatch(setStartTimeState(newStartTime));
  }
  function onChangeDTPEndAndroid(_event: AndroidEvent, selectedTime?: Date) {
    const newEndTime = selectedTime ?? endTime;
    setTimePickerType(null);
    updateStoredNotifStartEndTime({ newEndTime });
    store.dispatch(setEndTimeState(newEndTime));
  }
  function onChangeDTPStartIOS(selectedTime: Date) {
    const newStartTime = selectedTime ?? startTime;
    updateStoredNotifStartEndTime({ newStartTime });
    store.dispatch(setStartTimeState(newStartTime));
  }
  function onChangeDTPEndIOS(selectedTime: Date) {
    const newEndTime = selectedTime ?? endTime;
    updateStoredNotifStartEndTime({ newEndTime });
    store.dispatch(setEndTimeState(newEndTime));
  }

  const TimePickerStartAndriod = (
    <DateTimePicker
      testID="dateTimePickerStart"
      value={startTime}
      mode="time"
      is24Hour={is24Hour}
      display="default"
      onChange={onChangeDTPStartAndroid}
    />
  );

  const TimePickerEndAndriod = (
    <DateTimePicker
      testID="dateTimePickerEnd"
      value={endTime}
      mode="time"
      is24Hour={is24Hour}
      display="default"
      onChange={onChangeDTPEndAndroid}
    />
  );

  interface LocalStyles {
    dateTimePickerIOSModal: ViewStyle;
  }

  const localStyles: LocalStyles = {
    dateTimePickerIOSModal: {
      alignItems: 'center',
      backgroundColor: colors.background,
    },
  };

  const locale = is24Hour ? 'fr' : 'en';
  const TimePickerStartIOS = (
    <Modal
      backdropColor={colors.background}
      isVisible={true}
      style={localStyles.dateTimePickerIOSModal}
      backdropOpacity={1}
      onBackdropPress={() => setTimePickerType(null)}>
      <DatePickerIOS
        textColor={colors.dateTimePickerText}
        date={startTime}
        mode="time"
        locale={locale}
        onDateChange={onChangeDTPStartIOS}
      />
    </Modal>
  );

  const TimePickerEndIOS = (
    <Modal
      backdropColor={colors.background}
      isVisible={true}
      style={localStyles.dateTimePickerIOSModal}
      backdropOpacity={1}
      onBackdropPress={() => setTimePickerType(null)}>
      <DatePickerIOS
        textColor={colors.dateTimePickerText}
        date={endTime}
        mode="time"
        locale={locale}
        onDateChange={onChangeDTPEndIOS}
      />
    </Modal>
  );

  const TimePickerStart = isAndroid() ? TimePickerStartAndriod : TimePickerStartIOS;
  const TimePickerEnd = isAndroid() ? TimePickerEndAndriod : TimePickerEndIOS;

  switch (showDateTimePicker) {
    case 'start':
      return TimePickerStart;
    case 'end':
      return TimePickerEnd;
    default:
      return TimePickerStart;
  }
};

export default StartEndTimePicker;
