import { ViewStyle, TextStyle } from 'react-native';
import { calcScreenSpacerTop, calcScreenSpacerBottom } from '../helpers/calcScreenSpacers';
import { GlobalStyles, styleConstants } from './globalStyles';
import { ThemeColors } from '../types/types';

/**
 * Not using useTheme since this might trigger logging cycles
 */

export const activeOpacity = 0.7;

interface LogColors extends ThemeColors {
  logPaper: string;
}

const darkGreen = '#003333';
const paleGreen = '#007777';

export const colors: Partial<LogColors> = {
  text: 'white',
  logPaper: darkGreen,
  inputSpinner: paleGreen,
  inputSpinnerMinMax: darkGreen,
  background: '#011',
  textButtonBorder: paleGreen,
  textButtonText: paleGreen,
  textButtonTextDark: paleGreen,
};

export const globalStyles = (): Partial<GlobalStyles> => ({
  screen: {
    flex: 1,
    justifyContent: 'center' as const,
    alignItems: 'center' as const,
    backgroundColor: colors.background,
  },
  scrollView: {
    flexGrow: 1,
  },
  screenSpacerFlexGrow: {
    flex: 1,
    flexGrow: 1,
  },
  screenSpacerTop: {
    height: calcScreenSpacerTop() / 2,
  },
  screenSpacerBottom: {
    height: calcScreenSpacerBottom(),
  },
  textButtonContainer: {
    marginVertical: styleConstants.marginVH / 4,
    borderRadius: 22,
    borderWidth: styleConstants.borderWidth,
    borderColor: `${paleGreen}55`,
  },
  textButton: {
    fontSize: styleConstants.fontSizeMedium,
    marginVertical: 6,
    marginHorizontal: 22,
    color: paleGreen,
  },
});

export interface LocalStyles {
  button: ViewStyle;
  buttonContainer: ViewStyle;
  inputSpinner: TextStyle;
  inputSpinnerColors: {
    color: string;
    textColor: string;
    colorMin: string;
  };
  logContent: ViewStyle;
  logLevelContainer: ViewStyle;
  logLevelText: TextStyle;
  logPaper: ViewStyle;
  logText: TextStyle;
  logTitle: TextStyle;
  logSubtitle: TextStyle;
}

export const localStyles: LocalStyles = {
  button: {
    marginHorizontal: styleConstants.marginVH,
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputSpinner: {
    width: 111,
    height: 37,
    fontSize: styleConstants.fontSizeBase,
  },
  inputSpinnerColors: {
    color: colors.inputSpinner!,
    textColor: colors.text!,
    colorMin: colors.inputSpinnerMinMax!,
  },
  logContent: {
    flex: 1,
    flexGrow: 9999,
    width: '100%',
    alignItems: 'center',
  },
  logLevelContainer: { alignItems: 'center' },
  logLevelText: {
    marginBottom: -10,
    fontSize: 7,
    fontWeight: 'bold',
    color: colors.text,
  },
  logPaper: {
    width: styleConstants.logPaperWidth,
    marginHorizontal: styleConstants.marginVH,
    marginBottom: styleConstants.marginLRTB / 4,
    borderRadius: styleConstants.radius,
    borderWidth: styleConstants.borderWidth,
    backgroundColor: colors.logPaper,
    borderColor: colors.border,
  },
  logText: {
    padding: styleConstants.padding / 2,
    fontSize: 11,
    textAlign: 'left',
    opacity: 1,
    color: colors.text,
  },
  logTitle: {
    marginBottom: styleConstants.marginLRTB / 4,
    fontSize: styleConstants.logTextFontSize + 7,
    fontWeight: 'bold',
    color: colors.text,
  },
  logSubtitle: {
    fontSize: styleConstants.fontSizeBase,
    color: colors.text,
    fontStyle: 'italic',
  },
};
