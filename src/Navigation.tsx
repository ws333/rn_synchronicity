import React, { useEffect, ReactElement, FC } from 'react';
import { BackHandler } from 'react-native';

import { useDispatch, useSelector } from 'react-redux';
import { setActiveScreen } from './store/actions/appActions';
import { setColorScheme } from './store/actions/settingsActions';
import { RootState } from './store/store';

import useScreenDimensions from './hooks/useScreenDimensions';
import useTheme from './hooks/useTheme';

import AddQuotesSourceScreen from './screens/AddQuotesSourceScreen';
import BrainwaveScreen from './screens/BrainwaveScreen';
import DonateScreen from './screens/DonateScreen';
import FAQScreen from './screens/FAQScreen';
import HomeScreen from './screens/HomeScreen';
import LoadingScreen from './components/LoadingScreen';
import LogScreen from './screens/LogScreen';
import ManageQuotesSourcesScreen from './screens/ManageQuotesSourcesScreen';
import PurchaseDonationScreen from './screens/PurchaseDonationScreen';
import QuotesScreen from './screens/QuoteScreen';
import SettingsScreen from './screens/SettingsScreen';
import { alerts, AlertType } from './types/typesAlerts';
import { ScreenProps } from './types/types';

import { getColorScheme } from './helpers/getApperance';
import { handleBackPress } from './helpers/handleBackPress';
import { setStatusBarTranslucent } from './helpers/statusBarHelpers';

const Navigation: FC = (): ReactElement => {
  global.log('[Navigation] -> Component rendering...').green();

  const { spinnerSize } = useScreenDimensions();
  const { styles, colors } = useTheme();

  const activeScreenState = useSelector((state: RootState) => state.app.activeScreen);
  const alertState = useSelector((state: RootState) => state.app.alert);
  const colorSchemeState = useSelector((state: RootState) => state.settings.colorScheme);
  const followDeviceColorSchemeState = useSelector((state: RootState) => state.settings.followDeviceColorScheme);
  const isHydratedState = useSelector((state: RootState) => state.settings.isHydrated);
  global.log('[Navigation] -> activeScreenState =', activeScreenState).whiteOnRed();

  const dispatch = useDispatch();

  useEffect(() => {
    setStatusBarTranslucent();
  });

  useEffect(() => {
    if (isHydratedState && followDeviceColorSchemeState) {
      const deviceColorScheme = getColorScheme();
      dispatch(setColorScheme(deviceColorScheme, false));
    }
  }, [colorSchemeState, dispatch, followDeviceColorSchemeState, isHydratedState]);

  let ActiveScreen: FC<ScreenProps> = HomeScreen;
  switch (activeScreenState.screen) {
    case 'HOME':
      ActiveScreen = HomeScreen;
      break;
    case 'QUOTES':
      ActiveScreen = QuotesScreen;
      break;
    case 'BRAINWAVE':
      ActiveScreen = BrainwaveScreen;
      break;
    case 'SETTINGS':
      ActiveScreen = SettingsScreen;
      break;
    case 'ADD_QUOTES_SOURCE':
      ActiveScreen = AddQuotesSourceScreen;
      break;
    case 'MANAGE_QUOTES_SOURCES':
      ActiveScreen = ManageQuotesSourcesScreen;
      break;
    case 'DONATE':
      ActiveScreen = DonateScreen;
      break;
    case 'PURCHASE_DONATION':
      ActiveScreen = PurchaseDonationScreen;
      break;
    case 'FAQ':
      ActiveScreen = FAQScreen;
      break;
    case 'LOG':
      ActiveScreen = LogScreen;
      break;
    case 'EXIT':
      dispatch(setActiveScreen({ screen: 'HOME' }));
      BackHandler.exitApp();
      break;
    default:
      ActiveScreen = HomeScreen;
  }

  if (!isHydratedState) {
    return <LoadingScreen spinnerSize={spinnerSize} colors={colors} />;
  }

  const alertType: AlertType = alertState?.type ?? 'Message';
  const Alert = alerts[alertType];

  return (
    <>
      <ActiveScreen {...activeScreenState.props} handleBackPress={handleBackPress} />
      {!!alertState && <Alert alertState={alertState} styles={styles} colors={colors} />}
    </>
  );
};

export default Navigation;
