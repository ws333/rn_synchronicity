import store from '../store/store';
import { Quote } from '../types/types';
import { getEnabledQuotesSources } from './getEnabledQuotesSources';
import { getEnabledQuotes } from './getEnabledQuotes';
import { constants } from '../constants/constants';

type Args = {
  enabledSources?: string[];
  currentQuote?: Quote;
};

/**
 * Return a quote that is different from currentQuote
 */
export function getRandomQuote({
  enabledSources = getEnabledQuotesSources(),
  currentQuote = store.getState().app.quote,
}: Args = {}) {
  if (!enabledSources.length) return constants.quotes.noEnabledQoutesSources;

  const enabledQuotes = getEnabledQuotes(enabledSources);

  let newQuote: Quote;
  do {
    const randomIndex = Math.floor(Math.random() * enabledQuotes.length);
    newQuote = enabledQuotes[randomIndex];
  } while (enabledQuotes.length > 1 && newQuote.text === currentQuote.text);

  return newQuote;
}
