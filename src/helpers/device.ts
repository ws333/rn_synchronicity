/*
 * Documentation: https://github.com/react-native-device-info/react-native-device-info
 */
import DeviceInfo from 'react-native-device-info';

export function isTablet() {
  return DeviceInfo.isTablet();
}

export function isPhone() {
  return !DeviceInfo.isTablet();
}
