import { setQuote, setQuotes, setSelectedListItem } from '../store/actions/appActions';
import { setQuotesSources } from '../store/actions/settingsActions';
import store from '../store/store';
import { Quote, QuotesSources } from '../types/types';
import { getRandomQuote } from './getRandomQuote';
import { getStoredData, storeData } from '../services/storage';

export const removeOnlineQuotesSource = async (source: string) => {
  global.log('[removeOnlineQuotesSource] -> Removing online quotesSource:', source).yellowOnRed();

  try {
    const sourcesState = store.getState().settings.quotesSources;
    const quotesState = store.getState().app.quotes;
    const storedOnlineQutes: Quote[] = (await getStoredData('ONLINE_QUOTES')) ?? [];
    const updatedOnlineQutes = storedOnlineQutes?.filter((quote) => quote.source !== source);

    const quotes = quotesState.filter((quote) => quote.source !== source);
    const sources: QuotesSources = { ...sourcesState };
    delete sources[source];

    const isStored = await storeData('ONLINE_QUOTES', updatedOnlineQutes);
    if (!isStored) throw new Error('Storing ONLINE_QUOTES failed.');

    store.dispatch(setQuotes(quotes));
    store.dispatch(setSelectedListItem(''));
    store.dispatch(setQuotesSources(sources)); // settings state properties (here settings.quotesSources) are stored by default

    const quoteState = store.getState().app.quote;
    if (source === quoteState.source) {
      const newQuote = getRandomQuote();
      store.dispatch(setQuote(newQuote));
    }
    return true;
  } catch (e) {
    global.log('[removeOnlineQuotesSource] -> exception:', e).yellowOnBlack();
    return false;
  }
};
