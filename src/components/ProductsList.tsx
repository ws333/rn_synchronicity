import React, { FC, ReactElement, useEffect, useState } from 'react';
import { View, FlatList, TouchableHighlight, Text } from 'react-native';
import Purchases, { PurchasesProduct } from 'react-native-purchases';
import { Avatar, ListItem } from 'react-native-elements';
import store from '../store/store';
import { setActiveScreen } from '../store/actions/appActions';
import { PurchaseType, ThemeColors } from '../types/types';
import { alertIAPException } from '../helpers/alertsExceptions';
import { GlobalStyles, styleConstants } from '../styles/globalStyles';
import { LocalStyles } from '../styles/productsListStyles';
import { avatars } from '../constants/images';

interface Props {
  activeOpacity: number;
  colors: ThemeColors;
  productIDs: string[];
  purchaseType: PurchaseType;
  styles?: GlobalStyles;
}

const ProductsList: FC<Props> = (props: Props): ReactElement => {
  global.log('[ProductsList] -> Component rendering... props:', props).green();
  const { activeOpacity, colors, productIDs } = props;

  const [productsState, setProductsState] = useState<PurchasesProduct[]>([]);

  const localStyles: LocalStyles = require('../styles/productsListStyles')(colors);

  useEffect(() => {
    async function getOfferings() {
      try {
        if (productIDs.length) {
          const products = await Purchases.getProducts(productIDs, props.purchaseType);
          if (products.length) {
            setProductsState(products);
          }
        }
      } catch (e) {
        global.log('[ProductsList] -> Purchases.getProducts -> exception:', e).yellowOnBlack();
        const cb = () => {
          store.dispatch(setActiveScreen({ screen: 'DONATE' }));
        };
        alertIAPException(cb, e);
      }
    }
    getOfferings();
  }, [colors.text, productIDs, props.purchaseType]);

  async function handlePurchase(productID: string) {
    try {
      const { purchaserInfo, productIdentifier } = await Purchases.purchaseProduct(productID, null, props.purchaseType);
      global.log('[ProductsList] -> handlePurchase -> purchaseProduct: ', productID).yellowOnRed();
      global.log('[ProductsList] -> handlePurchase -> purchaserInfo', purchaserInfo).yellowOnRed();
      global.log('[ProductsList] -> handlePurchase -> productIdentifier: ', productIdentifier).yellowOnRed();
    } catch (e) {
      if (e.userCancelled) {
        global.log('[ProductsList] -> handlePurchase -> exception e.userCancelled:', e).yellowOnBlack();
      } else {
        global.log('[ProductsList] -> handlePurchase -> exception:', e).yellowOnBlack();
        const cb = () => {};
        alertIAPException(cb, e);
      }
    }
  }

  function handleOnPressListItem(productID: string) {
    global.log('[ProductsList] -> handleOnPressListItem -> productID', productID).yellowOnRed();
    handlePurchase(productID);
  }

  const GettingProducts = () => (
    <View style={{ alignItems: 'center' }}>
      <Text style={{ color: colors.text, fontSize: styleConstants.fontSizeLarge }}>Getting products...</Text>
    </View>
  );

  const renderItem = ({ item: productID }: { item: string }) => {
    const index = productIDs.indexOf(productID);
    const product = productsState.filter((p) => p.identifier === productID)[0] ?? null;
    if (!product) {
      // prettier-ignore
      global.log('[ProductsList] -> renderItem -> product not found in app/play store-> productID', productID).yellowOnBlack();
      return null;
    }

    return (
      <ListItem
        key={index}
        containerStyle={localStyles.listItemContainer}
        activeOpacity={1}
        underlayColor="transparent"
        onPress={() => handleOnPressListItem(productID)}>
        <Avatar
          rounded
          size={styleConstants.avatarSize}
          containerStyle={localStyles.avatarContainer}
          placeholderStyle={localStyles.avatarPlaceholder}
          source={avatars.heartGreen}
        />
        <ListItem.Content style={localStyles.listItemContent}>
          <ListItem.Title style={localStyles.listItemTitle}>{product.title}</ListItem.Title>
          <ListItem.Title style={localStyles.listItemTitle}>{product.price_string}</ListItem.Title>
          {/* <ListItem.Subtitle style={localStyles.listItemSubtitle}>{product.description}</ListItem.Subtitle> */}
        </ListItem.Content>
        <TouchableHighlight
          activeOpacity={activeOpacity}
          underlayColor={colors.background}
          style={localStyles.listItemCheckBoxPressArea}>
          <View style={localStyles.listItemCheckBoxContainer}>
            <ListItem.Chevron
              name="mobile-friendly"
              containerStyle={localStyles.listItemChevron}
              type="font-awesome5"
              color={colors.iconQuotesSourceCategory}
            />
          </View>
        </TouchableHighlight>
      </ListItem>
    );
  };

  const keyExtractor = (item: string) => item;

  return (
    <View style={localStyles.flatListContainer}>
      <FlatList
        showsVerticalScrollIndicator={false}
        keyExtractor={keyExtractor}
        data={productsState.length ? productIDs : [productIDs[0]]}
        renderItem={productsState.length ? renderItem : GettingProducts}
      />
    </View>
  );
};

export default ProductsList;
