import React, { ComponentClass, FC, ReactElement, useEffect, useRef } from 'react';
import { View, FlatList, LayoutRectangle, TouchableHighlight } from 'react-native';
import { Avatar, ListItem } from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store/store';
import { setQuotesSources } from '../store/actions/settingsActions';
import { setSelectedListItem, setAlert } from '../store/actions/appActions';
import TextButton from './TextButton';
import { QuotesListKeys, QuotesSource, ThemeColors, Torientation } from '../types/types';
import { AddUpdateSourceCallbackArgs, AlertHandlers, AlertState } from '../types/typesAlerts';
import { addUpdateOnlineQuotesSource } from '../helpers/addUpdateOnlineQuotesSource';
import { removeOnlineQuotesSource } from '../helpers/removeOnlineQuotesSource';
import { showToast } from '../helpers/toastHelpers';
import {
  AlertUpdateQuotesSource,
  AlertRemoveQuotesSourceFailed,
  AlertRemoveQuotesSource,
} from '../helpers/alertsManageQuotes';
import { GlobalStyles, styleConstants } from '../styles/globalStyles';
import { LocalStyles } from '../styles/quotesSourceListStyles';
import { avatars } from '../constants/images';

/**
 * ListItem heights (including margins) for calculating scrollToIndex offsets
 * Update the values if ListItem heights change
 */
const itemHeights = {
  compact: 78.9,
  expandedInApp: 114.2,
  expandedOnline: 177.6,
};

/**
 * Used to enable scrollToIndex when user opens ListItem
 */
let scrollToEnabled = false;

interface Props {
  activeOpacity: number;
  colors: ThemeColors;
  orientationState: Torientation;
  setShowSpinnerState: React.Dispatch<React.SetStateAction<boolean>>;
  styles: GlobalStyles;
}

const QuotesSourceList: FC<Props> = (props): ReactElement => {
  const { activeOpacity, colors, orientationState, setShowSpinnerState, styles } = props;
  const localStyles: LocalStyles = require('../styles/quotesSourceListStyles')(colors);

  const flatListRef = useRef<FlatList<string>>(null);
  const lastExpandedItemHeight = useRef<number>(105);

  const sourcesState = useSelector((state: RootState) => state.settings.quotesSources);
  const sourcesStateKeysSorted = Object.keys(sourcesState).sort();
  const selectedListItemState = useSelector((state: RootState) => state.app.selectedListItem);
  const quotesState = useSelector((state: RootState) => state.app.quotes);
  const dispatch = useDispatch();

  /**
   * ScrollToIndex with offset calculated based on current and previous (if any) selected ListItem height.
   */
  useEffect(() => {
    if (scrollToEnabled && selectedListItemState) {
      const keyIndex = sourcesStateKeysSorted.indexOf(selectedListItemState);
      let offset = selectedListItemState ? itemHeights.expandedInApp - itemHeights.compact : itemHeights.compact;
      offset += lastExpandedItemHeight.current;
      flatListRef.current?.scrollToIndex({ index: keyIndex, viewOffset: offset });
    }
    scrollToEnabled = false;
  }, [selectedListItemState, sourcesStateKeysSorted]);

  function getItemLayout(_data: any, index: number) {
    const offset = selectedListItemState ? lastExpandedItemHeight.current : itemHeights.compact;
    if (selectedListItemState && index === sourcesStateKeysSorted.indexOf(selectedListItemState)) {
    }
    return {
      length: sourcesStateKeysSorted.length,
      offset: itemHeights.compact * index + offset - itemHeights.compact / 2,
      index,
    };
  }

  function handleUpdateCallback({ alert }: AddUpdateSourceCallbackArgs) {
    if (alert) {
      const newAlertState: AlertState = {
        ...alert,
        handleOnPressCancel: handleAlertOnPressCancel,
        handleOnPressConfirm: handleAlertOnPressConfirmUpdate,
      };
      dispatch(setAlert(newAlertState));
    }
  }

  async function updateQoutesSource() {
    if (selectedListItemState && sourcesState[selectedListItemState].url) {
      setShowSpinnerState(true);
      const url = sourcesState[selectedListItemState].url as string;
      const updatedName = await addUpdateOnlineQuotesSource(url, handleUpdateCallback, setShowSpinnerState);
      setShowSpinnerState(false);
      if (updatedName) {
        const title = 'Update';
        const message = updatedName + ' successfully updated';
        showToast({ title, message });
      }
    }
  }

  function handleAlertOnPressCancel() {
    dispatch(setAlert(null));
  }

  function handleAlertOnPressConfirmUpdate() {
    dispatch(setAlert(null));
    updateQoutesSource();
  }

  function handleOnPressUpdateButton() {
    if (selectedListItemState) {
      const alertHandlers: AlertHandlers = {
        handleOnPressCancel: handleAlertOnPressCancel,
        handleOnPressConfirm: handleAlertOnPressConfirmUpdate,
      };
      AlertUpdateQuotesSource(selectedListItemState, alertHandlers);
    }
  }

  function removeSelectedSource() {
    if (selectedListItemState) {
      removeOnlineQuotesSource(selectedListItemState).then((isRemoved) => {
        if (isRemoved) {
          flatListRef.current?.scrollToIndex({ index: 0, viewOffset: itemHeights.compact });
          const title = 'Remove';
          const message = selectedListItemState + ' successfully removed';
          showToast({ title, message });
        } else {
          AlertRemoveQuotesSourceFailed(selectedListItemState);
        }
      });
    }
  }

  function handleAlertOnPressConfirmRemove() {
    dispatch(setAlert(null));
    removeSelectedSource();
  }

  function handleOnPressRemoveButton() {
    if (selectedListItemState) {
      const alertHandlers: AlertHandlers = {
        handleOnPressCancel: handleAlertOnPressCancel,
        handleOnPressConfirm: handleAlertOnPressConfirmRemove,
      };
      AlertRemoveQuotesSource(selectedListItemState, alertHandlers);
    }
  }

  function handleOnPressListItem(key: string) {
    if (selectedListItemState === key) {
      dispatch(setSelectedListItem(''));
    } else {
      scrollToEnabled = true;
      dispatch(setSelectedListItem(key));
    }
  }

  function handelOnPressCheckbox(key: string) {
    scrollToEnabled = false;
    const qs = { ...sourcesState };
    qs[key].enabled = !qs[key].enabled;
    dispatch(setQuotesSources(qs));
  }

  function handleOnLayout(layout: LayoutRectangle, key: string) {
    if (selectedListItemState === key) {
      lastExpandedItemHeight.current = layout.height;
    }
  }

  const AvatarImage = (key: string): Element => (
    <FastImage
      style={localStyles.avatarImage}
      source={
        sourcesState[key].category === 'In-app' ? avatars[key as QuotesListKeys] : { uri: sourcesState[key].imageUrl }
      }
    />
  );

  const OnlineListItemButtons = (
    <View style={localStyles.onlineListItemButtonsContainer}>
      <TextButton
        containerStyle={[styles.textButtonContainer, localStyles.textButtonContainer]}
        style={styles.textButtonSmall}
        onPress={handleOnPressUpdateButton}>
        Update
      </TextButton>
      <TextButton
        containerStyle={[styles.textButtonContainer, localStyles.textButtonContainer]}
        style={styles.textButtonSmall}
        onPress={handleOnPressRemoveButton}>
        Remove
      </TextButton>
    </View>
  );

  const ListItemDetails = (sourceItem: QuotesSource) => {
    const separatorText = orientationState === 'PORTRAIT' ? '\n' : '. ';
    const updateText = sourceItem.updatedDate ? `${separatorText}Updated: ${sourceItem.updatedDate}` : separatorText;
    const listItemQuotesCount = quotesState.filter((item) => item.source === selectedListItemState).length;
    return (
      <View style={localStyles.listItemDetailsContainer}>
        <View>
          <ListItem.Subtitle style={localStyles.listItemDetailsSubtitle}>
            {`\n${listItemQuotesCount} ${sourceItem.category} quotes${updateText}`}
          </ListItem.Subtitle>
        </View>
        <View style={localStyles.listItemDetailsButtonsContainer}>
          {sourceItem.category === 'Online' && OnlineListItemButtons}
        </View>
      </View>
    );
  };

  const renderItem = ({ item: key }: { item: string }) => (
    <ListItem
      onLayout={(event) => handleOnLayout(event.nativeEvent.layout, key)}
      key={key}
      containerStyle={localStyles.listItemContainer}
      activeOpacity={1}
      underlayColor="transparent"
      onPress={() => handleOnPressListItem(key)}>
      <Avatar
        rounded
        size={styleConstants.avatarSize}
        containerStyle={localStyles.avatarContainer}
        placeholderStyle={localStyles.avatarPlacehodler}
        /**
         * Casting needed since ImageComponent expects type ComponentClass<{}, any>
         */
        ImageComponent={((() => AvatarImage(key)) as unknown) as ComponentClass<{}, any>}
      />
      <ListItem.Content style={localStyles.listItemContent}>
        <ListItem.Title style={localStyles.listItemTitle}>{sourcesState[key].name}</ListItem.Title>
        <ListItem.Subtitle style={localStyles.listItemSubtitle}>{sourcesState[key].subtitle}</ListItem.Subtitle>
        {selectedListItemState === key && ListItemDetails(sourcesState[key])}
      </ListItem.Content>
      <TouchableHighlight
        activeOpacity={activeOpacity}
        underlayColor={colors.background}
        style={localStyles.listItemCheckBoxPressArea}
        onPressOut={() => handelOnPressCheckbox(key)}>
        <View style={localStyles.listItemCheckBoxContainer}>
          <ListItem.CheckBox
            activeOpacity={activeOpacity}
            checked={sourcesState[key].enabled ?? false}
            onPress={() => handelOnPressCheckbox(key)}
          />
          <ListItem.Chevron
            name={sourcesState[key].category === 'In-app' ? 'mobile-friendly' : 'online-prediction'}
            containerStyle={localStyles.listItemChevron}
            type="font-awesome5"
            color={colors.iconQuotesSourceCategory}
          />
        </View>
      </TouchableHighlight>
    </ListItem>
  );

  const keyExtractor = (item: string) => item;

  return (
    <View style={localStyles.flatListContainer}>
      <FlatList
        showsVerticalScrollIndicator={false}
        ref={flatListRef}
        keyExtractor={keyExtractor}
        data={sourcesStateKeysSorted}
        renderItem={renderItem}
        getItemLayout={getItemLayout}
      />
    </View>
  );
};

export default QuotesSourceList;
