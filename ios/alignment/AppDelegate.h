#import <React/RCTBridgeDelegate.h>
#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <AVFoundation/AVFoundation.h> // See AppDelegate.m -> activate sound in YouTube component when iPhone is in vibrate mode

// UNUserNotificationCenterDelegate added to delivered notification to a foreground app (see AppDelegate.m)
@interface AppDelegate : UIResponder <UIApplicationDelegate, RCTBridgeDelegate, UNUserNotificationCenterDelegate>

@property (nonatomic, strong) UIWindow *window;

@end
