import store from '../store/store';
import { Quote } from '../types/types';

export function getEnabledQuotes(enabledSources: string[]): Quote[] {
  const { quotes } = store.getState().app;
  const enabledQuotes: Quote[] = [];

  enabledSources.forEach((source) =>
    quotes
      .filter((quote) => quote?.source === source)
      .forEach((quote) => enabledQuotes.push({ source: quote.source, text: quote.text })),
  );

  return enabledQuotes;
}
