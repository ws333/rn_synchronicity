export function formatStartEndTime(startTime: Date, endTime: Date, is24Hour: boolean) {
  let startHours = startTime.getHours().toString().padStart(2, '0');
  let startMins = startTime.getMinutes().toString().padStart(2, '0');
  let endHours = endTime.getHours().toString().padStart(2, '0');
  let endMins = endTime.getMinutes().toString().padStart(2, '0');

  if (!is24Hour) {
    const startAmOrPm = Number(startHours) >= 12 ? 'pm' : 'am';
    const tmpStartHours = Number(startHours) % 12 || 12;
    startHours = tmpStartHours.toString().padStart(2, '0');
    startMins += startAmOrPm;
    const endAmOrPm = Number(endHours) >= 12 ? 'pm' : 'am';
    const tmpEndHours = Number(endHours) % 12 || 12;
    endHours = tmpEndHours.toString().padStart(2, '0');
    endMins += endAmOrPm;
  }

  const formattedStartTime = startHours + ':' + startMins;
  const formattedEndTime = endHours + ':' + endMins;
  return { formattedStartTime, formattedEndTime };
}
