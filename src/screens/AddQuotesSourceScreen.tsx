import React, { FC, ReactElement, useRef, useState } from 'react';
import { ImageBackground, KeyboardAvoidingView, Text, View } from 'react-native';
import { Input } from 'react-native-elements';
import { useDispatch } from 'react-redux';
import { gotoPreviousScreen, setAlert, setSelectedListItem } from '../store/actions/appActions';
import { AddUpdateSourceCallbackArgs, AlertState } from '../types/typesAlerts';
import SpinnerThreeBounce from '../components/SpinnerThreeBounce';
import TextButton from '../components/TextButton';
import useTheme from '../hooks/useTheme';
import useScreenDimensions from '../hooks/useScreenDimensions';
import { addUpdateOnlineQuotesSource } from '../helpers/addUpdateOnlineQuotesSource';
import { addProtocolIfMissing } from '../helpers/addProtocolIfMissing';
import { showToast } from '../helpers/toastHelpers';
import { timeout } from '../helpers/timeout';
import { isIOS } from '../helpers/platform';
import { LocalStyles } from '../styles/addQuotesSourceScreenStyle';
import { backgrounds } from '../constants/images';
import { constants } from '../constants/constants';
import { handlePressLink } from '../helpers/handlePressLink';
import { styleConstants } from '../styles/globalStyles';

const AddQuotesSourceScreen: FC = (): ReactElement => {
  const { styles, colors } = useTheme();

  const [addBtnDisabledState, setAddBtnDisabledState] = useState(true);
  const [showSpinnerState, setShowSpinnerState] = useState(false);

  const inputUrlRef = useRef<Input>(null);
  const { spinnerSize } = useScreenDimensions();
  const dispatch = useDispatch();

  function handleAlertOnPressCancel() {
    dispatch(setAlert(null));
    handleOnPressCancel();
  }

  async function handleAlertOnPressConfirm() {
    dispatch(setAlert(null));
    /*
     * The calls to blur(), timeout() and focus() are needed to make keyboard reappear.
     */
    inputUrlRef.current?.blur();
    await timeout(100);
    inputUrlRef.current?.focus();
  }

  function handleAddUpdateCallback({ closeCaller, cancelPressed, alert, key }: AddUpdateSourceCallbackArgs = {}): void {
    if (alert) {
      const newAlertState: AlertState = {
        ...alert,
        handleOnPressCancel: handleAlertOnPressCancel,
        handleOnPressConfirm: handleAlertOnPressConfirm,
      };
      dispatch(setAlert(newAlertState));
    }
    if (key) {
      dispatch(setSelectedListItem(key));
    }
    if (closeCaller || cancelPressed) {
      handleOnPressCancel();
    }
  }

  async function handleOnPressAdd() {
    type InputState = { url: string };
    const { url } = (inputUrlRef.current?.state as InputState) ?? ({ url: '' } as InputState);
    if (url.length === 0) {
      // prettier-ignore
      global.log('file: AddOnlineQuotesSource.tsx -> handleAddBtnPress -> url.length is 0').yellowOnBlack();
      return;
    }
    setShowSpinnerState(true);
    await timeout(500); // Alert on exception does not always display if no timeout
    const completeUrl = addProtocolIfMissing(url);
    const addedName = await addUpdateOnlineQuotesSource(completeUrl, handleAddUpdateCallback, setShowSpinnerState);
    if (addedName) {
      const title = 'Add quotes source';
      const message = addedName + ' successfully added';
      showToast({ title, message });
    }
  }

  function handleOnPressCancel() {
    dispatch(gotoPreviousScreen());
  }

  function handleInputOnChangeText(url: string) {
    try {
      inputUrlRef.current?.setState({ url });
      const completeUrl = addProtocolIfMissing(url);
      const validUrl = new URL(completeUrl).href;
      setAddBtnDisabledState(!validUrl);
    } catch (e) {
      setAddBtnDisabledState(true);
      // prettier-ignore
      global.log('file: AddOnlineQuotesSource.tsx -> handleOnChangeText -> exception: ', e).yellowOnBlack();
    }
  }

  const HelpText = () => (
    <>
      <Text
        style={{ ...localStyles.helpText, color: colors.quoteButtonText }}
        onPress={() => handlePressLink('https://livinglight.one/alignment/howto/')}>
        How to set up your own online quotes (tap to open link)
      </Text>
      <Text
        style={{
          ...localStyles.helpText,
          marginTop: styleConstants.marginLRTB / 2,
          marginBottom: styleConstants.marginLRTB / 2,
        }}>
        {constants.texts.enterUrlHelpText}
      </Text>
    </>
  );
  const localStyles: LocalStyles = require('../styles/addQuotesSourceScreenStyle')(colors);

  return (
    <View style={styles.screen}>
      <ImageBackground source={backgrounds.settingsScreenImg()} style={styles.imageBackground}>
        <View style={styles.screenOverlay}>
          <View style={styles.screenSpacerTop} />
          <KeyboardAvoidingView behavior={isIOS() ? 'position' : 'height'} style={localStyles.keyboardAvoidingView}>
            <View style={localStyles.addUserQuotesSourceStyle}>
              <SpinnerThreeBounce isVisible={showSpinnerState} spinnerSize={spinnerSize} colors={colors} />
              <Input
                placeholder={constants.texts.enterUrl}
                onChangeText={handleInputOnChangeText}
                autoFocus
                ref={inputUrlRef}
                enablesReturnKeyAutomatically
                returnKeyType="done"
                leftIcon={localStyles.input.leftIcon}
                style={localStyles.input.style}
                autoCapitalize="none"
                onSubmitEditing={handleOnPressAdd}
              />
              <HelpText />
              <View style={localStyles.textButtonContainer}>
                <TextButton
                  disabled={addBtnDisabledState}
                  styleDisabled={localStyles.textButtonDisabled}
                  containerStyle={styles.textButtonContainerNoTop}
                  style={styles.textButtonSmall}
                  onPress={handleOnPressAdd}>
                  Add
                </TextButton>
                <TextButton
                  containerStyle={styles.textButtonContainerNoTop}
                  style={styles.textButtonSmall}
                  onPress={handleOnPressCancel}>
                  Cancel
                </TextButton>
              </View>
            </View>
          </KeyboardAvoidingView>
          <View style={styles.screenSpacerBottom} />
        </View>
      </ImageBackground>
    </View>
  );
};

export default AddQuotesSourceScreen;
