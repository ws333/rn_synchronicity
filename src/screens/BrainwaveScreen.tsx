/* eslint-disable indent */
import React, { FC, ReactElement, useRef, useState } from 'react';
import { ScrollView, View, Text, FlatList, ImageBackground, Image } from 'react-native';
import { MenuItemBrainWaveScreen as MenuItem, ScreenProps } from '../types/types';
import GoBackButton from '../components/GoBackButton';
import YouTubeVideo from '../components/YoutubeVideo';
import MenuGridItem from '../components/MenuGridItem';
import useTheme from '../hooks/useTheme';
import useScreenDimensions from '../hooks/useScreenDimensions';
import { isIOS } from '../helpers/platform';
import { calcMenuGrid } from '../helpers/calcMenuGrid';
import { LocalStyles } from '../styles/brainwaveScreenStyles';
import { chooseSessionText, brainwaveInfoText } from '../constants/brainwaveTexts';
import { menuItemsBrainwaveScreen as menuItems } from '../constants/menuItems';
import { backgrounds, headers } from '../constants/images';

const BrainwaveScreen: FC<ScreenProps> = (props): ReactElement => {
  global.log('[BrainwaveScreen] -> Component rendering...').green();

  const [toggleTextState, setToggleTextState] = useState(false);
  const [videoState, setVideoState] = useState({ videoId: '' });

  const { dimensionsState, isSmallScreenHeight, orientationState, spinnerSize } = useScreenDimensions();
  const { styles, colors, activeOpacityMenuGridItem } = useTheme();

  const localStyles: LocalStyles = require('../styles/brainwaveScreenStyles')(colors);

  const header = (
    <View style={{ ...styles.header }}>
      <Image source={headers.brainwaveHeaderImg()} />
    </View>
  );

  const text = toggleTextState ? brainwaveInfoText : chooseSessionText;
  const textStyle = toggleTextState ? localStyles.brainwaveInfoText : localStyles.brainwaveChooseSessionText;
  const scrollViewRef = useRef<ScrollView>(null);

  const InfoSection = (
    <View style={localStyles.brainwavePaper}>
      <ScrollView
        ref={scrollViewRef}
        contentContainerStyle={styles.scrollView}
        alwaysBounceVertical={false}
        showsVerticalScrollIndicator={false}>
        {isSmallScreenHeight && orientationState === 'PORTRAIT' && !videoState.videoId && header}
        {orientationState === 'LANDSCAPE' && header}
        <Text style={textStyle}>{text}</Text>
      </ScrollView>
    </View>
  );

  const YouTubeContainer = (
    <YouTubeVideo
      colors={colors}
      containerStyle={localStyles.youtubeContainer}
      setVideoState={setVideoState}
      spinnerSize={spinnerSize}
      style={localStyles.youTubeComponent}
      videoState={videoState}
    />
  );

  const showHeader = !isSmallScreenHeight && orientationState === 'PORTRAIT' && !videoState.videoId;

  type FlatListItem = { item: MenuItem };
  const renderMenuGridItem = ({ item }: FlatListItem) => {
    return (
      <MenuGridItem
        activeOpacity={activeOpacityMenuGridItem}
        item={item}
        onPress={
          item.videoId
            ? () => {
                setVideoState({ ...videoState, videoId: item.videoId });
                setToggleTextState(false);
              }
            : () => {
                setVideoState({ videoId: '' });
                scrollViewRef.current?.scrollTo({ x: 0 });
                setToggleTextState(!toggleTextState);
              }
        }
        styles={styles}
      />
    );
  };

  const menuItemsCount = menuItems.length;
  const { menuColumns, menuGridWidth } = calcMenuGrid(dimensionsState, menuItemsCount);

  const MenuFlatList = (
    <FlatList
      data={menuItems}
      renderItem={renderMenuGridItem}
      key={orientationState}
      numColumns={menuColumns}
      scrollEnabled={false}
    />
  );

  return (
    <View style={styles.screen}>
      <ImageBackground source={backgrounds.brainwaveScreenImg()} style={styles.imageBackground}>
        <View style={styles.screenSpacerTop} />

        {showHeader && <View style={styles.headerPaper}>{header}</View>}

        <View style={localStyles.brainWaveContent}>
          {!videoState.videoId && InfoSection}
          {!!videoState.videoId && YouTubeContainer}
          {!videoState.videoId && <View style={styles.screenSpacerFlexGrow} />}
        </View>

        <View style={{ ...styles.menuGrid, width: menuGridWidth }}>{MenuFlatList}</View>
        {isIOS() && (
          <View style={styles.navButtonsContainer}>
            <GoBackButton onPress={props.handleBackPress!} />
          </View>
        )}
        <View style={styles.screenSpacerBottom} />
      </ImageBackground>
    </View>
  );
};

export default BrainwaveScreen;
