import React, { FC } from 'react';
import { ImageBackground, Text, TouchableOpacity, View } from 'react-native';
import { GlobalStyles } from '../styles/globalStyles';
import { MenuItem } from '../types/types';

interface Props {
  item: MenuItem;
  onPress: () => void;
  styles: GlobalStyles;
  activeOpacity: number;
}

const MenuGridItem: FC<Props> = (props) => {
  const { activeOpacity, item, onPress, styles } = props;
  const imageDims = {
    width: styles.menuGridItem.width as number,
    height: styles.menuGridItem.height as number,
  };
  return (
    <TouchableOpacity activeOpacity={activeOpacity} onPress={onPress}>
      <View style={styles.menuGridItem}>
        <ImageBackground
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            width: imageDims.width,
            height: imageDims.height,
          }}
          imageStyle={{
            resizeMode: 'contain',
            marginTop: 3,
            opacity: 0.7,
          }}
          source={item.image ? item.image() : null}>
          {!item.image && <Text style={styles.menuGridItemText}>{item.title}</Text>}
        </ImageBackground>
      </View>
    </TouchableOpacity>
  );
};

export default MenuGridItem;
