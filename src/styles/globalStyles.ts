import { Platform, TextStyle, ViewStyle } from 'react-native';
import { StyleConstants } from '../types/typesStyleConstants';
import { darkBlue } from './themeColors';

export const fontSizeBase = 14;

/**
 * Style constants used globally
 */
export const styleConstants: StyleConstants = {
  avatarSize: 'medium' as const,
  borderWidth: 0.5,
  fontSizeSmall: fontSizeBase - 2,
  fontSizeBase: fontSizeBase,
  fontSizeMedium: fontSizeBase + 2,
  fontSizeLarge: fontSizeBase + 4,
  fontSizeXLarge: fontSizeBase + 6,
  headerPaperWidth: '80%',
  infoSectionWidth: '80%',
  infoSectionWidthSmallScreen: '90%',
  listWidth: '80%',
  logTextFontSize: fontSizeBase,
  logPaperWidth: '90%',
  marginLRTB: 22,
  marginVH: 22,
  menuGridItemsOffsetTopLeft: 6, // Needs to be size of bottom/right side shadow to avoid cutoff
  menuGridItemsOffsetBottomRight: 5, // Needs to be size of top/left side shadow to avoid cutoff
  padding: 22,
  paperWidth: '80%',
  quotesSourceWidth: '93%',
  radius: 22,
  screenSpacer: 22,
  smallScreenSpacerBottomFactor: 2,
  textOpacity: 1,

  /**
   * https://ethercreative.github.io/react-native-shadow-generator/
   * iOS use shadow... styles - Andorid use elevation only
   * Android elevation cause a box "artifact" when backgroundColor is (semi) transparent
   * Disabled unless background is opaque, see https://github.com/facebook/react-native/issues/25093
   */
  shadow07: {
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    // elevation: 7,
  },
  shadow11: {
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,
    // elevation: 11,
  },
  shadow17: {
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,
    // elevation: 17,
  },
  shadow22: {
    shadowOffset: {
      width: 0,
      height: 22,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,
    // elevation: 22,
  },
  shadow07NoOffset: {
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    // elevation: 7,
  },
  shadow11NoOffset: {
    shadowOpacity: 0.36,
    shadowRadius: 6.68,
    // elevation: 11,
  },
  shadow17NoOffset: {
    shadowOpacity: 0.46,
    shadowRadius: 11.14,
    // elevation: 17,
  },
  shadow22NoOffset: {
    shadowOpacity: 0.55,
    shadowRadius: 14.78,
    // elevation: 22,
  },
};

export interface GlobalStyles {
  header: ViewStyle;
  headerPaper: ViewStyle;
  imageBackground: ViewStyle;
  list: ViewStyle;
  menuGrid: ViewStyle;
  menuGridItem: ViewStyle;
  menuGridItemText: TextStyle;
  navButtonsContainer: ViewStyle;
  screen: ViewStyle;
  screenOverlay: ViewStyle;
  screenSpacerTop?: ViewStyle;
  screenSpacerFlexGrow: ViewStyle;
  screenSpacerBottom?: ViewStyle;
  scrollView: ViewStyle;
  textButtonContainer: ViewStyle;
  textButtonContainerNoTop?: ViewStyle;
  textButton: TextStyle;
  textButtonSmall?: TextStyle;
}

export interface SwitchStyles {
  switchContainer: ViewStyle;
  switchColors: {
    switchThumbColor: string;
    switchTrackColor: {
      true: string;
      false: string;
    };
  };
  switchText: TextStyle;
}

/**
 * Styles used globally
 * Theme colors and dynamic styles are added in useTheme and in the different screens
 */
export const globalStyles: GlobalStyles = {
  header: {
    // header styling is needed when header is rendered inside page content.
    alignItems: 'center',
    marginVertical: styleConstants.marginVH / 2,
    paddingTop: styleConstants.marginLRTB / 2,
  },
  headerPaper: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: styleConstants.marginLRTB,
    paddingHorizontal: styleConstants.padding,
  },
  imageBackground: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  list: {
    width: styleConstants.listWidth,
  },
  menuGrid: {
    flex: 0,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    width: '95%', // Used in calcMenuGrid, needs to be in percent.
    paddingTop: styleConstants.menuGridItemsOffsetTopLeft,
    paddingBottom: styleConstants.menuGridItemsOffsetBottomRight,
    paddingLeft: styleConstants.menuGridItemsOffsetTopLeft, // Used in calcMenuGrid
    paddingRight: styleConstants.menuGridItemsOffsetBottomRight, // Used in calcMenuGrid
    borderRadius: styleConstants.radius,
    ...styleConstants.shadow07,
  },
  menuGridItem: {
    ...Platform.select({
      // Used in calcMenuGrid
      ios: { width: 110 },
      android: { width: 90 },
    }),
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'visible',
    marginLeft: styleConstants.menuGridItemsOffsetBottomRight, // Used in calcMenuGrid
    marginRight: styleConstants.menuGridItemsOffsetTopLeft, // Used in calcMenuGrid
    marginTop: styleConstants.menuGridItemsOffsetBottomRight,
    marginBottom: styleConstants.menuGridItemsOffsetTopLeft, // Used in calcMenuGrid
    padding: 3,
    borderRadius: styleConstants.radius / 1.5,
    borderWidth: styleConstants.borderWidth,
  },
  menuGridItemText: {
    fontSize: styleConstants.fontSizeMedium,
    fontFamily: 'Merienda-Bold',
  },
  navButtonsContainer: {
    alignItems: 'center',
    marginTop: styleConstants.marginLRTB / 2,
  },
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  screenOverlay: {
    width: '100%',
    height: '100%',
    backgroundColor: darkBlue + '80',
    alignItems: 'center',
  },
  scrollView: {
    flexGrow: 1,
  },
  screenSpacerFlexGrow: {
    flex: 1,
    flexGrow: 1,
  },
  textButtonContainer: {
    marginVertical: styleConstants.marginVH / 4,
    borderRadius: styleConstants.radius,
    borderWidth: styleConstants.borderWidth,
    ...styleConstants.shadow07,
  },
  textButton: {
    marginHorizontal: 17,
    marginVertical: 5,
    paddingHorizontal: styleConstants.padding / 2,
    paddingVertical: 3,
    fontSize: styleConstants.fontSizeMedium,
  },
};
