import { Appearance } from 'react-native';

export function getColorScheme() {
  return Appearance.getColorScheme();
}
