/**
 * Unit tests for calcMenuGridNumColumns
 * Expects:
 *    width of menuGridItem to be 100
 *    width of menuGrid to be about 90% of screen width
 *    both values are calculated based on the respective keys in constants.ts
 */

import { ScaledSize } from 'react-native';
import { calcMenuGrid } from '../src/helpers/calcMenuGrid';
import { logToConsoleWithColors } from '../src/services/logToConsoleWithColors';

/**
 * Test setup:
 * global.log is used for logging in the functions being tested.
 * Set logLevel = 0 to disable logging, 5 for debug level
 */
global.log = logToConsoleWithColors;
global.logLevel = 0;

/**
 * max 3 menuItems
 */
test('calcMenuGridNumColumns -> max 3 menuItems', () => {
  const dimensionsState: ScaledSize = {
    width: 400,
    height: 900,
    scale: 1,
    fontScale: 1,
  };
  expect(calcMenuGrid(dimensionsState, 0).menuColumns).toBe(0);
  expect(calcMenuGrid(dimensionsState, 1).menuColumns).toBe(1);
  expect(calcMenuGrid(dimensionsState, 2).menuColumns).toBe(2);
  expect(calcMenuGrid(dimensionsState, 3).menuColumns).toBe(3);
  expect(calcMenuGrid(dimensionsState, 4).menuColumns).toBe(2);
  expect(calcMenuGrid(dimensionsState, 5).menuColumns).toBe(3);
  expect(calcMenuGrid(dimensionsState, 6).menuColumns).toBe(3);
  expect(calcMenuGrid(dimensionsState, 7).menuColumns).toBe(3);
  expect(calcMenuGrid(dimensionsState, 8).menuColumns).toBe(3);
  expect(calcMenuGrid(dimensionsState, 9).menuColumns).toBe(3);
  expect(calcMenuGrid(dimensionsState, 10).menuColumns).toBe(3);
});

/**
 * max 4 menuItems
 */
test('calcMenuGridNumColumns -> max 4 menuItems', () => {
  const dimensionsState = {
    width: 500,
    height: 1100,
    scale: 1,
    fontScale: 1,
  };
  expect(calcMenuGrid(dimensionsState, 1).menuColumns).toBe(1);
  expect(calcMenuGrid(dimensionsState, 2).menuColumns).toBe(2);
  expect(calcMenuGrid(dimensionsState, 3).menuColumns).toBe(3);
  expect(calcMenuGrid(dimensionsState, 4).menuColumns).toBe(4);
  expect(calcMenuGrid(dimensionsState, 5).menuColumns).toBe(3);
  expect(calcMenuGrid(dimensionsState, 6).menuColumns).toBe(3);
  expect(calcMenuGrid(dimensionsState, 7).menuColumns).toBe(4);
  expect(calcMenuGrid(dimensionsState, 8).menuColumns).toBe(4);
  expect(calcMenuGrid(dimensionsState, 9).menuColumns).toBe(3);
  expect(calcMenuGrid(dimensionsState, 10).menuColumns).toBe(4);
  expect(calcMenuGrid(dimensionsState, 11).menuColumns).toBe(4);
  expect(calcMenuGrid(dimensionsState, 12).menuColumns).toBe(4);
  expect(calcMenuGrid(dimensionsState, 13).menuColumns).toBe(4);
  expect(calcMenuGrid(dimensionsState, 14).menuColumns).toBe(4);
  expect(calcMenuGrid(dimensionsState, 15).menuColumns).toBe(4);
  expect(calcMenuGrid(dimensionsState, 16).menuColumns).toBe(4);
  expect(calcMenuGrid(dimensionsState, 17).menuColumns).toBe(4);
});
