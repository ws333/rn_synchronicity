/**
 * Init global logging with color
 */
import './src/services/initLogging';

/**
 * React Native URL class has som issues that this polyfill fixes https://github.com/charpeni/react-native-url-polyfill
 */
import 'react-native-url-polyfill/auto';

if (__DEV__) {
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'));
}

import { AppRegistry } from 'react-native';
import * as Sentry from '@sentry/react-native';
import App from './src/App';
import { name as appName } from './app.json';
import PushService from './src/services/PushService';
import IAP from './src/services/IAPService';

/**
 * Init Sentry diagnostics
 */
Sentry.init({
  dsn: 'https://4a67bdba559544e099889e89a3457669@o569276.ingest.sentry.io/5714860',
});

/**
 * Init react-native-push-notification
 */
PushService.init();

/**
 * Init RevenueCat IAP
 */
IAP.init();

AppRegistry.registerComponent(appName, () => App);
