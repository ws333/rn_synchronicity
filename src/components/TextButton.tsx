/**
 * Button documentation https://github.com/ide/react-native-button
 */

import React, { FC, ReactElement } from 'react';
import { ImageBackground, TextStyle, View, ViewStyle } from 'react-native';
import Button from 'react-native-button';

type Props = {
  accessibilityLabel?: string;
  activeOpacity?: number;
  allowFontScaling?: boolean;
  disabled?: boolean;
  style?: TextStyle;
  styleDisabled?: TextStyle;
  containerStyle?: ViewStyle | ViewStyle[];
  disabledContainerStyle?: ViewStyle;
  childGroupStyle?: ViewStyle;
  androidBackground?: ImageBackground;
  onPress?: () => void;
};

const TextButton: FC<Props> = (props): ReactElement => {
  const buttonProps = {
    ...props,
    activeOpacity: props.activeOpacity ?? 0.7,
  };

  // prettier-ignore
  return (
    <View>
      <Button {...buttonProps}>
        {props.children}
      </Button>
    </View>
  );
};

export default TextButton;
