import { StatusBar, StatusBarStyle } from 'react-native';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import store from '../store/store';
import { isAndroid } from './platform';

export function setStatusBarTranslucent() {
  const isLightMode = store.getState().settings.colorScheme === 'light';
  const barStyle: StatusBarStyle = isLightMode ? 'dark-content' : 'light-content';
  StatusBar.setBarStyle(barStyle, true);
  if (isAndroid()) {
    changeNavigationBarColor('transparent', isLightMode, true);
    StatusBar.setTranslucent(true);
    StatusBar.setBackgroundColor('transparent');
  }
}
