import React, { FC, ReactElement } from 'react';
import { View, Switch, TouchableOpacity, Text } from 'react-native';
import { SwitchStyles } from '../styles/globalStyles';

interface Props {
  activeOpacity: number;
  styles: SwitchStyles;
  onValueChange: (value: boolean) => void;
  text: string;
  value: boolean;
}

const SwitchSettings: FC<Props> = (props): ReactElement => {
  const { activeOpacity, styles, onValueChange, text, value } = props;
  return (
    <View style={styles.switchContainer}>
      <Switch
        thumbColor={styles.switchColors.switchThumbColor}
        trackColor={styles.switchColors.switchTrackColor}
        value={value}
        onValueChange={onValueChange}
      />
      <TouchableOpacity
        style={styles.switchContainer}
        activeOpacity={activeOpacity}
        onPress={() => onValueChange(!value)}>
        <Text style={styles.switchText}>{text}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SwitchSettings;
