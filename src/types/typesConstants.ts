import { ColorSchemeName, ImageRequireSource, ModalPropsIOS } from 'react-native';
import { QuotesSources, Quote, QuotesListKeys, Status } from './types';
import exceptions from '../constants/exceptions';

export const HTTP = 'http:' as const;
export const HTTPS = 'https:' as const;

type AvatarKeys = 'default' | 'heartGreen' | QuotesListKeys;
export type Avatars = {
  [key in AvatarKeys]: ImageRequireSource;
};

type exceptionsKeys = keyof typeof exceptions;
export type exceptions = {
  [x in exceptionsKeys]: string;
};

interface Defaults {
  colorScheme: ColorSchemeName;
  enabledQuotesSources: QuotesListKeys[];
  followDeviceColorScheme: boolean;
  followDeviceTimeFormat: boolean;
  is24Hour: boolean;
  onlineAvatar: string;
  quote: Quote;
  quoteNotifStartTime: Date;
  quoteNotifEndTime: Date;
  quoteNotifStatus: Status;
  quotesDaily: number;
  quotesSources: QuotesSources;
}

export interface Constants {
  defaults: Defaults;
  dimensions: {
    lowScreenHeightLandscape: number;
    lowScreenHeightPortrait: number;
    lowScreenWidth: number;
  };
  keys: {
    googleApiKey: string;
    revenueCatApiKey: string;
  };
  links: {
    buyMeACoffee: string;
    patreon: string;
    paypal: string;
  };
  notification: {
    channel: string;
    channelName: string;
    channelDescription: string;
  };
  protocols: {
    http: string;
    https: string;
    supportedProtocols: string[];
  };
  quotes: {
    sourceAI: string;
    noEnabledQoutesSources: Quote;
    noQuotesExistsForEnabledSources: Quote;
  };
  supportedOrientations: ModalPropsIOS['supportedOrientations'];
  texts: {
    enterUrl: string;
    enterUrlHelpText: string;
    supportMesssage: string;
  };
}
