export function hoursToMillis(hours: number) {
  return hours * 60 * 60 * 1000;
}

export function millisToHours(millis: number) {
  return millis / 1000 / 60 / 60;
}

export function getTimeStamp() {
  const date = new Date();
  const hours = date.getHours().toString().padStart(2, '0');
  const mins = date.getMinutes().toString().padStart(2, '0');
  const secs = date.getSeconds().toString().padStart(2, '0');
  const millis = date.getMilliseconds().toString().padStart(3, '0');
  const timeStamp = `${date.toString().split(' ')[0]} ${hours}:${mins}:${secs}.${millis}`; // Extract day of week and concat time...
  return timeStamp;
}

/**
 * Used for test/debugging
 */
export function adjustTime(currentTime: number, hours: number = 0, minutes: number = 0) {
  const dateObj = new Date(currentTime);
  const currentHours = dateObj.getHours();
  const currentMinutes = dateObj.getMinutes();
  dateObj.setHours(currentHours + hours);
  dateObj.setMinutes(currentMinutes + minutes);
  return dateObj.getTime();
}

export function formatDate(date = new Date()) {
  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  // const [month, date, year] = date.toLocaleDateString('en-US').split('/'); // Not working on Android release version (yet), probably Hermes issue.
  const dd = String(date.getDate()).padStart(2, '0');
  const mm = String(date.getMonth() + 1).padStart(2, '0');
  const yyyy = date.getFullYear();
  const textDate = Number(dd) + '-' + monthNames[date.getMonth()] + '-' + yyyy;
  const numberDateUS = `${mm}/${dd}/${yyyy}`;
  const numberDateEU = `${dd}/${mm}/${yyyy}`;
  return { textDate, numberDateUS, numberDateEU };
}

export function formatTime(date = new Date()) {
  const [hour, minute, second] = date.toLocaleTimeString('en-US').split(/:| /u);
  const hhmmss = `${hour}:${minute}:${second}`;
  const hhmm = `${hour}:${minute}`;
  return { hhmmss, hhmm };
}
