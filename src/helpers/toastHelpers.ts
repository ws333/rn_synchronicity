import store from '../store/store';
import { setToast } from '../store/actions/appActions';
import { ToastParams } from '../types/types';

export function showToast({ title, message, type = 'success', visibilityTime = 2000 }: ToastParams) {
  store.dispatch(
    setToast({
      type: type,
      position: 'top',
      text1: title,
      text2: message,
      visibilityTime: visibilityTime,
      onHide: () => {
        hideToast();
      },
    }),
  );
}

export function hideToast() {
  const { toast } = store.getState().app;
  if (toast && toast.type !== 'hidden') {
    store.dispatch(setToast({ ...toast, type: 'hidden' }));
  }
}
