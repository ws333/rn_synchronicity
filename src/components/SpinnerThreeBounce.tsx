import React, { FC, ReactElement } from 'react';
import { Modal, ModalBaseProps, View, ViewStyle } from 'react-native';
import Spinner from 'react-native-spinkit';
import { ThemeColors } from '../types/types';
import { constants } from '../constants/constants';

interface Props {
  isVisible: boolean;
  colors: ThemeColors;
  spinnerSize: number;
  animationType?: ModalBaseProps['animationType'];
  noModal?: boolean;
}

const SpinnerThreeBounce: FC<Props> = (props): ReactElement => {
  const { animationType, colors, isVisible, spinnerSize } = props;

  const centeredView: ViewStyle = {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: props.noModal ? 'transparent' : colors.backdropSpinner,
  };

  const SpinnerCentered = () => (
    <View style={centeredView}>
      <Spinner style={{ marginBottom: spinnerSize / 2 }} size={spinnerSize} type="ThreeBounce" color={colors.text} />
    </View>
  );
  return (
    <View>
      {props.noModal ? (
        <SpinnerCentered />
      ) : (
        <Modal
          animationType={animationType ?? 'none'}
          visible={isVisible}
          transparent={true}
          supportedOrientations={constants.supportedOrientations}>
          <SpinnerCentered />
        </Modal>
      )}
    </View>
  );
};

export default SpinnerThreeBounce;
