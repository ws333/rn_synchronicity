import React, { FC } from 'react';
import { TextStyle, ViewStyle } from 'react-native';
import Toast, { BaseToast } from 'react-native-toast-message';
import { hideToast } from '../helpers/toastHelpers';
import { ThemeColors } from '../types/types';
import { styleConstants } from '../styles/globalStyles';

interface Props {
  style: ViewStyle;
  colors: ThemeColors;
  height: number;
  topOffset: number;
}

const ToastMessage: FC<Props> = (props) => {
  const { colors, height, style, topOffset } = props;
  const localStyles: ToastStyles = toastStyles(colors, height);
  const toastConfig = {
    success: ({ ...rest }) => (
      <BaseToast
        {...rest}
        style={localStyles.baseToastStyle}
        contentContainerStyle={localStyles.contentContainerStyle}
        text1Style={localStyles.text1Style}
        text2Style={localStyles.text2Style}
        onPress={handleOnPress}
        onLeadingIconPress={handleOnPress}
        onTrailingIconPress={handleOnPress}
        activeOpacity={1}
      />
    ),
  };

  function handleOnPress() {
    hideToast();
  }

  return <Toast style={style} config={toastConfig} ref={(ref) => Toast.setRef(ref)} topOffset={topOffset} />;
};

export default ToastMessage;
export { Toast, BaseToast };

interface ToastStyles {
  baseToastStyle: ViewStyle;
  contentContainerStyle: ViewStyle;
  text1Style: TextStyle;
  text2Style: TextStyle;
}

export const toastConstants = {
  top: -150, // Needed to hide toast fully when rotating device
  height: 55,
  text1FontSize: styleConstants.fontSizeMedium,
  text2FontSize: styleConstants.fontSizeBase,
};

function toastStyles(colors: ThemeColors, height: number): ToastStyles {
  return {
    baseToastStyle: {
      height: height ?? toastConstants.height,
      borderLeftColor: colors.toastBorderLeft,
      backgroundColor: colors.background,
      shadowColor: colors.shadowColorMenuGrid,
      ...styleConstants.shadow17,
      elevation: 22,
    },
    contentContainerStyle: {
      marginVertical: styleConstants.marginVH / 2,
      paddingHorizontal: styleConstants.padding / 2,
    },
    text1Style: {
      marginTop: styleConstants.marginLRTB / 2,
      fontSize: toastConstants.text1FontSize,
      color: colors.text,
    },
    text2Style: {
      marginBottom: styleConstants.marginLRTB / 2,
      fontSize: toastConstants.text2FontSize,
      color: colors.text,
    },
  };
}
