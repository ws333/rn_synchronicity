import { ViewStyle, TextStyle } from 'react-native';
import { isTablet } from '../helpers/device';
import { getDimensionsState, getInfoSectionWidth } from '../helpers/screenDimensions';
import { ThemeColors } from '../types/types';
import { styleConstants } from './globalStyles';

export interface LocalStyles {
  brainwavePaper: ViewStyle;
  brainWaveContent: ViewStyle;
  brainwaveChooseSessionText: TextStyle;
  brainwaveInfoText: TextStyle;
  youtubeContainer: ViewStyle;
  youTubeComponent: ViewStyle;
}

const fontSize = isTablet() ? styleConstants.fontSizeLarge : styleConstants.fontSizeMedium;

module.exports = (colors: ThemeColors): LocalStyles => {
  const infoWidthInPercent = getInfoSectionWidth();
  const dimensionsState = getDimensionsState();
  const brainwavePaperWidth = (dimensionsState.width / 100) * parseInt(infoWidthInPercent, 10);
  const youTubeComponentWidthInPercent = '87%';
  const youTubeComponentWidth = (dimensionsState.width / 100) * parseInt(youTubeComponentWidthInPercent, 10);
  return {
    brainwavePaper: {
      marginHorizontal: styleConstants.marginVH,
      marginBottom: styleConstants.marginLRTB / 2,
      borderRadius: styleConstants.radius,
      borderWidth: styleConstants.borderWidth,
      width: brainwavePaperWidth,
      backgroundColor: colors.brainwavePaper,
      shadowColor: colors.shadowColor,
      borderColor: colors.border,
      ...styleConstants.shadow11,
    },
    brainWaveContent: {
      flex: 1,
      flexGrow: 1,
    },
    brainwaveChooseSessionText: {
      marginHorizontal: styleConstants.marginVH,
      marginVertical: styleConstants.marginVH,
      textAlign: 'center',
      fontSize: fontSize,
      color: colors.text,
    },
    brainwaveInfoText: {
      marginHorizontal: styleConstants.marginVH,
      marginVertical: styleConstants.marginVH,
      textAlign: 'justify',
      fontSize: fontSize,
      color: colors.text,
    },
    youtubeContainer: {
      flex: 1,
      flexGrow: 1,
      alignItems: 'center',
      marginBottom: styleConstants.marginLRTB,
      borderRadius: styleConstants.radius,
      borderWidth: 7,
    },
    youTubeComponent: {
      alignSelf: 'stretch',
      height: '100%',
      width: youTubeComponentWidth,
      backgroundColor: 'black',
    },
  };
};
