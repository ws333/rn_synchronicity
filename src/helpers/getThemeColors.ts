import { ColorSchemeName } from 'react-native';
import { ColorKeys, themeColors } from '../styles/themeColors';
import { ThemeColors } from '../types/types';

export function getThemeColors(colorScheme: ColorSchemeName): ThemeColors {
  const colors: Partial<ThemeColors> = {};
  try {
    if (!colorScheme) throw new Error('No colorScheme specified');
    for (const key in themeColors) {
      if (Object.prototype.hasOwnProperty.call(themeColors, key)) {
        colors[key as ColorKeys] = themeColors[key as ColorKeys][colorScheme];
      }
    }
  } catch (error) {
    global.log('file: getAppColors.ts -> exception:', error).yellowOnBlack();
  }
  return colors as ThemeColors;
}
