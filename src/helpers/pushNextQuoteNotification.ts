import store from '../store/store';
import { Quote } from '../types/types';
import { getRandomQuote } from './getRandomQuote';
import { calcNextNotifTime } from './calcNextNotifTime';
import { storeQuoteNotification } from './quoteNotificationStorage';
import { scheduleLocalNotification } from './scheduleLocalNotification';

/**
 * Argument firstExecution is true when enabling notification Schedule from SettingsScreen else false
 */
type pushNextQuoteNotificationArgs = { quotesDaily: number; quote?: Quote; firstExecution?: boolean };
export const pushNextQuoteNotification = async ({
  quotesDaily,
  quote,
  firstExecution = false,
}: pushNextQuoteNotificationArgs) => {
  const { quoteNotifSchedule } = store.getState().settings;
  const nextNotifTime = await calcNextNotifTime(quoteNotifSchedule, quotesDaily, firstExecution);

  if (nextNotifTime === 0) {
    global.log('[pushNextQuoteNotification] async -> nextNotifTime is 0, NOT pushing notif!').redOnBlack();
    return false;
  }

  const nextQuote = getRandomQuote({ currentQuote: quote });
  const date = new Date(nextNotifTime);
  await storeQuoteNotification(nextQuote, date);
  scheduleLocalNotification({ title: nextQuote.source, message: nextQuote.text, date });

  return true;
};
