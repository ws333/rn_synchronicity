import { ViewStyle, TextStyle } from 'react-native';
import { isTablet } from 'react-native-device-info';
import { ThemeColors } from '../types/types';
import { getInfoSectionWidth, getIsSmallScreenHeight } from '../helpers/screenDimensions';
import { styleConstants } from './globalStyles';

export interface LocalStyles {
  credits: TextStyle;
  FAQPaper: ViewStyle;
  FAQContent: ViewStyle;
  FAQItemContainer: ViewStyle;
  FAQSpacer: ViewStyle;
  FAQTextQ: TextStyle;
  FAQTextA: TextStyle;
}

module.exports = (colors: ThemeColors): LocalStyles => {
  const fontSizeBase = isTablet() ? styleConstants.fontSizeLarge : styleConstants.fontSizeBase;
  const fontSize = getIsSmallScreenHeight() ? fontSizeBase : fontSizeBase + 3;
  const infoSectionWidth = getInfoSectionWidth();
  return {
    credits: {
      textAlign: 'justify',
      opacity: 1,
      fontSize: fontSize,
      fontStyle: 'italic',
      color: colors.quoteButtonText,
    },
    FAQPaper: {
      flex: 1,
      marginHorizontal: styleConstants.marginVH,
      borderRadius: styleConstants.radius,
      borderWidth: styleConstants.borderWidth,
      width: infoSectionWidth,
      backgroundColor: colors.FAQPaper,
      shadowColor: colors.shadowColor,
      borderColor: colors.border,
      ...styleConstants.shadow11NoOffset,
    },
    FAQItemContainer: {
      marginHorizontal: styleConstants.marginVH,
    },
    FAQContent: {
      flex: 1,
      /**
       * flexGrow hack needed when GoBackButton is visible (i.e. on iOS)
       * https://joren.co/flex-grow-9999-hack/
       */
      flexGrow: 9999,
      marginBottom: styleConstants.marginLRTB / 2,
    },
    FAQTextQ: {
      marginVertical: styleConstants.marginVH / 2,
      fontStyle: 'italic',
      fontWeight: 'bold',
      opacity: 1,
      fontSize: fontSize + 5,
      color: colors.text,
    },
    FAQTextA: {
      textAlign: 'justify',
      opacity: 1,
      fontSize: fontSize,
      color: colors.text,
    },
    FAQSpacer: {
      height: styleConstants.marginVH,
    },
  };
};
