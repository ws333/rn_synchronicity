import React, { useState, useEffect, ReactElement, FC } from 'react';
import { ScrollView, Text, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import InputSpinner from 'react-native-input-spinner';
import { ScreenProps } from '../types/types';
import ClearLogButton from '../components/ClearLogButton';
import { clearStoredLog, logStorageLength, setStoredLogLevel } from '../services/logToStorage';
import { isIOS } from '../helpers/platform';
import { activeOpacity, colors, globalStyles, localStyles } from '../styles/logScreenStyles';
import TextButtonAppTheme from '../components/TextButtonAppTheme';
import { GlobalStyles } from '../styles/globalStyles';
import { arraysAreEqual } from '../helpers/arraysAreEqual';

const LogScreen: FC<ScreenProps> = (props): ReactElement => {
  console.log('file: LogScreen.tsx -> Component rendering... -> props', { ...props });
  const [logState, setLogState] = useState(['Reading log from storage...']);
  const [logLevelState, setLogLevelState] = useState(global.logLevel);
  const styles = globalStyles();

  useEffect(() => {
    let mounted = true;
    async function fetchStoredLog() {
      const storedLog = (await AsyncStorage.getItem('LOG')) ?? '["Not able to fetch stored log!"]';
      const parsedLog = JSON.parse(storedLog);
      const logHasChanged = !arraysAreEqual(logState, parsedLog);
      if (mounted && logHasChanged) setLogState(parsedLog);
    }
    fetchStoredLog();
    return () => {
      mounted = false;
    };
  });

  async function handleOnPressClearLogButton() {
    const logItems = await clearStoredLog();
    setLogState(logItems);
  }

  async function handleOnChangeLogLevel(value: LogLevel) {
    global.logLevel = value;
    setLogLevelState(value);
    await setStoredLogLevel(value);
  }

  const LogContent = (
    <View style={localStyles.logPaper}>
      <ScrollView contentContainerStyle={styles.scrollView} alwaysBounceVertical={false}>
        {logState.map((logItem, i) => (
          // eslint-disable-next-line react/no-array-index-key
          <Text key={i} style={localStyles.logText}>
            {logItem}
          </Text>
        ))}
      </ScrollView>
    </View>
  );

  return (
    <View style={styles.screen}>
      <View style={styles.screenSpacerTop} />

      <Text style={localStyles.logTitle}>Stored log</Text>
      <Text style={localStyles.logSubtitle}>Last {logStorageLength} items - newest on top</Text>
      <View style={localStyles.logContent}>{LogContent}</View>

      <View style={localStyles.buttonContainer}>
        <View style={localStyles.button}>
          <ClearLogButton
            onPress={handleOnPressClearLogButton}
            colors={colors}
            styles={styles}
            activeOpacity={activeOpacity}
          />
        </View>
        <View style={localStyles.logLevelContainer}>
          <Text style={localStyles.logLevelText}>Log level</Text>
          <InputSpinner
            {...localStyles.inputSpinnerColors}
            {...localStyles.inputSpinner}
            min={0}
            max={5}
            step={1}
            value={logLevelState}
            editable={false}
            placeholderTextColor="grey"
            onChange={handleOnChangeLogLevel}
          />
        </View>
        {isIOS() && (
          <View style={localStyles.button}>
            <TextButtonAppTheme
              dark
              colors={colors}
              activeOpacity={activeOpacity}
              onPress={props.handleBackPress!}
              styles={styles as GlobalStyles}>
              Go back
            </TextButtonAppTheme>
          </View>
        )}
      </View>
      <View style={styles.screenSpacerBottom} />
    </View>
  );
};

export default LogScreen;
