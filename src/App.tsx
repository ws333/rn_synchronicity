import React, { useEffect, ReactElement, FC } from 'react';
import { AppState, AppStateStatus, BackHandler } from 'react-native';
import { Provider } from 'react-redux';
import store from './store/store';
import { hydrateRedux } from './store/hydrateRedux';
import { setColorScheme, setIs24Hour } from './store/actions/settingsActions';
import { setQuoteIsWaiting } from './store/actions/appActions';
import NavigationWrapper from './NavigationWrapper';
import useOrientation from './hooks/useOrientation';
import PushService from './services/PushService';
import { isIOS } from './helpers/platform';
import { getColorScheme } from './helpers/getApperance';
import { handleBackPress } from './helpers/handleBackPress';
import { getDeviceTimeFormat } from './helpers/getDeviceTimeFormat';
import { displayStoredQuoteNotification } from './helpers/displayStoredQuoteNotification';

hydrateRedux();

const isHermes = () => global.HermesInternal !== null;

const App: FC = (): ReactElement => {
  global.log('[App] -> Component rendering... isHermes =', isHermes()).green();

  useOrientation(); // Add orientation listener

  useEffect(() => {
    /**
     * notifIsPressed is used to prevent displayNotification from being called
     * by both onNotification handler and AppState change.
     */
    let notifIsPressed = false;

    function displayNotification() {
      const { isHydrated } = store.getState().settings;
      if (isHydrated) {
        displayStoredQuoteNotification();
      } else {
        store.dispatch(setQuoteIsWaiting(true));
        global.log('[App] -> displayNotification -> dispatch(setQuoteIsWaiting(true))').yellowOnGreen();
      }
    }

    /**
     * Calling displayNotification since no AppState change event on iOS on app launch
     */
    if (isIOS()) {
      displayNotification();
      global.log('[App] -> useEffect() -> isIOS() -> display stored notif on app launch').yellowOnRed();
    }

    function handleOnNotification() {
      global.log('[App] -> handleOnNotification() -> executing displayNotification()!').yellowOnBlack();
      notifIsPressed = true;
      displayNotification();
    }

    PushService.setCallback(handleOnNotification);

    /**
     * prevAppState used to prevent displaying stored notification when user opens notification center on iOS
     * which trigger the changes 'inactive' - 'active' - 'inactive'
     */
    let prevAppState: AppStateStatus = 'unknown';

    async function handleAppStateChange(appState: AppStateStatus) {
      // prettier-ignore
      global.log('[App] -> handleAppStateChange() -> appState, prevAppState, notifIsPressed is', appState, prevAppState, notifIsPressed).blackOnRed();

      if (!notifIsPressed && appState === 'active' && prevAppState !== 'inactive') {
        global.log('[App] -> handleAppStateChange() ->  executing displayNotification()!').yellowOnBlack();
        displayNotification();
      }

      prevAppState = appState;
      notifIsPressed = false;

      /**
       * Reflect changes in device colorScheme
       * Checking isHydrated since Android trigger AppState change event on app launch before redux store is hydrated.
       */
      const { isHydrated } = store.getState().settings;
      const { followDeviceColorScheme, followDeviceTimeFormat } = store.getState().settings;

      if (isHydrated && followDeviceColorScheme) {
        const deviceColorScheme = getColorScheme();
        store.dispatch(setColorScheme(deviceColorScheme, false));
      }

      if (isHydrated && followDeviceTimeFormat) {
        const deviceTimeFormat = await getDeviceTimeFormat();
        store.dispatch(setIs24Hour(deviceTimeFormat, false));
      }
    }

    AppState.addEventListener('change', handleAppStateChange);
    BackHandler.addEventListener('hardwareBackPress', handleBackPress);
    return () => {
      AppState.removeEventListener('change', handleAppStateChange);
      BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
    };
  }, []);

  return (
    <Provider store={store}>
      <NavigationWrapper />
    </Provider>
  );
};

export default App;
