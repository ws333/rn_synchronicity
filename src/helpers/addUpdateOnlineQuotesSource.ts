import store from '../store/store';
import { setQuotes } from '../store/actions/appActions';
import { setQuotesSources } from '../store/actions/settingsActions';
import { OnlineQuotesObject, Quote, QuotesSource } from '../types/types';
import { AddUpdateSourceCallback } from '../types/typesAlerts';
import { fetchWithTimeout } from './fetchWithTimeout';
import { exceptionHandler } from './addUpdateQuoteSourceExceptionHandler';
import { getQuotesFromJson } from './getQuotes';
import { sentrySendMessage } from './sentryHelpers';
import { getStoredData, storeData } from '../services/storage';
import exceptions from '../constants/exceptions';
import { constants } from '../constants/constants';
import { formatDate, formatTime } from './time';

export const addUpdateOnlineQuotesSource = async (
  url: string,
  callback?: AddUpdateSourceCallback,
  setShowSpinnerState?: React.Dispatch<React.SetStateAction<boolean>>,
): Promise<string> => {
  try {
    const parsedUrl = new URL(url);
    const { supportedProtocols } = constants.protocols;
    if (!supportedProtocols.includes(parsedUrl.protocol)) {
      throw new Error(`${exceptions.protocolNotSupported} ${parsedUrl.protocol}`);
    }
    global.log('[addOnlineQuotesSource] -> addOnlineQuotesSource() -> url =', parsedUrl.href).yellowOnRed();
    if (!parsedUrl.href) throw new Error('url not valid');

    const response = await fetchWithTimeout(parsedUrl.href);
    if (!response.ok) throw new Error(String(exceptions.httpError + response.status));

    const sourcesState = store.getState().settings.quotesSources;

    /**
     * response.json() returns a JS object parsed from the json file and throws an exception if the file content is invalid.
     * The only required property is qoutes, the others has fallbacks when building the new QuotesSource object below.
     */
    const quotesSource: Partial<OnlineQuotesObject> = await response.json();
    global.log('[addOnlineQuotesSource] -> addOnlineQuotesSource -> quotesSource =', quotesSource).yellowOnGreen();
    if (!quotesSource?.quotes?.length) throw new Error(exceptions.quotesLengthZero);

    const nextNumber = Object.keys(sourcesState).length + 1;
    const nextNumberPadded = nextNumber.toString().padStart(2, '0');
    const defaultName = `Quotes #${nextNumberPadded}`;

    const updatedDate = formatDate().textDate;
    const updatedTime = formatTime().hhmmss;
    const updatedDateText = `${updatedDate} ${updatedTime}`;

    const newItem: QuotesSource = {
      name: quotesSource.name ?? defaultName,
      subtitle: quotesSource.subtitle ?? '',
      category: 'Online',
      url: parsedUrl.href,
      updatedDate: updatedDateText,
      imageUrl: quotesSource.imageUrl ?? constants.defaults.onlineAvatar,
      enabled: true,
    };

    const quotesState = store.getState().app.quotes;
    let quotes = [...quotesState];
    let storedOnlineQutes: Quote[] = (await getStoredData('ONLINE_QUOTES')) ?? [];

    /**
     * Source already exists:
     *  - Apply current enabled status
     *  - Remove existing quotes
     */
    if (quotesSource?.name && Object.keys(sourcesState).includes(quotesSource.name)) {
      const enabledStatus = sourcesState[quotesSource.name].enabled;
      newItem.enabled = enabledStatus;
      // prettier-ignore
      global.log('[addOnlineQuotesSource] -> addOnlineQuotesSource() -> Update! Removing quotes for key:', quotesSource.name).yellowOnRed();
      quotes = quotes.filter((quote) => quote.source !== quotesSource.name);
      storedOnlineQutes = storedOnlineQutes?.filter((quote) => quote.source !== quotesSource.name);
    }

    const newQuotes = getQuotesFromJson({ [newItem.name]: quotesSource.quotes });
    const mergedOnlineQuotes = newQuotes.concat(storedOnlineQutes);
    await storeData('ONLINE_QUOTES', mergedOnlineQuotes);

    const mergedQuotes = quotes.concat(newQuotes);
    store.dispatch(setQuotes(mergedQuotes));
    const mergedQuotesSources = { ...sourcesState, [newItem.name]: newItem };
    store.dispatch(setQuotesSources(mergedQuotesSources));

    if (callback) {
      callback({ closeCaller: true, key: newItem.name });
    }

    sentrySendMessage('Someone added an online quote source: ' + newItem.name);
    return newItem.name;
  } catch (e) {
    global.log('[addOnlineQuotesSource] -> addOnlineQuotesSource() -> exception:', String(e)).yellowOnBlack();
    if (setShowSpinnerState) {
      setShowSpinnerState(false);
    }
    exceptionHandler(e, callback);
    /**
     * Return empty string to signify false
     */
    return '';
  }
};
