import { Torientation, Quote, ToastState } from '../../types/types';
import { AlertState } from '../../types/typesAlerts';
// prettier-ignore
import {
  AppActionTypes,
  ActiveScreen,
  GOTO_PREVIOUS_SCREEN,
  SET_ACTIVE_SCREEN,
  SET_ALERT,
  SET_ORIENTATION,
  SET_QUOTE,
  SET_QUOTES,
  SET_QUOTE_IS_WAITING,
  SET_SELECTED_LIST_ITEM,
  SET_TOAST,
  SET_ADD_SOURCE_BUTTON_HEIGHT,
} from '../../types/typesRedux';

export const gotoPreviousScreen = (): AppActionTypes => {
  return { type: GOTO_PREVIOUS_SCREEN };
};
export const setActiveScreen = (screenAndProps: ActiveScreen): AppActionTypes => {
  return { type: SET_ACTIVE_SCREEN, value: screenAndProps };
};
export const setAlert = (alertState: AlertState): AppActionTypes => {
  return { type: SET_ALERT, value: alertState };
};
export const setOrientation = (orientation: Torientation): AppActionTypes => {
  return { type: SET_ORIENTATION, value: orientation };
};
export const setQuote = (quote: Quote): AppActionTypes => {
  return { type: SET_QUOTE, value: quote };
};
export const setQuoteIsWaiting = (isWaiting: boolean): AppActionTypes => {
  return { type: SET_QUOTE_IS_WAITING, value: isWaiting };
};
export const setQuotes = (quotes: Quote[]): AppActionTypes => {
  return { type: SET_QUOTES, value: quotes };
};
export const setSelectedListItem = (key: string): AppActionTypes => {
  return { type: SET_SELECTED_LIST_ITEM, value: key };
};
export const setToast = (toast: ToastState): AppActionTypes => {
  return { type: SET_TOAST, value: toast };
};
export const setAddSourceButtonHeight = (addSourceButtonHeight: number): AppActionTypes => {
  return { type: SET_ADD_SOURCE_BUTTON_HEIGHT, value: addSourceButtonHeight };
};
