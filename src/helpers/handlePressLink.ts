import { Linking } from 'react-native';

export function handlePressLink(url: string) {
  Linking.openURL(url);
}
