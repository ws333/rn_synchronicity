import { getDeviceTimeFormat } from './getDeviceTimeFormat';
import { getStoredData } from '../services/storage';

export async function getStoredIs24Hour() {
  const storedIs24Hour: boolean | null = await getStoredData('IS_24_HOUR');
  const is24Hour: boolean = storedIs24Hour ?? (await getDeviceTimeFormat());
  return is24Hour;
}
