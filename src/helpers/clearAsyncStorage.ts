import store from '../store/store';
import { setQuote } from '../store/actions/appActions';
import { hydrateRedux } from '../store/hydrateRedux';
import { removeStoredData } from '../services/storage';
import { constants } from '../constants/constants';

export const clearAsyncStorage = async () => {
  await removeStoredData('QUOTES_SOURCES');
  await removeStoredData('ONLINE_QUOTES');
  await removeStoredData('COLOR_SCHEME');
  await removeStoredData('FOLLOW_DEVICE_COLOR_SCHEME');
  await removeStoredData('FOLLOW_DEVICE_TIME_FORMAT');
  await removeStoredData('IS_24_HOUR');
  await removeStoredData('QUOTES_DAILY');
  await removeStoredData('QUOTE_NOTIF');
  await removeStoredData('QUOTE_NOTIF_STATUS');
  await removeStoredData('QUOTE_NOTIF_START_TIME');
  await removeStoredData('QUOTE_NOTIF_END_TIME');
  await hydrateRedux();
  store.dispatch(setQuote(constants.defaults.quote));
};
