import store from '../store/store';
import { setAlert } from '../store/actions/appActions';
import { AlertState } from '../types/typesAlerts';

export function alertIAPException(cb: () => void, error: string) {
  const handleOnPressConfirm = () => {
    store.dispatch(setAlert(null));
    cb();
  };
  let message = `\n${error}`;
  if (String(error).startsWith('Error: The device or user is not allowed')) {
    message += '\n\nPlease go to Android settings and check if you have configured a valid Google Play account.';
  }
  const newAlertState: AlertState = {
    type: 'Message',
    title: 'In app purchase',
    message,
    showCancel: false,
    confirmText: 'Close',
    handleOnPressConfirm,
  };
  store.dispatch(setAlert(newAlertState));
}

export function alertAppException(cb: () => void, error: string) {
  const handleOnPressConfirm = () => {
    store.dispatch(setAlert(null));
    cb();
  };
  const message = `\n${error}\n\nPlease send feedback to the developers\nsupport@livinglight.one`;
  const newAlertState: AlertState = {
    type: 'Exception',
    title: 'An unexpected app error has occured...',
    message,
    showCancel: false,
    confirmText: 'Close',
    handleOnPressConfirm,
  };
  store.dispatch(setAlert(newAlertState));
}
