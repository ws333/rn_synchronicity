import { quotesSources } from '../constants/quotesSources';
import { Quote, QuotesList, QuotesListKeys } from '../types/types';

export function getQuotesFromJson(quotesList: QuotesList) {
  const quotes: Quote[] = [];
  const quotesListKeys = Object.keys(quotesList);
  quotesListKeys.forEach((key) =>
    quotesList[key].forEach((quote: string): void => {
      quotes.push({ source: key, text: quote });
    }),
  );
  return quotes;
}

export function getQuotesInApp() {
  const quotesList: QuotesList = {};
  const quotesSourcesKeys = Object.keys(quotesSources);
  quotesSourcesKeys.forEach((key): void => {
    quotesList[key] = quotesSources[key as QuotesListKeys].quotes;
  });
  return getQuotesFromJson(quotesList);
}

export function getUniqueQuoteSources(quotes: Quote[]) {
  return [...new Set(quotes.map((quote) => quote.source))].sort();
}
