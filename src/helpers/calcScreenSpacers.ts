import { Platform, StatusBar } from 'react-native';
import { isTablet } from 'react-native-device-info';
import { getIsPortrait, getIsSmallScreenHeight, getNavbarHeightAndroid } from './screenDimensions';
import { isIOS } from './platform';
import { styleConstants } from '../styles/globalStyles';

export function calcScreenSpacer() {
  return getIsSmallScreenHeight() ? 0 : styleConstants.screenSpacer;
}

function calcTopIOS(base: number, isPortrait: boolean): number {
  const topIOSPortrait = (styleConstants.screenSpacer / 2) * 7 + base;
  const topIOSLandscape = (styleConstants.screenSpacer / 2) * 3 + base;
  return isPortrait ? topIOSPortrait : topIOSLandscape;
}

function calcTopAndroid(base: number, isPortrait: boolean): number {
  const topAndroidPortrait = Number(StatusBar.currentHeight) + styleConstants.screenSpacer * 1.3 + base;
  const topAndroidLandscape = Number(StatusBar.currentHeight) + styleConstants.screenSpacer / 2 + base;
  return isPortrait ? topAndroidPortrait : topAndroidLandscape;
}

export function calcScreenSpacerTop() {
  const isPortrait = getIsPortrait();
  const baseTablet = (styleConstants.screenSpacer / 2) * 3;
  const base = isTablet() ? baseTablet : 0;
  const screenSpacerTop =
    Platform.select({
      ios: calcTopIOS(base, isPortrait),
      android: calcTopAndroid(base, isPortrait),
    }) || styleConstants.screenSpacer;
  return screenSpacerTop;
}

export function calcScreenSpacerBottom() {
  const base = isTablet() ? (styleConstants.screenSpacer / 2) * 3 : styleConstants.screenSpacer / 2;
  const bottomIOS = styleConstants.screenSpacer + base;
  const bottomAndroid = getNavbarHeightAndroid() + styleConstants.screenSpacer + base;
  const screenSpacerBottom = isIOS() ? bottomIOS : bottomAndroid;
  return getIsSmallScreenHeight()
    ? screenSpacerBottom / styleConstants.smallScreenSpacerBottomFactor
    : screenSpacerBottom;
}
