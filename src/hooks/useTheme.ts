import { StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import { RootState } from '../store/store';
import { calcScreenSpacer, calcScreenSpacerBottom, calcScreenSpacerTop } from '../helpers/calcScreenSpacers';
import { globalStyles, GlobalStyles, styleConstants } from '../styles/globalStyles';

const useTheme = () => {
  global.log('[useTheme] -> start executing...').green();

  const colorsState = useSelector((state: RootState) => state.settings.themeColors);
  const colorSchemeState = useSelector((state: RootState) => state.settings.colorScheme);
  const colors = colorsState;

  let styles = StyleSheet.create<GlobalStyles>(globalStyles);
  const screenSpacer = calcScreenSpacer();
  const screenSpacerTop = calcScreenSpacerTop();
  const screenSpacerBottom = calcScreenSpacerBottom();
  const isDarkMode = colorSchemeState === 'dark';

  /**
   * Add dynamic styles and colors
   */
  styles = {
    ...styles,
    header: {
      ...styles.header,
      opacity: isDarkMode ? 0.96 : 0.77,
    },
    headerPaper: {
      ...styles.headerPaper,
      backgroundColor: colors.headerPaper,
      shadowColor: colors.shadowColor,
      borderColor: colors.border,
    },
    menuGrid: {
      ...styles.menuGrid,
      backgroundColor: colors.menuGridBackground,
      shadowColor: colors.shadowColorMenuGrid,
      borderColor: colors.border,
    },
    menuGridItem: {
      ...styles.menuGridItem,
      backgroundColor: colors.menuGridItemBackground,
      shadowColor: colors.shadowColorMenuGrid,
      borderColor: colors.menuGridItemBorder,
    },
    menuGridItemText: {
      ...styles.menuGridItemText,
      color: colors.menuGridItemText,
    },
    screen: {
      ...styles.screen,
      backgroundColor: colors.background,
    },
    screenOverlay: {
      ...styles.screenOverlay,
      backgroundColor: colors.screenOverlay,
    },
    screenSpacerBottom: {
      height: screenSpacerBottom,
    },
    screenSpacerTop: {
      height: screenSpacerTop,
    },
    textButton: {
      ...styles.textButton,
      color: colors.textButtonText,
    },
    textButtonSmall: {
      ...styles.textButton,
      marginHorizontal: 11,
      marginVertical: 5,
      fontSize: styleConstants.fontSizeBase,
      color: colors.textButtonText,
    },
    textButtonContainer: {
      ...styles.textButtonContainer,
      backgroundColor: colors.textButtonBackground,
      borderColor: colors.textButtonBorder,
      shadowColor: colors.shadowColor,
    },
    textButtonContainerNoTop: {
      ...styles.textButtonContainer,
      marginTop: 0,
      marginHorizontal: styleConstants.marginVH / 2,
      backgroundColor: colors.textButtonBackground,
      borderColor: colors.textButtonBorder,
      shadowColor: colors.shadowColor,
      ...styleConstants.shadow11,
    },
  };

  const activeOpacity = Number(colors.activeOpacity);
  const activeOpacityMenuGridItem = Number(colors.activeOpacityMenuGridItem);

  return {
    activeOpacity,
    activeOpacityMenuGridItem,
    colors,
    isDarkMode,
    styles,
    screenSpacer,
    screenSpacerTop,
    screenSpacerBottom,
  };
};

export default useTheme;
