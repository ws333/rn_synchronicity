import React, { FC, ReactElement } from 'react';
import { GlobalStyles } from '../styles/globalStyles';
import TextButton from './TextButton';

type Props = {
  dark?: boolean;
  styles: Partial<GlobalStyles>;
  colors: any;
  activeOpacity: number;
  onPress: () => void;
};

const TextButtonAppTheme: FC<Props> = (props): ReactElement => {
  const textButtonStyles = {
    activeOpacity: props.activeOpacity ?? 0.5,
    containerStyle: { ...props.styles.textButtonContainer },
    style: { ...props.styles.textButton },
  };

  if (props.dark) {
    textButtonStyles.containerStyle = {
      ...textButtonStyles.containerStyle,
      backgroundColor: props.colors?.textButtonBackgroundDark ?? '#000',
    };

    textButtonStyles.style = {
      ...textButtonStyles.style,
      color: props.colors?.textButtonTextDark ?? '#fff',
    };
  }

  return (
    <TextButton
      activeOpacity={textButtonStyles.activeOpacity}
      containerStyle={textButtonStyles.containerStyle}
      style={textButtonStyles.style}
      onPress={props.onPress}>
      {props.children}
    </TextButton>
  );
};

export default TextButtonAppTheme;
