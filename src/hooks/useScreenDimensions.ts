import { useSelector } from 'react-redux';
import { RootState } from '../store/store';
import {
  getInfoSectionWidth,
  getIsSmallScreenHeightPortrait,
  getIsSmallScreenHeigthLandscape,
} from '../helpers/screenDimensions';

const useScreenDimensions = () => {
  global.log('[useScreenDimensions] -> start executing...').green();

  const dimensionsState = useSelector((state: RootState) => state.app.dimensions);
  const orientationState = useSelector((state: RootState) => state.app.orientation);

  const infoSectionWidth = getInfoSectionWidth();

  const isPortrait = orientationState === 'PORTRAIT';
  const isLandscape = orientationState === 'LANDSCAPE';

  const isSmallScreenHeightPortrait = getIsSmallScreenHeightPortrait(dimensionsState, orientationState);
  const isSmallScreenHeigthLandscape = getIsSmallScreenHeigthLandscape(dimensionsState, orientationState);
  const isSmallScreenHeight = isSmallScreenHeightPortrait || isSmallScreenHeigthLandscape;

  const spinnerSize =
    dimensionsState.width < dimensionsState.height ? dimensionsState.width / 4 : dimensionsState.height / 4;

  return {
    dimensionsState,
    orientationState,
    isPortrait,
    isLandscape,
    isSmallScreenHeight,
    infoSectionWidth,
    spinnerSize,
  };
};

export default useScreenDimensions;
