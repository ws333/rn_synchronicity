import { ViewStyle, TextStyle } from 'react-native';
import { getInfoSectionWidth } from '../helpers/screenDimensions';
import { isTablet } from '../helpers/device';
import { ThemeColors } from '../types/types';
import { styleConstants } from './globalStyles';

export interface LocalStyles {
  donateButtons: ViewStyle;
  donatePaper: ViewStyle;
  donateTextContainer: ViewStyle;
  donateText: TextStyle;
  paymentText: TextStyle;
}

module.exports = (colors: ThemeColors): LocalStyles => {
  const infoSectionWidth = getInfoSectionWidth();
  return {
    donateButtons: {
      marginVertical: styleConstants.marginLRTB / 2,
    },
    donatePaper: {
      alignItems: 'center',
      marginHorizontal: styleConstants.marginVH,
      paddingBottom: styleConstants.padding,
      borderRadius: styleConstants.radius,
      borderWidth: styleConstants.borderWidth,
      width: infoSectionWidth,
      backgroundColor: colors.donatePaper,
      shadowColor: colors.shadowColor,
      borderColor: colors.border,
      ...styleConstants.shadow11,
    },
    donateTextContainer: {
      width: '80%',
    },
    donateText: {
      textAlign: 'center',
      fontSize: isTablet() ? styleConstants.fontSizeXLarge : styleConstants.fontSizeLarge,
    },
    paymentText: {
      textAlign: 'center',
      marginTop: styleConstants.marginLRTB / 2,
      fontSize: isTablet() ? styleConstants.fontSizeMedium : styleConstants.fontSizeBase,
      fontStyle: 'italic',
    },
  };
};
