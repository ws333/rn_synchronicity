import { Dimensions } from 'react-native';
import { isTablet } from 'react-native-device-info';
import { isIOS } from './platform';
import store from '../store/store';
import { styleConstants } from '../styles/globalStyles';

export function calcScreenSpacerTop() {
  const orientationState = store.getState().app.orientation;
  const isPortrait = orientationState === 'PORTRAIT';
  const baseTablet = (styleConstants.screenSpacer / 2) * 3;
  const base = isTablet() ? baseTablet : 0;
  const topIOSPortrait = (styleConstants.screenSpacer / 2) * 7 + base;
  const topIOSLandscape = (styleConstants.screenSpacer / 2) * 3 + base;
  const topIOS = isPortrait ? topIOSPortrait : topIOSLandscape;
  const topAndroid = styleConstants.screenSpacer * 2 + base;
  const screenSpacerTop = isIOS() ? topIOS : topAndroid;
  return screenSpacerTop;
}

export const calcScreenSpacerBottom = () => {
  const dim = Dimensions.get('screen');
  const isPortrait = dim.height > dim.width;
  const bottomAndroid = isPortrait ? getNavbarHeightAndroid() : 0;
  const bottomIOS = styleConstants.screenSpacer;
  const screenSpacerBottom = isIOS() ? bottomIOS : bottomAndroid;
  return screenSpacerBottom;
};

export function getNavbarHeightAndroid() {
  const screenHeight = Dimensions.get('screen').height;
  const windowHeight = Dimensions.get('window').height;
  const navbarHeight = screenHeight - windowHeight;
  return navbarHeight;
}
