import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import { isIOS } from '../helpers/platform';
import { constants } from '../constants/constants';

export default class PushService {
  static init: () => void;
  static onNotification: (notification: typeof PushNotification) => void;
  static setCallback: (handler: () => void) => void;
  static handleOnNotification: () => void; // handleOnNotification can be set by PushService.setCallback()
}

PushService.init = () => {
  global.log('[PushService] -> init() start executing...').yellowOnGreen();
  PNconfigure();
  PNcreateChannel();
};

PushService.onNotification = (notification) => {
  global.log('[PushService] -> PushNotification.onNotification -> notification = ', { ...notification }).yellowOnRed();
  if (PushService.handleOnNotification) PushService.handleOnNotification();
  if (isIOS()) {
    // (required) Called when a remote is received or opened, or local notification is opened
    notification.finish(PushNotificationIOS.FetchResult.NoData);
  }
};

PushService.setCallback = (handler) => {
  PushService.handleOnNotification = handler;
};

function PNconfigure() {
  PushNotification.configure({
    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function (token) {
      global.log('[PushService] -> onRegister() -> token =', token).yellowOnRed();
    },

    onNotification: (notification) => {
      global.log('[PushService] -> onNotification() -> notification =', { ...notification }).yellowOnRed();
      PushService.onNotification(notification);
    },

    // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
    onAction: function (notification) {
      global.log('[PushService] -> onAction -> notification =', { ...notification }).yellowOnRed();
    },

    // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
    onRegistrationError: function (err) {
      global.log('[PushService] -> onRegistrationError -> err =', err).yellowOnRed();
    },

    /**
     * (optional) default: true
     * - Specified if permissions (ios) and token (android and ios) will requested or not,
     * - if not, you must call PushNotificationsHandler.requestPermissions() later
     * - if you are not using remote notification or do not have Firebase installed, use this:
     *     requestPermissions: Platform.OS === 'ios'
     */
    requestPermissions: isIOS(),
  });
}

function PNcreateChannel() {
  PushNotification.createChannel(
    {
      channelId: constants.notification.channel,
      channelName: constants.notification.channelName,
      channelDescription: constants.notification.channelDescription,
    },
    (created) => {
      // prettier-ignore
      global.log(`[PushService] -> PushNotification.createChannel() -> channel created? (false means it already existed) '${created}'`).yellowOnGreen();
    },
  );
}
