import { getStoredData, storeData, removeStoredData } from '../services/storage';
import { Quote, StoredNotification, QuoteNotification } from '../types/types';

export async function getStoredQuoteNotification(): Promise<StoredNotification | null> {
  try {
    return await getStoredData('QUOTE_NOTIF');
  } catch (e) {
    global.log('[quoteNotificationStorage] -> getStoredQuoteNotification() -> exception:', e).yellowOnBlack();
    return null;
  }
}

export async function storeQuoteNotification(quote: Quote, date: Date) {
  try {
    const notification: QuoteNotification = { quote, date };
    return await storeData('QUOTE_NOTIF', notification);
  } catch (e) {
    global.log('[quoteNotificationStorage] -> storeQuoteNotification() -> exception:', e).yellowOnBlack();
    return false;
  }
}

export async function removeStoredQuoteNotification() {
  try {
    return await removeStoredData('QUOTE_NOTIF');
  } catch (e) {
    global.log('[quoteNotificationStorage] -> removeStoredQuoteNotification() -> exception:', e).yellowOnBlack();
    return false;
  }
}
