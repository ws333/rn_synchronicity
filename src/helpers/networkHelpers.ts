import NetInfo from '@react-native-community/netinfo';
import store from '../store/store';
import { setAlert } from '../store/actions/appActions';
import { AlertHandlers } from '../types/typesAlerts';
import { alertNetworkOffline } from './alertsNetworkState';

export async function getNetworkState() {
  const state = await NetInfo.fetch();
  return state;
}

export function showNetworkOfflineAlert() {
  const alertHandlers: AlertHandlers = {
    handleOnPressConfirm: () => store.dispatch(setAlert(null)),
  };
  alertNetworkOffline(alertHandlers);
}

export async function executeIfOnline(cb: () => void) {
  const networkState = await getNetworkState();
  if (networkState.isConnected) {
    cb();
  } else {
    showNetworkOfflineAlert();
  }
}
