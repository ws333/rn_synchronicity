import {
  AlertNetworkRequestFailed,
  AlertHttpError404,
  AlertSyntaxErrorJsonParse,
  AlertQuotesLengthZero,
  AlertProtocolNotSupported,
  AlertDefault,
} from './alertsAddUpdateQuoteSourceExceptions';
import { AddUpdateSourceCallback } from '../types/typesAlerts';
import exceptions from '../constants/exceptions';

type Callback = AddUpdateSourceCallback;

export function exceptionHandler(error: string, callback: Callback = (_dummy: any) => {}) {
  /**
   * fetchWithTimeout() timed out
   * Fetch exception: "AbortError: Aborted"
   * Test with no internet connection
   */
  if (String(error).startsWith(exceptions.fetchTimeout)) {
    AlertNetworkRequestFailed(callback);
    return;
  }
  /**
   * Device not online or domain not valid
   * Fetch exception: "TypeError: Network request failed"
   * Test url: https://www.asdasd.one
   */
  if (String(error).startsWith(exceptions.networkRequestFailed)) {
    AlertNetworkRequestFailed(callback);
    return;
  }

  /**
   * Protocol not supported
   * Test url: hjkl://
   */
  if (String(error).includes(exceptions.protocolNotSupported)) {
    const errorText = String(error).trim();
    const protocol = errorText.split(' ').pop() ?? '';
    AlertProtocolNotSupported(callback, protocol);
    return;
  }

  /**
   * Http error 404 file not found
   * Test url: https://livinglight.one/quotes/tests/ainstain.json
   */
  if (String(error).startsWith(exceptions.httpError404)) {
    AlertHttpError404(callback);
    return;
  }

  /**
   * Syntax error in json file
   * Test utl: https://livinglight.one/quotes/tests/einstein2InvalidJson.json
   */
  if (String(error).startsWith(exceptions.syntaxErrorJsonParse)) {
    AlertSyntaxErrorJsonParse(callback, error);
    return;
  }

  /**
   * Json file is valid, but missing property: "quotes": string[]
   * Test url: https://livinglight.one/quotes/tests/einstein2MIssingQuotesKey.json
   */
  if (String(error).includes(exceptions.quotesLengthZero)) {
    AlertQuotesLengthZero(callback);
    return;
  }

  AlertDefault(callback, String(error));
}
