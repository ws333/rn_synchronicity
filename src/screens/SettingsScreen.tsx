import React, { FC, ReactElement, useState } from 'react';
import { ColorSchemeName, ImageBackground, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import InputSpinner from 'react-native-input-spinner';

import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store/store';
import { setActiveScreen, setQuote, setQuotes } from '../store/actions/appActions';
import {
  setColorScheme,
  setIs24Hour,
  setQuotesDaily,
  setQuoteNotifStartTime,
  setQuoteNotifEndTime,
  setQuoteNotifStatus,
  setFollowDeviceColorScheme,
  setFollowDeviceTimeFormat,
} from '../store/actions/settingsActions';

import { ScreenProps, TimePickerType } from '../types/types';

import TextButton from '../components/TextButton';
import GoBackButton from '../components/GoBackButton';
import SwitchSettings from '../components/SwitchSettings';
import StartEndTimePicker from '../components/StartEndTimePicker';
import useTheme from '../hooks/useTheme';

import { getQuotesInApp } from '../helpers/getQuotes';
import { getRandomQuote } from '../helpers/getRandomQuote';
import { isIOS } from '../helpers/platform';
import { clearAsyncStorage } from '../helpers/clearAsyncStorage';
import { getDeviceTimeFormat } from '../helpers/getDeviceTimeFormat';
import { getStoredIs24Hour } from '../helpers/getStoredIs24Hour';
import { formatStartEndTime } from '../helpers/formatStartEndTime';
import { getStoredColorScheme } from '../helpers/getStoredColorScheme';
import { updateStoredNotifDate } from '../helpers/updateStoredNotifDate';
import { pushNextQuoteNotification } from '../helpers/pushNextQuoteNotification';
import { storeData, removeStoredData } from '../services/storage';
import { cancelAllLocalNotifications } from '../helpers/cancelAllLocalNotifications';
import { fetchAndDispatchOnlineQuotes } from '../helpers/fetchAndDispatchOnlineQuotes';

import { LocalStyles } from '../styles/settingsScreenStyles';
import { backgrounds } from '../constants/images';

const SettingsScreen: FC<ScreenProps> = (props): ReactElement => {
  global.log('[SettingsScreen] -> Component rendering...').green();

  const [timePickerType, setTimePickerType] = useState<TimePickerType>(null);

  const dispatch = useDispatch();

  const colorSchemeState = useSelector((state: RootState) => state.settings.colorScheme);
  const followDeviceColorSchemeState = useSelector((state: RootState) => state.settings.followDeviceColorScheme);
  const followDeviceTimeFormatState = useSelector((state: RootState) => state.settings.followDeviceTimeFormat);
  const is24HourState = useSelector((state: RootState) => state.settings.is24Hour);
  const quoteNotifStartTimeState = useSelector((state: RootState) => state.settings.quoteNotifStartTime);
  const quoteNotifEndTimeState = useSelector((state: RootState) => state.settings.quoteNotifEndTime);
  const quoteNotifStatusState = useSelector((state: RootState) => state.settings.quoteNotifStatus);
  const quotesDailyState = useSelector((state: RootState) => state.settings.quotesDaily);
  const quotesSourcesState = useSelector((state: RootState) => state.settings.quotesSources);
  const isDarkMode = colorSchemeState === 'dark';

  const { styles, colors, activeOpacity } = useTheme();
  const localStyles: LocalStyles = require('../styles/settingsScreenStyles')(colors);

  async function handleOnChangeFollowDeviceColorSchemeSwitch(isEnabled: boolean) {
    // Navigation.ts rerenders and dispatch colorScheme currently in use on the device if isEnabled is true
    dispatch(setFollowDeviceColorScheme(isEnabled));
    if (!isEnabled) {
      const colorScheme = await getStoredColorScheme();
      dispatch(setColorScheme(colorScheme, false));
    }
  }

  function handleOnChangeColorSchemeSwitch(isEnabled: boolean) {
    const newColorScheme: ColorSchemeName = isEnabled ? 'dark' : 'light';
    dispatch(setColorScheme(newColorScheme));
  }

  async function handleOnChangeFollowDeviceTimeFormat(isEnabled: boolean) {
    dispatch(setFollowDeviceTimeFormat(isEnabled));
    let is24Hour: boolean;
    if (isEnabled) {
      is24Hour = await getDeviceTimeFormat();
    } else {
      is24Hour = await getStoredIs24Hour();
    }
    dispatch(setIs24Hour(is24Hour, false));
  }

  function handleOnChange1224HourSwitch(newValue: boolean) {
    dispatch(setIs24Hour(newValue));
  }

  function handleOnChangeQuotesDaily(value: number) {
    dispatch(setQuotesDaily(value));
    updateStoredNotifDate({ quotesDaily: value });
  }

  function handleOnPressManageQuotesSourcesButton() {
    dispatch(setActiveScreen({ screen: 'MANAGE_QUOTES_SOURCES' }));
  }

  function handleOnPressEnableNotifButton() {
    cancelAllLocalNotifications();
    pushNextQuoteNotification({ quotesDaily: quotesDailyState, firstExecution: true });
    dispatch(setQuoteNotifStatus('Enabled'));
  }

  function handleOnPressCancelNotifButton() {
    cancelAllLocalNotifications();
    storeData('QUOTE_NOTIF_PREV_CALC_PERIOD', -1);
    removeStoredData('QUOTE_NOTIF');
    dispatch(setQuoteNotifStatus('Disabled'));
  }

  function handleOnPressLogButton() {
    dispatch(setActiveScreen({ screen: 'LOG' }));
  }

  function handleOnPressFetchQuotesButton() {
    fetchAndDispatchOnlineQuotes(quotesSourcesState);
  }

  async function handleOnPressRemoveStoredQuotesButton() {
    await removeStoredData('ONLINE_QUOTES');
    const quotes = getQuotesInApp();
    dispatch(setQuotes(quotes));
    const newQuote = getRandomQuote();
    dispatch(setQuote(newQuote));
  }

  function handleOnPressClearAsyncStorageButton() {
    clearAsyncStorage();
  }

  const SwitchFollowDeviceTheme = (
    <SwitchSettings
      activeOpacity={activeOpacity}
      styles={localStyles.switchStyles}
      text="Follow device theme"
      value={followDeviceColorSchemeState}
      onValueChange={handleOnChangeFollowDeviceColorSchemeSwitch}
    />
  );

  const SwitchActivateDarkTheme = (
    <SwitchSettings
      activeOpacity={activeOpacity}
      styles={localStyles.switchStyles}
      text="Activate dark theme"
      value={isDarkMode}
      onValueChange={handleOnChangeColorSchemeSwitch}
    />
  );

  const SwitchFollowDeviceTimeFormat = (
    <SwitchSettings
      activeOpacity={activeOpacity}
      styles={localStyles.switchStyles}
      text="Follow device time format"
      value={followDeviceTimeFormatState}
      onValueChange={handleOnChangeFollowDeviceTimeFormat}
    />
  );

  const Switch1224HourMode = (
    <SwitchSettings
      activeOpacity={activeOpacity}
      styles={localStyles.switchStyles}
      text="12/24 hour mode"
      value={is24HourState}
      onValueChange={handleOnChange1224HourSwitch}
    />
  );

  const InputSpinnerDailyQuoes = (
    <View style={localStyles.inputSpinnerContainer}>
      <InputSpinner
        {...localStyles.inputSpinnerColors}
        {...localStyles.inputSpinner}
        min={0}
        max={999}
        step={1}
        value={quotesDailyState}
        editable={false}
        onChange={handleOnChangeQuotesDaily}
        placeholderTextColor="grey"
      />
    </View>
  );

  const { formattedStartTime, formattedEndTime } = formatStartEndTime(
    quoteNotifStartTimeState,
    quoteNotifEndTimeState,
    is24HourState,
  );

  const SectionStartEndTime = (
    <View style={localStyles.startEndTimeContainer}>
      <View style={localStyles.startEndTimeItems}>
        <Text style={localStyles.startEndTimeHeader}>Start</Text>
        <TouchableOpacity
          activeOpacity={activeOpacity}
          onPress={() => setTimePickerType('start')}
          style={{
            ...localStyles.startEndTimeTouchable,
            backgroundColor: colors.dateTimeItemBackground,
          }}>
          <Text style={{ ...localStyles.startEndTimeBtnText, color: colors.dateTimeText }}>{formattedStartTime}</Text>
        </TouchableOpacity>
      </View>
      <View style={localStyles.startEndTimeItems}>
        <Text style={localStyles.startEndTimeHeader}>End</Text>
        <TouchableOpacity
          activeOpacity={activeOpacity}
          onPress={() => setTimePickerType('end')}
          style={{
            ...localStyles.startEndTimeTouchable,
            backgroundColor: colors.dateTimeItemBackground,
          }}>
          <Text style={{ ...localStyles.startEndTimeBtnText, color: colors.dateTimeText }}>{formattedEndTime}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  const ButtonManageQuotesSources = (
    <TextButton
      containerStyle={styles.textButtonContainer}
      style={styles.textButton}
      onPress={handleOnPressManageQuotesSourcesButton}>
      Manage quote sources
    </TextButton>
  );

  const SectionEnableDisableQuoteNotifications = (
    <>
      <TextButton
        disabled={quoteNotifStatusState === 'Enabled'}
        containerStyle={styles.textButtonContainer}
        style={styles.textButton}
        styleDisabled={{ color: colors.disabledText }}
        onPress={handleOnPressEnableNotifButton}>
        Enable quote notifications
      </TextButton>
      <View style={localStyles.quoteNotifTextContainer}>
        <Text style={localStyles.quoteNotifText}>Swipe notification right to pause notifications</Text>
        <Text style={localStyles.quoteNotifText}>Open the app again to resume</Text>
      </View>
      <TextButton
        disabled={quoteNotifStatusState === 'Disabled'}
        containerStyle={styles.textButtonContainer}
        style={styles.textButton}
        styleDisabled={{ color: colors.disabledText }}
        onPress={handleOnPressCancelNotifButton}>
        Disable quote notifications
      </TextButton>
    </>
  );

  const LogButton = (
    <TextButton containerStyle={styles.textButtonContainer} style={styles.textButton} onPress={handleOnPressLogButton}>
      Log
    </TextButton>
  );

  const Buttons__Dev__ = (
    <>
      <TextButton
        containerStyle={styles.textButtonContainer}
        style={styles.textButton}
        onPress={handleOnPressFetchQuotesButton}>
        Get online quotes
      </TextButton>
      <TextButton
        containerStyle={styles.textButtonContainer}
        style={styles.textButton}
        onPress={handleOnPressRemoveStoredQuotesButton}>
        Remove stored online quotes
      </TextButton>
      <TextButton
        containerStyle={styles.textButtonContainer}
        style={styles.textButton}
        onPress={handleOnPressClearAsyncStorageButton}>
        Clear async storage (partial)
      </TextButton>
    </>
  );

  return (
    <ScrollView contentContainerStyle={styles.scrollView} bounces={false}>
      <View style={styles.screen}>
        <ImageBackground source={backgrounds.settingsScreenImg()} style={styles.imageBackground}>
          <View style={styles.screenSpacerTop} />
          <View style={styles.screenSpacerFlexGrow} />

          <View style={localStyles.settingsContent}>
            <View style={localStyles.settingsPaper}>
              <View>
                {SwitchFollowDeviceTheme}
                {!followDeviceColorSchemeState && SwitchActivateDarkTheme}
                {SwitchFollowDeviceTimeFormat}
                {!followDeviceTimeFormatState && Switch1224HourMode}
              </View>
            </View>

            <View style={localStyles.settingsPaper}>
              <View style={localStyles.dailyQuotesContainer}>
                <Text style={localStyles.dailyQuotesText}>Number of daily quotes</Text>
                {InputSpinnerDailyQuoes}
              </View>
              {SectionStartEndTime}
              {ButtonManageQuotesSources}
            </View>

            <View style={localStyles.settingsPaper}>{SectionEnableDisableQuoteNotifications}</View>
            {__DEV__ && global.loggingToStorage && <View style={localStyles.settingsPaper}>{LogButton}</View>}
            {__DEV__ && <View style={localStyles.settingsPaper}>{Buttons__Dev__}</View>}

            <View style={styles.screenSpacerFlexGrow} />
          </View>
          {timePickerType && (
            <StartEndTimePicker
              colors={colors}
              is24Hour={is24HourState}
              startTime={quoteNotifStartTimeState}
              endTime={quoteNotifEndTimeState}
              setStartTimeState={setQuoteNotifStartTime}
              setEndTimeState={setQuoteNotifEndTime}
              setTimePickerType={setTimePickerType}
              timePickerType={timePickerType}
            />
          )}
          {isIOS() && (
            <View style={styles.navButtonsContainer}>
              <GoBackButton onPress={props.handleBackPress!} />
            </View>
          )}
          <View style={styles.screenSpacerBottom} />
        </ImageBackground>
      </View>
    </ScrollView>
  );
};

export default SettingsScreen;
