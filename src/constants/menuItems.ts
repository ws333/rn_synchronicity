import { MenuItemHomeScreen as MenuItemHS, MenuItemBrainWaveScreen as MenuItemBS } from '../types/types';
import { menuItemImgs } from './images';

export const menuItemsHomeScreen = [
  new MenuItemHS('01', 'Quotes', 'QuotesImage', 'QUOTES', menuItemImgs.menuItemQuotes),
  new MenuItemHS('02', 'Manage\n quotes', 'Manage quotes', 'MANAGE_QUOTES_SOURCES', menuItemImgs.menuItemManageQuotes),
  new MenuItemHS('03', ' Brain\nwaves', 'BWImage', 'BRAINWAVE', menuItemImgs.menuItemBrainwaves),
  new MenuItemHS('04', 'FAQ', 'FAQImage', 'FAQ', menuItemImgs.menuItemFAQ),
  new MenuItemHS('05', 'Donate', 'DonationsImage', 'DONATE', menuItemImgs.menuItemDonate),
  new MenuItemHS('06', 'Settings', 'SettingsImage', 'SETTINGS', menuItemImgs.menuItemSettings),
];

export const menuItemsBrainwaveScreen = [
  new MenuItemBS('00', 'Info', 'Information text', '', menuItemImgs.menuItemInfo),
  new MenuItemBS(
    '01',
    'Monaural\n   waves',
    'Brainwave journey from epsilon to lambda and back',
    'IPXVRBmKcYY',
    menuItemImgs.menuItemMonaural,
  ),
  new MenuItemBS(
    '02',
    'Binaural\n  waves',
    'Binaural brainwave journey from epsilon to lambda and back',
    'H8Cbuc_03gg',
    menuItemImgs.menuItemBinaural,
  ),
  new MenuItemBS(
    '03',
    'Monaural\nw/stream',
    'Brainwave journey from epsilon to lambda and back with sound of a small stream',
    'BAGinXmlR0g',
    menuItemImgs.menuItemMonauralStream,
  ),
  new MenuItemBS(
    '04',
    ' Binaural\nw/stream',
    'Binaural brainwave journey from epsilon to lambda and back with sound of a small stream',
    'Pg24eAGYbDk',
    menuItemImgs.menuItemBinauralStream,
  ),
];
