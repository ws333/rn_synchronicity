import { Dimensions, ScaledSize } from 'react-native';
import { Torientation } from '../types/types';
import store from '../store/store';
import { styleConstants } from '../styles/globalStyles';
import { constants } from '../constants/constants';

export function getDimensionsState() {
  const dimensionsState = store.getState().app.dimensions;
  return dimensionsState;
}

export function getOrientationState() {
  const orientationState = store.getState().app.orientation;
  return orientationState;
}

export function getIsSmallScreenHeigthLandscape(dimensionsState: ScaledSize, orientationState: Torientation) {
  return dimensionsState.height < constants.dimensions.lowScreenHeightLandscape && orientationState === 'LANDSCAPE';
}

export function getIsSmallScreenHeightPortrait(dimensionsState: ScaledSize, orientationState: Torientation) {
  return dimensionsState.height < constants.dimensions.lowScreenHeightPortrait && orientationState === 'PORTRAIT';
}

export function getIsSmallScreenHeight() {
  const dimensionsState = getDimensionsState();
  const orientationState = getOrientationState();
  const isSmallScreenHeightPortrait = getIsSmallScreenHeightPortrait(dimensionsState, orientationState);
  const isSmallScreenHeigthLandscape = getIsSmallScreenHeigthLandscape(dimensionsState, orientationState);
  const isSmallScreenHeight = isSmallScreenHeightPortrait || isSmallScreenHeigthLandscape;
  return isSmallScreenHeight;
}

export function getInfoSectionWidth() {
  const dimensionsState = getDimensionsState();
  const infoSectionWidth =
    dimensionsState.width < constants.dimensions.lowScreenWidth
      ? styleConstants.infoSectionWidthSmallScreen
      : styleConstants.infoSectionWidth;
  return infoSectionWidth;
}

export function getNavbarHeightAndroid() {
  const screenHeight = Dimensions.get('screen').height;
  const windowHeight = Dimensions.get('window').height;
  const navbarHeight = screenHeight - windowHeight;
  return navbarHeight;
}

/**
 * Orientation.addOrientationListener from package react-native-orientation not stable on Android
 * Using react-native API Dimensions
 * (Update 12.04.2021: react-native-orientation-locker is active and might work)
 */
export function getIsPortrait() {
  const dim = Dimensions.get('screen');
  return dim.height > dim.width;
}

export function getIsLandscape() {
  return !getIsPortrait();
}
