import React, { FC, ReactElement, useEffect } from 'react';
import { View, FlatList, ImageBackground, Image, ViewStyle, Platform, ScrollView, LogBox } from 'react-native';
import { useDispatch } from 'react-redux';
import { setActiveScreen } from '../store/actions/appActions';
import { MenuItemHomeScreen as MenuItem } from '../types/types';
import MenuGridItem from '../components/MenuGridItem';
import useScreenDimensions from '../hooks/useScreenDimensions';
import useTheme from '../hooks/useTheme';
import { calcMenuGrid } from '../helpers/calcMenuGrid';
import { styleConstants } from '../styles/globalStyles';
import { menuItemsHomeScreen as menuItems } from '../constants/menuItems';
import { backgrounds, logo } from '../constants/images';

const HomeScreen: FC = (): ReactElement => {
  global.log('[HomeScreen] -> Component rendering...').green();

  const dispatch = useDispatch();
  const { dimensionsState, orientationState, isPortrait, isSmallScreenHeight } = useScreenDimensions();
  const { activeOpacityMenuGridItem, colors, styles } = useTheme();

  /**
   * No optimization penalty for such a small list
   */
  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);

  type FlatListItem = { item: MenuItem };
  const renderMenuGridItem = ({ item }: FlatListItem) => {
    return (
      <MenuGridItem
        activeOpacity={activeOpacityMenuGridItem}
        item={item}
        onPress={() => {
          global.log('[HomeScreen] -> Navigate to:', item.route).red();
          dispatch(setActiveScreen({ screen: item.route }));
        }}
        styles={styles}
      />
    );
  };

  const menuItemsCount = menuItems.length;
  const { menuColumns, menuGridWidth } = calcMenuGrid(dimensionsState, menuItemsCount);

  const MenuFlatList = (
    <FlatList<MenuItem>
      data={menuItems}
      renderItem={renderMenuGridItem}
      key={orientationState}
      numColumns={menuColumns}
      scrollEnabled={false}
    />
  );

  const homeLogoPaperStyle: ViewStyle = {
    alignItems: 'center' as const,
    justifyContent: 'center' as const,
    borderRadius: styleConstants.radius * 4,
    paddingVertical: styleConstants.padding / 1.5,
    paddingHorizontal: styleConstants.padding * 2,
    backgroundColor: colors.logoPaper,
    shadowColor: colors.shadowColor,
    ...styleConstants.shadow22NoOffset,
  };

  /**
   * extraBottomSpacer based on heigh and margins of GoBackButton
   */
  const extraScreenSpacerBottom = Platform.select({ ios: 53, android: 0 });
  const flexGrowTop = ((dimensionsState.height / dimensionsState.width) * 2) / 10;

  return (
    <ScrollView contentContainerStyle={styles.scrollView} bounces={false}>
      <View style={styles.screen}>
        <ImageBackground source={backgrounds.homeScreenImg()} style={styles.imageBackground}>
          <View style={styles.screenSpacerTop} />
          <View style={{ flexGrow: flexGrowTop }} />
          <View style={homeLogoPaperStyle}>
            <Image source={logo.homeLogo()} />
          </View>
          <View style={{ flexGrow: 1 - flexGrowTop }} />
          <View style={{ ...styles.menuGrid, width: menuGridWidth }}>{MenuFlatList}</View>
          {isSmallScreenHeight && orientationState === 'PORTRAIT' && <View style={styles.screenSpacerBottom} />}
          {isPortrait && <View style={{ height: extraScreenSpacerBottom }} />}
          <View style={styles.screenSpacerBottom} />
        </ImageBackground>
      </View>
    </ScrollView>
  );
};

export default HomeScreen;
