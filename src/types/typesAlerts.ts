import { ModalBaseProps } from 'react-native';
import AlertException from '../components/AlertException';
import AlertMessage from '../components/AlertMessage';
import { GlobalStyles } from '../styles/globalStyles';
import { ThemeColors } from './types';

export interface Props {
  alertState: AlertState | null;
  styles: GlobalStyles;
  colors: ThemeColors;
  animationType?: ModalBaseProps['animationType'];
}

export type AlertType = 'Message' | 'Exception';

type AlertComponents = typeof AlertMessage | typeof AlertException;
export type Alerts = { [k in AlertType]: AlertComponents };

export const alerts: Alerts = {
  Message: AlertMessage,
  Exception: AlertException,
};

export type ConfirmText = 'Done' | 'Close' | 'Ok' | 'Try again' | 'Yes';
export type CancelText = 'Abort' | 'Cancel' | 'Stop';

export interface Alert {
  type?: AlertType;
  title?: string;
  message?: string;
  closeOnPressBackdrop?: boolean;
  closeOnHardwareBackPress?: boolean;
  showCancel?: boolean;
  showConfirm?: boolean;
  cancelText?: CancelText;
  confirmText?: ConfirmText;
  destrutiveButtonColor?: string;
  handleOnPressCancel?: () => void;
  handleOnPressConfirm?: () => void;
  handleOnPressBackdrop?: () => void;
  handleOnDismiss?: () => void;
}

export type AlertState = Alert | null;

export interface AlertHandlers {
  handleOnPressCancel?: () => void;
  handleOnPressConfirm?: () => void;
}

export interface AddUpdateSourceCallbackArgs {
  closeCaller?: boolean;
  cancelPressed?: boolean;
  alert?: AlertState;
  key?: string;
}
export type AddUpdateSourceCallback = ({ alert, cancelPressed, closeCaller, key }: AddUpdateSourceCallbackArgs) => void;
