import store from '../store/store';
import { setAlert } from '../store/actions/appActions';
import { AlertHandlers, AlertState } from '../types/typesAlerts';

export function alertNetworkOffline(handlers: AlertHandlers) {
  const { handleOnPressConfirm } = handlers;
  const title = 'Network status';
  const message = '\nThe device is offline.\nConnect to the internet and try again.';
  const newAlertState: AlertState = {
    title,
    message,
    confirmText: 'Close',
    handleOnPressConfirm,
  };
  store.dispatch(setAlert(newAlertState));
}
