import { ScaledSize } from 'react-native';
import { globalStyles } from '../styles/globalStyles';

export function calcMenuGrid(dimensionsState: ScaledSize, menuItemsCount: number) {
  const gridWidthInPercent = parseInt(globalStyles.menuGrid.width as string, 10);

  const itemMarginHorizontal =
    (globalStyles.menuGridItem.marginLeft as number) + (globalStyles.menuGridItem.marginRight as number);
  const itemWidth = (globalStyles.menuGridItem.width as number) + itemMarginHorizontal;

  const widthAvailableToMenuGrid = dimensionsState.width * (gridWidthInPercent / 100);
  const maxColsCount = Math.floor(widthAvailableToMenuGrid / itemWidth);

  let numCols = maxColsCount;
  const numRows = Math.ceil(menuItemsCount / numCols);

  /**
   * Adjust numCols to fill up last row
   */
  if (numCols < menuItemsCount && menuItemsCount / numCols <= numRows) {
    const emptySlotsLastRow = numCols * numRows - menuItemsCount;

    if (emptySlotsLastRow >= numRows) {
      const numColsAdjustemnt = Math.floor(emptySlotsLastRow / numRows);
      numCols -= numColsAdjustemnt;
    }
  }

  const gridPaddingHorizontal =
    (globalStyles.menuGrid.paddingLeft as number) + (globalStyles.menuGrid.paddingRight as number);
  const menuColumns = menuItemsCount < numCols ? menuItemsCount : numCols;
  const menuGridWidth = itemWidth * menuColumns + gridPaddingHorizontal * 2;
  return { menuColumns, menuGridWidth };
}
