import { Constants, HTTP, HTTPS } from '../types/typesConstants';
import { quotesSources } from './quotesSources';
import { googleApiKey, revenueCatApiKey } from '../../secrets.keys';

const sourceAI = 'App AI';

export const constants: Constants = {
  defaults: {
    colorScheme: 'dark',
    followDeviceColorScheme: false,
    followDeviceTimeFormat: true,
    quote: { source: '', text: '' },
    quoteNotifStartTime: new Date(new Date().setHours(8, 0, 0, 0)),
    quoteNotifEndTime: new Date(new Date().setHours(22, 0, 0, 0)),
    is24Hour: false,
    quotesDaily: 1,
    quoteNotifStatus: 'Disabled',
    quotesSources: quotesSources,
    enabledQuotesSources: ['Nassim Haramein'],
    onlineAvatar: 'https://livinglight.one/quotes/avatar-default.png',
  },
  dimensions: {
    lowScreenHeightLandscape: 380,
    lowScreenHeightPortrait: 700,
    lowScreenWidth: 370,
  },
  keys: {
    googleApiKey: googleApiKey,
    revenueCatApiKey: revenueCatApiKey,
  },
  links: {
    paypal: 'https://paypal.me/livinglight333',
    patreon: 'https://www.patreon.com/livinglight333',
    buyMeACoffee: 'https://www.buymeacoffee.com/whitestone',
  },
  notification: {
    channel: '1',
    channelName: 'one.livinglight.alignment',
    channelDescription:
      'Notifications with inspirational quotes and affirmations with the timing of universal synchronicity.',
  },
  protocols: {
    http: HTTP,
    https: HTTPS,
    supportedProtocols: [HTTP, HTTPS],
  },
  quotes: {
    sourceAI: sourceAI,
    noEnabledQoutesSources: {
      source: sourceAI,
      text:
        'No quote sources are enabled\n\nGo to the Settings screen and press "Manage qoute sources" to select your preferred sources.',
    },
    noQuotesExistsForEnabledSources: {
      source: sourceAI,
      text:
        'No quotes found for the enabled qoutes sources. Please update the online quotes source,' +
        " if that doesn't help please remove it and contact the maker of the online qoutes source file.",
    },
  },
  supportedOrientations: ['landscape', 'portrait'],
  texts: {
    enterUrl: 'Enter url to online quotes source',
    enterUrlHelpText:
      "You don't need to specify protocol if it's http:// or https://\nNote that spaces are not allowed in urls\n\nExample to test: bit.ly/quotes-ae2",
    supportMesssage:
      'Please restart the app or phone and try again. ' +
      "If that doesn't help, first uninstall and then reinstall the app. If you still need assistance " +
      'after following these steps please send an email to: support@livinglight.one.',
  },
};
