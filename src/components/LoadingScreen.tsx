import React, { FC, ReactElement } from 'react';
import { Text, View, ViewStyle } from 'react-native';
import { Overlay } from 'react-native-elements';
import Spinner from 'react-native-spinkit';
import { ThemeColors } from '../types/types';
import { styleConstants } from '../styles/globalStyles';
import { constants } from '../constants/constants';

interface Props {
  spinnerSize: number;
  colors: ThemeColors;
}

const LoadingScreen: FC<Props> = (props): ReactElement => {
  const { spinnerSize, colors } = props;

  const overlayStyle: ViewStyle = {
    width: '100%',
    height: '100%',
    backgroundColor: colors.background,
  };

  const containerStyle: ViewStyle = {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  };

  return (
    <Overlay
      isVisible={true}
      overlayStyle={overlayStyle}
      statusBarTranslucent={true}
      supportedOrientations={constants.supportedOrientations}>
      <View style={containerStyle}>
        <View style={{ flexGrow: 0.6 }} />
        <Spinner isVisible={true} size={spinnerSize} type="ThreeBounce" color={colors.text} />
        <Text style={{ color: colors.text, fontSize: styleConstants.fontSizeXLarge }}> Loading...</Text>
        <View style={{ flexGrow: 1 }} />
      </View>
    </Overlay>
  );
};

export default LoadingScreen;
