import React, { FC, ReactElement } from 'react';
import { View } from 'react-native';
import { LocalStyles } from '../styles/alertStyles';
import { GlobalStyles } from '../styles/globalStyles';
import { AlertState } from '../types/typesAlerts';
import TextButton from './TextButton';

interface Props {
  alertState: AlertState;
  styles: GlobalStyles;
  localStyles: LocalStyles;
  showConfirm: boolean;
  confirmText: string;
  showCancel: boolean;
  cancelText: string;
}

const AlertButtons: FC<Props> = (props): ReactElement => {
  const { alertState, styles, localStyles, showConfirm, confirmText, showCancel, cancelText } = props;
  return (
    <View style={localStyles.buttonContainer}>
      {showConfirm && (
        <TextButton
          containerStyle={styles.textButtonContainerNoTop}
          style={styles.textButtonSmall}
          onPress={alertState?.handleOnPressConfirm}>
          {confirmText}
        </TextButton>
      )}
      {showCancel && (
        <TextButton
          containerStyle={styles.textButtonContainerNoTop}
          style={styles.textButtonSmall}
          onPress={alertState?.handleOnPressCancel}>
          {cancelText}
        </TextButton>
      )}
    </View>
  );
};

export default AlertButtons;
