import store from '../store/store';
import { UpdateStartEndTime } from '../types/types';
import { updateStoredNotifDate } from './updateStoredNotifDate';

export const updateStoredNotifStartEndTime = ({ newStartTime, newEndTime }: UpdateStartEndTime) => {
  let { quoteNotifSchedule } = store.getState().settings;

  if (newStartTime) {
    const _startHours = new Date(newStartTime).getHours();
    const _startMins = new Date(newStartTime).getMinutes();
    quoteNotifSchedule = { ...quoteNotifSchedule, startHours: _startHours, startMins: _startMins };
  }
  if (newEndTime) {
    const _endHours = new Date(newEndTime).getHours();
    const _endMins = new Date(newEndTime).getMinutes();
    quoteNotifSchedule = { ...quoteNotifSchedule, endHours: _endHours, endMins: _endMins };
  }
  updateStoredNotifDate({ quoteNotifSchedule });
};
