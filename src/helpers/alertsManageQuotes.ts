import store from '../store/store';
import { setAlert } from '../store/actions/appActions';
import { AlertHandlers, AlertState } from '../types/typesAlerts';
import { constants } from '../constants/constants';

export function AlertUpdateQuotesSource(source: string, handlers: AlertHandlers) {
  const { handleOnPressCancel, handleOnPressConfirm } = handlers;
  const title = 'Update';
  const message = `\nThis will update "${source}" and related quotes from server.\n\nDo you want to continue?`;
  const newAlertState: AlertState = {
    title,
    message,
    confirmText: 'Yes',
    showCancel: true,
    handleOnPressCancel,
    handleOnPressConfirm,
  };
  store.dispatch(setAlert(newAlertState));
}

export function AlertRemoveQuotesSource(source: string, handlers: AlertHandlers) {
  const { handleOnPressCancel, handleOnPressConfirm } = handlers;
  const title = 'Remove';
  const message = `\nThis will remove "${source}" and related quotes from the app.\n\nDo you want to continue?`;
  const newAlertState: AlertState = {
    title,
    message,
    confirmText: 'Yes',
    showCancel: true,
    handleOnPressCancel,
    handleOnPressConfirm,
  };
  store.dispatch(setAlert(newAlertState));
}

export function AlertRemoveQuotesSourceFailed(source: string) {
  const message = `\nRemoval of "${source}" failed...\n\n${constants.texts.supportMesssage}`;
  const newAlertState: AlertState = {
    type: 'Exception',
    message,
    confirmText: 'Close',
    showCancel: false,
    handleOnPressConfirm: () => store.dispatch(setAlert(null)),
  };
  store.dispatch(setAlert(newAlertState));
}
