import { ColorSchemeName } from 'react-native';
import { getStoredData } from '../services/storage';
import { constants } from '../constants/constants';

export async function getStoredColorScheme() {
  const storedcolorScheme: ColorSchemeName | null = await getStoredData('COLOR_SCHEME');
  const colorScheme: ColorSchemeName = storedcolorScheme ?? constants.defaults.colorScheme;
  return colorScheme;
}
