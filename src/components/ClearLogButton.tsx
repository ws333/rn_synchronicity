import React, { FC, ReactElement } from 'react';
import { GlobalStyles } from '../styles/globalStyles';
import { ThemeColors } from '../types/types';
import TextButtonAppTheme from './TextButtonAppTheme';

type Props = {
  onPress: () => void;
  styles: Partial<GlobalStyles>;
  colors: Partial<ThemeColors>;
  activeOpacity: number;
};

const ClearLogButton: FC<Props> = (props): ReactElement => {
  const { colors, activeOpacity, styles } = props;
  return (
    <TextButtonAppTheme dark colors={colors} activeOpacity={activeOpacity} onPress={props.onPress} styles={styles}>
      Clear log
    </TextButtonAppTheme>
  );
};

export default ClearLogButton;
