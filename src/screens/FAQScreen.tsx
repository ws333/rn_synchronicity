import React, { FC, ReactElement } from 'react';
import { ScrollView, View, Text, Image, ImageBackground } from 'react-native';
import { ScreenProps } from '../types/types';
import GoBackButton from '../components/GoBackButton';
import useTheme from '../hooks/useTheme';
import { isIOS } from '../helpers/platform';
import { LocalStyles } from '../styles/FAQScreenStyles';
import { backgrounds, headers } from '../constants/images';
import { FAQItems } from '../constants/FAQitems';
import { handlePressLink } from '../helpers/handlePressLink';

const FAQScreen: FC<ScreenProps> = (props): ReactElement => {
  global.log('[FAQScreen] -> Component rendering...').green();

  const { styles, colors } = useTheme();
  const localStyles: LocalStyles = require('../styles/FAQScreenStyles')(colors);

  const FAQBody = FAQItems.map((item, i) => {
    return (
      // eslint-disable-next-line react/no-array-index-key
      <View key={i} style={localStyles.FAQItemContainer}>
        <Text style={localStyles.FAQTextQ}>{item.Q}</Text>
        <Text style={localStyles.FAQTextA}>{item.A}</Text>
        <Text />
      </View>
    );
  });

  const header = (
    <View style={{ ...styles.header }}>
      <Image source={headers.FAQHeaderImg()} />
    </View>
  );

  const CreditsSection = (
    <View style={localStyles.FAQItemContainer}>
      <Text style={localStyles.credits}>Credits:</Text>
      <Text style={localStyles.credits} onPress={() => handlePressLink('https://unsplash.com/photos/kQ73K7pJKrA')}>
        Light theme background photo by SUHAIL RA (tap to open link)
      </Text>
    </View>
  );

  const InfoSection = (
    <View style={localStyles.FAQPaper}>
      <ScrollView contentContainerStyle={styles.scrollView} showsVerticalScrollIndicator={false}>
        {header}
        <View style={localStyles.FAQSpacer} />
        {FAQBody}
        <Text />
        {CreditsSection}
      </ScrollView>
    </View>
  );

  return (
    <View style={styles.screen}>
      <ImageBackground source={backgrounds.FAQScreenImg()} style={styles.imageBackground}>
        <View style={styles.screenSpacerTop} />

        <View style={localStyles.FAQContent}>{InfoSection}</View>

        <View style={styles.screenSpacerFlexGrow} />
        {isIOS() && (
          <View style={styles.navButtonsContainer}>
            <GoBackButton onPress={props.handleBackPress!} />
          </View>
        )}
        <View style={styles.screenSpacerBottom} />
      </ImageBackground>
    </View>
  );
};

export default FAQScreen;
