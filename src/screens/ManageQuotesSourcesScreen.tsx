import React, { FC, ReactElement, useEffect, useRef, useState } from 'react';
import { ImageBackground, LayoutChangeEvent, Text, View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { setActiveScreen, setQuote, setSelectedListItem, setAddSourceButtonHeight } from '../store/actions/appActions';
import { RootState } from '../store/store';
import { Quote, ScreenProps } from '../types/types';
import SpinnerThreeBounce from '../components/SpinnerThreeBounce';
import QuotesSourceList from '../components/QuotesSourceList';
import GoBackButton from '../components/GoBackButton';
import TextButton from '../components/TextButton';
import useTheme from '../hooks/useTheme';
import useScreenDimensions from '../hooks/useScreenDimensions';
import { getEnabledQuotesSources } from '../helpers/getEnabledQuotesSources';
import { updateStoredNotifQuote } from '../helpers/updateStoredNotifQuote';
import { getEnabledQuotes } from '../helpers/getEnabledQuotes';
import { isAndroid, isIOS } from '../helpers/platform';
import { getRandomQuote } from '../helpers/getRandomQuote';
import { arraysAreEqual } from '../helpers/arraysAreEqual';
import { styleConstants } from '../styles/globalStyles';
import { LocalStyles } from '../styles/manageQuotesSourcesScreenStyles';
import { backgrounds } from '../constants/images';
import { constants } from '../constants/constants';

const ManageQuotesSourcesScreen: FC<ScreenProps> = (props): ReactElement => {
  global.log('[ManageQuotesSourcesScreen] -> rendering... -> props', { ...props }).green();

  const [showSpinnerState, setShowSpinnerState] = useState(false);

  const { activeOpacity, colors, styles } = useTheme();
  const { orientationState, isPortrait, isSmallScreenHeight, spinnerSize } = useScreenDimensions();
  const localStyles: LocalStyles = require('../styles/manageQuotesSourcesScreenStyles')(colors);

  if (isAndroid() && isSmallScreenHeight) {
    styles.screenSpacerBottom = { height: Number(styles.screenSpacerBottom?.height) * 2 };
  }

  const quoteNotifStatusState = useSelector((state: RootState) => state.settings.quoteNotifStatus);
  const dispatch = useDispatch();

  const initialEnabledState = useRef<string[]>(getEnabledQuotesSources());

  useEffect(() => {
    function updateQuoteAndNotif() {
      const initialEnabled = initialEnabledState.current;
      const currentEnabled = getEnabledQuotesSources();
      const listHasChanged = !arraysAreEqual(initialEnabled, currentEnabled);
      if (listHasChanged) {
        const newNotificationQuote = getRandomQuote();
        if (quoteNotifStatusState === 'Enabled') {
          updateStoredNotifQuote(newNotificationQuote);
        }
        /**
         * Get/dispatch a new quote to display on QuoteScreen that is different from redux app.quote (which is default) AND newNotificationQuote
         */
        const enabledSources = getEnabledQuotesSources();
        const quotesCount = getEnabledQuotes(enabledSources).length;
        let newDisplayQuote: Quote = constants.defaults.quote;
        do {
          newDisplayQuote = getRandomQuote();
        } while (newDisplayQuote.text === newNotificationQuote.text && quotesCount > 2);
        dispatch(setQuote(newDisplayQuote));
      }
    }
    return () => {
      dispatch(setSelectedListItem(''));
      updateQuoteAndNotif();
    };
  }, [dispatch, quoteNotifStatusState]);

  function handleOnLayoutAddSourceButton(event: LayoutChangeEvent) {
    const addSourceButtonHeight =
      event.nativeEvent.layout.height +
      Number(localStyles.paper.marginTop) -
      Number(styles.textButtonContainerNoTop?.marginVertical);
    dispatch(setAddSourceButtonHeight(addSourceButtonHeight));
  }

  const addButtonMarginTop = Number(
    isPortrait ? Number(styles.textButtonContainer.marginVertical) * 2 : styles.textButtonContainer.marginVertical,
  );

  const AddOnlineQuotesSourceButton = () => (
    <View onLayout={handleOnLayoutAddSourceButton}>
      <TextButton
        containerStyle={{
          ...styles.textButtonContainer,
          marginTop: addButtonMarginTop,
          alignSelf: 'center',
        }}
        style={styles.textButton}
        onPress={() => dispatch(setActiveScreen({ screen: 'ADD_QUOTES_SOURCE' }))}>
        Add online quotes source
      </TextButton>
    </View>
  );

  function handleOnPressGoBackButton() {
    if (props.handleBackPress) {
      props.handleBackPress();
    }
  }

  const [tapTextHeight, setTapTextHeight] = useState(0);

  function handleOnLayoutTapText(event: LayoutChangeEvent) {
    const { height } = event.nativeEvent.layout;
    setTapTextHeight(height);
  }

  /**
   * Calculate a constant distance to AddOnlineQuotesSourceButton in case AddOnlineQuotesSourceButton margins are modified
   * Delay displaying content until tapTextHeight is set
   */
  const TapText = () => {
    const offset = addButtonMarginTop - 5;
    return (
      <Text
        onLayout={handleOnLayoutTapText}
        style={{
          alignSelf: 'center',
          color: colors.textButtonText,
          fontSize: styleConstants.fontSizeBase,
          fontStyle: 'italic',
          marginTop: -tapTextHeight + offset,
          marginBottom: -offset,
        }}>
        {tapTextHeight ? 'Tap an item for details' : null}
      </Text>
    );
  };

  return (
    <View style={styles.screen}>
      <ImageBackground source={backgrounds.settingsScreenImg()} style={styles.imageBackground}>
        <View style={styles.screenOverlay}>
          <View style={styles.screenSpacerTop} />
          <View style={localStyles.paper}>
            <TapText />
            {tapTextHeight ? (
              <View style={localStyles.content}>
                <SpinnerThreeBounce isVisible={showSpinnerState} spinnerSize={spinnerSize} colors={colors} />
                <AddOnlineQuotesSourceButton />
                <QuotesSourceList
                  activeOpacity={activeOpacity}
                  colors={colors}
                  orientationState={orientationState}
                  setShowSpinnerState={setShowSpinnerState}
                  styles={styles}
                />
              </View>
            ) : null}
          </View>
          {isIOS() && (
            <View style={styles.navButtonsContainer}>
              <GoBackButton onPress={handleOnPressGoBackButton} />
            </View>
          )}
          <View style={styles.screenSpacerBottom} />
        </View>
      </ImageBackground>
    </View>
  );
};

export default ManageQuotesSourcesScreen;
