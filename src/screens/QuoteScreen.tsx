import React, { FC, ReactElement } from 'react';
import { ScrollView, View, ImageBackground } from 'react-native';
import { useSelector } from 'react-redux';
import { RootState } from '../store/store';
import { ScreenProps } from '../types/types';
import Quote from '../components/Quote';
import GoBackButton from '../components/GoBackButton';
import useTheme from '../hooks/useTheme';
import { isIOS } from '../helpers/platform';
import { backgrounds } from '../constants/images';

const QuotesScreen: FC<ScreenProps> = (props): ReactElement => {
  global.log('[QuotesScreen] -> Component rendering...').green();

  const quoteState = useSelector((state: RootState) => state.app.quote);
  const { activeOpacity, colors, screenSpacerTop, styles } = useTheme();

  return (
    <ScrollView contentContainerStyle={styles.scrollView} bounces={false}>
      <View style={styles.screen}>
        <ImageBackground source={backgrounds.quoteScreenImg()} style={styles.imageBackground}>
          <View style={{ height: screenSpacerTop / 2 }} />
          <View style={styles.screenSpacerFlexGrow} />
          <View>
            <Quote quote={quoteState} colors={colors} activeOpacity={activeOpacity} />
          </View>
          <View style={styles.screenSpacerFlexGrow} />
          {isIOS() && <GoBackButton onPress={props.handleBackPress!} />}
          <View style={styles.screenSpacerBottom} />
        </ImageBackground>
      </View>
    </ScrollView>
  );
};

export default QuotesScreen;
