import { constants } from '../constants/constants';

export function addProtocolIfMissing(url: string) {
  const { supportedProtocols } = constants.protocols;
  const protocolInUrl = url.includes(':/') ? url.split(':/')[0] : [];
  const supportedProtocolInUrl = supportedProtocols.filter((protocol) => url.startsWith(protocol));
  const hasSupportedProtocol = !!supportedProtocolInUrl.length;
  const hasUnsupportedProtocol = !hasSupportedProtocol && !!protocolInUrl.length;
  if (!hasSupportedProtocol && !hasUnsupportedProtocol) {
    url = `${constants.protocols.https}//${url}`;
  }
  return url;
}
