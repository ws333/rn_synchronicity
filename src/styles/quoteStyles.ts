import { TextStyle, ViewStyle } from 'react-native';
import { isTablet } from '../helpers/device';
import { ThemeColors } from '../types/types';
import { styleConstants } from './globalStyles';

export interface LocalStyles {
  quoteButtonContainer: ViewStyle;
  quoteButton: TextStyle;
  quotePaper: ViewStyle;
  quoteSource: TextStyle;
  quoteText: TextStyle;
}

module.exports = (colors: ThemeColors): LocalStyles => {
  return {
    quoteButtonContainer: {
      alignItems: 'flex-end',
      flexDirection: 'row',
      justifyContent: 'center',
    },
    quoteButton: {
      marginHorizontal: styleConstants.marginVH,
      marginVertical: styleConstants.marginVH / 2,
      fontSize: styleConstants.fontSizeMedium,
      fontStyle: 'italic',
      color: colors.quoteButtonText,
    },
    quotePaper: {
      width: isTablet() ? '70%' : styleConstants.paperWidth,
      marginBottom: styleConstants.marginLRTB / 2,
      marginHorizontal: styleConstants.marginVH,
      marginTop: styleConstants.marginLRTB,
      borderRadius: styleConstants.radius,
      borderWidth: styleConstants.borderWidth,
      backgroundColor: colors.quotePaper,
      shadowColor: colors.shadowColor,
      borderColor: colors.border,
      ...styleConstants.shadow11NoOffset,
    },
    quoteSource: {
      paddingTop: 3,
      paddingBottom: 0,
      fontSize: styleConstants.fontSizeBase,
      fontStyle: 'italic',
      fontWeight: 'bold',
      textAlign: 'center',
      opacity: 1,
      color: colors.text,
    },
    quoteText: {
      fontSize: isTablet() ? (styleConstants.fontSizeXLarge / 4) * 5 : styleConstants.fontSizeXLarge,
      fontStyle: 'italic',
      textAlign: 'center',
      padding: styleConstants.padding,
      paddingBottom: 0,
      opacity: 1,
      color: colors.text,
    },
  };
};
