import { logToConsoleWithColors } from '../services/logToConsoleWithColors';

declare global {
  type Log = typeof logToConsoleWithColors;
  type LogLevel = 0 | 1 | 2 | 3 | 4 | 5;

  /**
   * Extend the Global interface for the NodeJS namespace
   */
  namespace NodeJS {
    interface Global {
      log: Log;
      logLevel: LogLevel;
      loggingToStorage: boolean;
      HermesInternal: boolean;
    }
  }
}
