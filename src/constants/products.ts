export const onetimeDonations = [
  'alignment_donation_onetime_1',
  'alignment_donation_onetime_3',
  'alignment_donation_onetime_5',
  'alignment_donation_onetime_10',
  'alignment_donation_onetime_20',
  'alignment_donation_onetime_30',
];

export const monthlyDonations = [
  'alignment_donation_monthly_1',
  'alignment_donation_monthly_3',
  'alignment_donation_monthly_5',
  'alignment_donation_monthly_10',
  'alignment_donation_monthly_20',
  'alignment_donation_monthly_30',
];
