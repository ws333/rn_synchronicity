import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
import { Notification } from '../types/types';
import { isAndroid, isIOS } from './platform';
import { constants } from '../constants/constants';

export const scheduleLocalNotification = ({ title, message, date }: Notification) => {
  global.log('[scheduleLocalNotification] -> start executing...').yellowOnRed();
  if (isAndroid()) {
    PushNotification.localNotificationSchedule({
      channelId: constants.notification.channel,
      title: title,
      message: message,
      date: date,
    });
  }
  if (isIOS()) {
    PushNotificationIOS.addNotificationRequest({
      id: Date.now().toString(),
      fireDate: date,
      title: title,
      body: message,
      badge: 1,
    });
  }
};
