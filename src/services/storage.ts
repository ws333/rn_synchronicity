import AsyncStorage from '@react-native-async-storage/async-storage';
import { StorageKey } from '../types/typesStorage';

export async function storeData(key: StorageKey, value: any) {
  try {
    const data = JSON.stringify(value);
    global.log('[storage] -> async storeData() -> key, value =', key, value).blackOnOrange();
    await AsyncStorage.setItem(key, data);
    return true;
  } catch (e) {
    global.log('[storage] -> async storeData() exception:', e).yellowOnBlack();
    return false;
  }
}

export async function getStoredData(key: StorageKey) {
  try {
    const value = await AsyncStorage.getItem(key);
    global.log('[storage] -> async getStoredData() -> key, value =', key, value).blackOnOrange();
    return value === null ? null : JSON.parse(value);
  } catch (e) {
    global.log('[storage] -> async getStoredData() -> exception:', e).yellowOnBlack();
    return null;
  }
}

export async function removeStoredData(key: StorageKey) {
  try {
    await AsyncStorage.removeItem(key);
    global.log('[storage] -> async removeStoredData() -> key =', key).blackOnOrange();
    return true;
  } catch (e) {
    global.log('[storage] -> async removeStoredData() -> exception:', e).yellowOnBlack();
    return false;
  }
}
