import { StyleProp, TextStyle, ViewStyle } from 'react-native';
import { ImageStyle } from 'react-native-fast-image';
import { ThemeColors } from '../types/types';
import { styleConstants } from './globalStyles';

export interface LocalStyles {
  avatarContainer: ViewStyle;
  avatarImage: StyleProp<ImageStyle>;
  avatarPlacehodler: ViewStyle;
  flatListContainer: ViewStyle;
  listItemContainer: ViewStyle;
  listItemContent: ViewStyle;
  listItemCheckBoxContainer: ViewStyle;
  listItemCheckBoxPressArea: ViewStyle;
  listItemChevron: ViewStyle;
  listItemDetailsContainer: ViewStyle;
  listItemDetailsButtonsContainer: ViewStyle;
  listItemDetailsSubtitle: TextStyle;
  listItemTitle: TextStyle;
  listItemSubtitle: TextStyle;
  onlineListItemButtonsContainer: ViewStyle;
  textButtonContainer: ViewStyle;
}

module.exports = (colors: ThemeColors): LocalStyles => {
  return {
    avatarContainer: {
      margin: styleConstants.marginLRTB / 2,
    },
    avatarImage: {
      width: '100%',
      height: '100%',
    },
    avatarPlacehodler: {
      backgroundColor: colors.background,
    },
    flatListContainer: {
      marginBottom: styleConstants.marginLRTB,
    },
    listItemContainer: {
      borderRadius: 22,
      borderWidth: styleConstants.borderWidth,
      borderColor: colors.border,
      marginVertical: 3,
      marginHorizontal: 11,
      backgroundColor: colors.background,
      alignItems: 'flex-start',
      padding: 0,
      shadowColor: colors.shadowColor,
      shadowOffset: {
        width: 0,
        height: 7,
      },
      shadowOpacity: 0.29,
      shadowRadius: 4.65,
    },
    listItemContent: {
      flex: 8,
      width: '100%',
      marginVertical: styleConstants.marginLRTB / 2,
    },
    listItemCheckBoxContainer: {
      flex: 1,
      alignItems: 'flex-start',
      margin: styleConstants.marginLRTB / 2,
    },
    listItemCheckBoxPressArea: {
      borderRadius: styleConstants.radius,
    },
    listItemChevron: {
      alignSelf: 'flex-start',
      marginTop: styleConstants.marginLRTB / 3,
    },
    listItemDetailsContainer: {
      width: '100%',
    },
    listItemDetailsButtonsContainer: {
      alignItems: 'center',
    },
    listItemTitle: {
      fontWeight: 'bold',
      color: colors.text,
    },
    listItemSubtitle: {
      fontSize: 14,
      color: colors.text,
    },
    listItemDetailsSubtitle: {
      fontSize: styleConstants.fontSizeBase - 2,
      fontStyle: 'italic',
      color: colors.textButtonText,
    },
    onlineListItemButtonsContainer: {
      flexDirection: 'row',
    },
    textButtonContainer: {
      marginTop: styleConstants.marginLRTB,
      marginRight: styleConstants.marginLRTB,
    },
  };
};
